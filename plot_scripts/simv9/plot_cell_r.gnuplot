# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels


set terminal unknown
plot "../output/data/simv9/cell.dat" using 1:16

# output
set terminal png size 1920,1080 enhanced font "Helvetica,14" lw 1.2 background rgb 'white'
set output "../output/plot/simv9/cell_r.png"

set yrange [0 : *]
set key out horiz right top box

# function for range day 6 to day 9
BGX(x,y)=(x==y?0:1/0)
BGY(x,y)=(x==y?GPVAL_Y_MAX:1/0)


# Plot 2 - 2
set xlabel "Time (min)"
set ylabel "Glucose in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot "../output/data/simv9/cell.dat" using 1:(BGX($9,1)):(BGY($9,1)) with filledcurve lc rgb "#e41a1c" fs solid 0.3 title 'G1', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,2)):(BGY($9,2)) with filledcurve lc rgb "#377eb8" fs solid 0.3 title 'S', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,3)):(BGY($9,3)) with filledcurve lc rgb "#4daf4a" fs solid 0.3 title 'G2', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,4)):(BGY($9,4)) with filledcurve lc rgb "#984ea3" fs solid 0.3 title 'M', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,5)):(BGY($9,5)) with filledcurve lc rgb "#ff7f00" fs solid 0.3 title 'G0', \
     # "../output/data/simv9/cell.dat" u 1:10 w lines title "CR" lw 2 lt rgb "black"
     # "../output/data/simv9/cell.dat" u 1:11 w lines title "LAF" lw 2 lt rgb "black"
     # "../output/data/simv9/cell.dat" u 1:12 w lines title "GLTMNLYS" lw 2 lt rgb "black"
     # "../output/data/simv9/cell.dat" u 1:13 w lines title "LR" lw 2 lt rgb "black"
     # "../output/data/simv9/cell.dat" u 1:14 w lines title "CRLAF" lw 2 lt rgb "black"
     # "../output/data/simv9/cell.dat" u 1:15 w lines title "GlcGltBM" lw 2 lt rgb "black"
     # "../output/data/simv9/cell.dat" u 1:16 w lines title "GlcBM" lw 2 lt rgb "black"
     # "../output/data/simv9/cell.dat" u 1:17 w lines title "GltBM" lw 2 lt rgb "black"
     # "../output/data/simv9/cell.dat" u 1:18 w lines title "LctBM" lw 2 lt rgb "black"
