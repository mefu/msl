# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# output
set terminal png size 1920,1080 enhanced font "Helvetica,14" lw 1.2 background rgb 'white'
set output "../output/plot/simv9/cell.png"

# Multi plot layout
set multiplot layout 2, 2
set tmargin 2
set offset .01,.01,.01,.01
set yrange [0 : *]
set key off

# function for range day 6 to day 9
BGX(x,y)=(x==y?0:1/0)
BGY(x,y)=(x==y?STATS_max_y:1/0)


# Plot 2 - 2
stats "../output/data/simv9/cell.dat" u 1:3 nooutput
set xlabel "Time (min)"
set ylabel "Glucose in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot "../output/data/simv9/cell.dat" using 1:(BGX($9,1)):(BGY($9,1)) with filledcurve lc rgb "#e41a1c" fs solid 0.3 title 'G1', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,2)):(BGY($9,2)) with filledcurve lc rgb "#377eb8" fs solid 0.3 title 'S', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,3)):(BGY($9,3)) with filledcurve lc rgb "#4daf4a" fs solid 0.3 title 'G2', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,4)):(BGY($9,4)) with filledcurve lc rgb "#984ea3" fs solid 0.3 title 'M', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,5)):(BGY($9,5)) with filledcurve lc rgb "#ff7f00" fs solid 0.3 title 'G0', \
     "../output/data/simv9/cell.dat" using 1:3 with lines notitle lt rgb "black"

set key out horiz right top box
# Plot 2 - 2
stats "../output/data/simv9/cell.dat" u 1:6 nooutput
set xlabel "Time (min)"
set ylabel "Oxygen in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot "../output/data/simv9/cell.dat" using 1:(BGX($9,1)):(BGY($9,1)) with filledcurve lc rgb "#e41a1c" fs solid 0.3 title 'G1', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,2)):(BGY($9,2)) with filledcurve lc rgb "#377eb8" fs solid 0.3 title 'S', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,3)):(BGY($9,3)) with filledcurve lc rgb "#4daf4a" fs solid 0.3 title 'G2', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,4)):(BGY($9,4)) with filledcurve lc rgb "#984ea3" fs solid 0.3 title 'M', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,5)):(BGY($9,5)) with filledcurve lc rgb "#ff7f00" fs solid 0.3 title 'G0', \
     "../output/data/simv9/cell.dat" using 1:6 with lines notitle lt rgb "black"

set key off
# Plot 2 - 2
stats "../output/data/simv9/cell.dat" u 1:7 nooutput
set xlabel "Time (min)"
set ylabel "ATP in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot "../output/data/simv9/cell.dat" using 1:(BGX($9,1)):(BGY($9,1)) with filledcurve lc rgb "#e41a1c" fs solid 0.3 title 'G1', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,2)):(BGY($9,2)) with filledcurve lc rgb "#377eb8" fs solid 0.3 title 'S', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,3)):(BGY($9,3)) with filledcurve lc rgb "#4daf4a" fs solid 0.3 title 'G2', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,4)):(BGY($9,4)) with filledcurve lc rgb "#984ea3" fs solid 0.3 title 'M', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,5)):(BGY($9,5)) with filledcurve lc rgb "#ff7f00" fs solid 0.3 title 'G0', \
     "../output/data/simv9/cell.dat" using 1:7 with lines notitle lt rgb "black"

# Plot 2 - 3
stats "../output/data/simv9/cell.dat" u 1:8 nooutput
set xlabel "Time (min)"
set ylabel "Biomass in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot "../output/data/simv9/cell.dat" using 1:(BGX($9,1)):(BGY($9,1)) with filledcurve lc rgb "#e41a1c" fs solid 0.3 title 'G1', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,2)):(BGY($9,2)) with filledcurve lc rgb "#377eb8" fs solid 0.3 title 'S', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,3)):(BGY($9,3)) with filledcurve lc rgb "#4daf4a" fs solid 0.3 title 'G2', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,4)):(BGY($9,4)) with filledcurve lc rgb "#984ea3" fs solid 0.3 title 'M', \
     "../output/data/simv9/cell.dat" using 1:(BGX($9,5)):(BGY($9,5)) with filledcurve lc rgb "#ff7f00" fs solid 0.3 title 'G0', \
     "../output/data/simv9/cell.dat" using 1:8 with lines notitle lt rgb "black"

