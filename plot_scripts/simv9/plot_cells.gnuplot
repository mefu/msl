# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# Commons
set key out vert right box
set offset .01,.01,.01,.01
set yrange [0 : *]
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
set tmargin 2

# output
set terminal png size 1920,1080 enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv9/cells_0_2.png"

# Multi plot layout
set multiplot layout 2, 1


# function for range day 6 to day 9
IR(x)=((x>=(6*24*60)) ? ((x<=(9*24*60)) ? x : 1/0) : 1/0)

# Plot 1 - 1
set title "Cell Cycle States [0, 2) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Cell Count"
plot  "../output/data/simv9/cells.dat" u (IR($1)):3 w lines title 'G1' lt rgb "#1b9eff", \
      "../output/data/simv9/cells.dat" u (IR($1)):4 w lines title 'S' lt rgb "#d95f02", \
      "../output/data/simv9/cells.dat" u (IR($1)):5 w lines title 'G2' lt rgb "#7570b3", \
      "../output/data/simv9/cells.dat" u (IR($1)):6 w lines title 'M' lt rgb "#e7298a", \
      "../output/data/simv9/cells.dat" u (IR($1)):7 w lines title 'G0' lt rgb "#66a61e", \
      "../output/data/simv9/cells.dat" u (IR($1)):2 w lines title 'Total' lt rgb "#e6ab02"

# Plot 1 - 2
set title "Contents of cells in distance [0, 2) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Mean in Cells (?)"
plot  "../output/data/simv9/cells.dat" u (IR($1)):8 w lines title 'Glucose' lt rgb "#1b9e77", \
      "../output/data/simv9/cells.dat" u (IR($1)):9 w lines title 'Glutamine' lt rgb "#d95f02", \
      "../output/data/simv9/cells.dat" u (IR($1)):10 w lines title 'Lactate' lt rgb "#7570b3", \
      "../output/data/simv9/cells.dat" u (IR($1)):11 w lines title 'Oxygen' lt rgb "#e7298a", \
      "../output/data/simv9/cells.dat" u (IR($1)):12 w lines title 'ATP' lt rgb "#66a61e", \
      "../output/data/simv9/cells.dat" u (IR($1)):13 w lines title 'Biomass' lt rgb "#e6ab02"

# unset
unset multiplot

# output
set terminal png size 1920,1080 enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv9/cells_2_4.png"

# Multi plot layout
set multiplot layout 2, 1

# Plot 1 - 1
set title "Cell Cycle States [2, 4) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Cell Count"
plot  "../output/data/simv9/cells.dat" u (IR($1)):15 w lines title 'G1' lt rgb "#1b9eff", \
      "../output/data/simv9/cells.dat" u (IR($1)):16 w lines title 'S' lt rgb "#d95f02", \
      "../output/data/simv9/cells.dat" u (IR($1)):17 w lines title 'G2' lt rgb "#7570b3", \
      "../output/data/simv9/cells.dat" u (IR($1)):18 w lines title 'M' lt rgb "#e7298a", \
      "../output/data/simv9/cells.dat" u (IR($1)):19 w lines title 'G0' lt rgb "#66a61e", \
      "../output/data/simv9/cells.dat" u (IR($1)):14 w lines title 'Total' lt rgb "#e6ab02"

# Plot 1 - 2
set title "Contents of cells in distance [2, 4) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Mean in Cells (?)"
plot  "../output/data/simv9/cells.dat" u (IR($1)):20 w lines title 'Glucose' lt rgb "#1b9e77", \
      "../output/data/simv9/cells.dat" u (IR($1)):21 w lines title 'Glutamine' lt rgb "#d95f02", \
      "../output/data/simv9/cells.dat" u (IR($1)):22 w lines title 'Lactate' lt rgb "#7570b3", \
      "../output/data/simv9/cells.dat" u (IR($1)):23 w lines title 'Oxygen' lt rgb "#e7298a", \
      "../output/data/simv9/cells.dat" u (IR($1)):24 w lines title 'ATP' lt rgb "#66a61e", \
      "../output/data/simv9/cells.dat" u (IR($1)):25 w lines title 'Biomass' lt rgb "#e6ab02"

# unset
unset multiplot

# output
set terminal png size 1920,1080 enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv9/cells_4_6.png"

# Multi plot layout
set multiplot layout 2, 1

# Plot 1 - 1
set title "Cell Cycle States [4, 6) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Cell Count"
plot  "../output/data/simv9/cells.dat" u (IR($1)):27 w lines title 'G1' lt rgb "#1b9eff", \
      "../output/data/simv9/cells.dat" u (IR($1)):28 w lines title 'S' lt rgb "#d95f02", \
      "../output/data/simv9/cells.dat" u (IR($1)):29 w lines title 'G2' lt rgb "#7570b3", \
      "../output/data/simv9/cells.dat" u (IR($1)):30 w lines title 'M' lt rgb "#e7298a", \
      "../output/data/simv9/cells.dat" u (IR($1)):31 w lines title 'G0' lt rgb "#66a61e", \
      "../output/data/simv9/cells.dat" u (IR($1)):26 w lines title 'Total' lt rgb "#e6ab02"

# Plot 1 - 2
set title "Contents of cells in distance [4, 6) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Mean in Cells (?)"
plot  "../output/data/simv9/cells.dat" u (IR($1)):32 w lines title 'Glucose' lt rgb "#1b9e77", \
      "../output/data/simv9/cells.dat" u (IR($1)):33 w lines title 'Glutamine' lt rgb "#d95f02", \
      "../output/data/simv9/cells.dat" u (IR($1)):34 w lines title 'Lactate' lt rgb "#7570b3", \
      "../output/data/simv9/cells.dat" u (IR($1)):35 w lines title 'Oxygen' lt rgb "#e7298a", \
      "../output/data/simv9/cells.dat" u (IR($1)):36 w lines title 'ATP' lt rgb "#66a61e", \
      "../output/data/simv9/cells.dat" u (IR($1)):37 w lines title 'Biomass' lt rgb "#e6ab02"

# unset
unset multiplot

# output
set terminal png size 1920,1080 enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv9/cells_6_8.png"

# Multi plot layout
set multiplot layout 2, 1

# Plot 1 - 1
set title "Cell Cycle States [6, 8) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Cell Count"
plot  "../output/data/simv9/cells.dat" u (IR($1)):39 w lines title 'G1' lt rgb "#1b9eff", \
      "../output/data/simv9/cells.dat" u (IR($1)):40 w lines title 'S' lt rgb "#d95f02", \
      "../output/data/simv9/cells.dat" u (IR($1)):41 w lines title 'G2' lt rgb "#7570b3", \
      "../output/data/simv9/cells.dat" u (IR($1)):42 w lines title 'M' lt rgb "#e7298a", \
      "../output/data/simv9/cells.dat" u (IR($1)):43 w lines title 'G0' lt rgb "#66a61e", \
      "../output/data/simv9/cells.dat" u (IR($1)):38 w lines title 'Total' lt rgb "#e6ab02"

# Plot 1 - 2
set title "Contents of cells in distance [6, 8) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Mean in Cells (?)"
plot  "../output/data/simv9/cells.dat" u (IR($1)):44 w lines title 'Glucose' lt rgb "#1b9e77", \
      "../output/data/simv9/cells.dat" u (IR($1)):45 w lines title 'Glutamine' lt rgb "#d95f02", \
      "../output/data/simv9/cells.dat" u (IR($1)):46 w lines title 'Lactate' lt rgb "#7570b3", \
      "../output/data/simv9/cells.dat" u (IR($1)):47 w lines title 'Oxygen' lt rgb "#e7298a", \
      "../output/data/simv9/cells.dat" u (IR($1)):48 w lines title 'ATP' lt rgb "#66a61e", \
      "../output/data/simv9/cells.dat" u (IR($1)):49 w lines title 'Biomass' lt rgb "#e6ab02"

# unset
unset multiplot

# output
set terminal png size 1920,1080 enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv9/cells_8_10.png"

# Multi plot layout
set multiplot layout 2, 1

# Plot 1 - 1
set title "Cell Cycle States [8, 10] to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Cell Count"
plot  "../output/data/simv9/cells.dat" u (IR($1)):51 w lines title 'G1' lt rgb "#1b9eff", \
      "../output/data/simv9/cells.dat" u (IR($1)):52 w lines title 'S' lt rgb "#d95f02", \
      "../output/data/simv9/cells.dat" u (IR($1)):53 w lines title 'G2' lt rgb "#7570b3", \
      "../output/data/simv9/cells.dat" u (IR($1)):54 w lines title 'M' lt rgb "#e7298a", \
      "../output/data/simv9/cells.dat" u (IR($1)):55 w lines title 'G0' lt rgb "#66a61e", \
      "../output/data/simv9/cells.dat" u (IR($1)):50 w lines title 'Total' lt rgb "#e6ab02"

# Plot 1 - 2
set title "Contents of cells in distance [8, 10] to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Mean in Cells (?)"
plot  "../output/data/simv9/cells.dat" u (IR($1)):56 w lines title 'Glucose' lt rgb "#1b9e77", \
      "../output/data/simv9/cells.dat" u (IR($1)):57 w lines title 'Glutamine' lt rgb "#d95f02", \
      "../output/data/simv9/cells.dat" u (IR($1)):58 w lines title 'Lactate' lt rgb "#7570b3", \
      "../output/data/simv9/cells.dat" u (IR($1)):59 w lines title 'Oxygen' lt rgb "#e7298a", \
      "../output/data/simv9/cells.dat" u (IR($1)):60 w lines title 'ATP' lt rgb "#66a61e", \
      "../output/data/simv9/cells.dat" u (IR($1)):61 w lines title 'Biomass' lt rgb "#e6ab02"
