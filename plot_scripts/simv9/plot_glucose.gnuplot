# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# output
set terminal png size 1920,1080 enhanced font "Helvetica,14" lw 1.2 background rgb 'white'
set output "../output/plot/simv9/glucose.png"

# Multi plot layout
set multiplot layout 2, 1 title "Glucose Concentrations" font ",14"
set tmargin 2
unset key
set key out vert right box
set offset .01,.01,.01,.01
set yrange [0 : *]

# Plot 1 - 1
set title "Glucose in Blood to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Glucose in Blood (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
plot  "../output/data/simv9/glucose.dat" using 1:3 with lines title 'Blood' lt rgb "#e41a1c"

# Plot 2 - 1
set title "Total Glucose in Tissue to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Total Glucose in Tissue (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
plot  "../output/data/simv9/glucose.dat" using 1:4 with lines title 'Total' lt rgb "#377eb8"

