# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# output
set terminal png size 1920,1080 enhanced font "Helvetica,14" lw 1.2 background rgb 'white'
set output "../output/plot/simv9/oxygen.png"

# Multi plot layout
set multiplot layout 2, 1 title "Oxygen Concentrations" font ",14"
set tmargin 2
unset key
set key out vert right box
set offset .01,.01,.01,.01
set yrange [0 : *]

# Plot 1 - 1
set title "Oxygen in Blood to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Oxygen in Blood (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
plot  "../output/data/simv9/oxygen.dat" using 1:3 with lines title 'Blood' lt rgb "#e41a1c"

# Plot 2 - 1
set title "Total Oxygen in Tissue to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Total Oxygen in Tissue (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
plot  "../output/data/simv9/oxygen.dat" using 1:4 with lines title 'Total' lt rgb "#377eb8"

