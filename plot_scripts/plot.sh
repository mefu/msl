#!/bin/bash
SIMV=$1
PLOT=$2

for file in simv$SIMV/plot$PLOT.gnuplot; do
  [ -e "$file" ] || continue
  gnuplot "$file"
done