# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
unset key

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv6/plot4.svg"

# function for range day 15 to day 18
InRange(x)=((x>=(15*24*60)) ? ((x<=(18*24*60)) ? x : 1/0) : 1/0)

# Plot 1 - 1
set xtic 60
set ytic 10
set title "Blood Glucose Rate to Time(min) day 15-18"
set xlabel "Time (min)"
set ylabel "Glucose Rate (?)"
set xr [21600: 25920]
set yr [0: 130]
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
set key inside center right vert box 
plot  "../output/data/simv6/data.dat" using (InRange($1)):3 with lines title 'Blood Glucose Rate' lt rgb "red", \
      "../output/data/simv6/data.dat" using (InRange($1)):4 with lines title 'Glucose Increase from Food' lt rgb "blue"