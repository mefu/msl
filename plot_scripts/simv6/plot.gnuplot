# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,14" lw 1.2 background rgb 'white'
set output "../output/plot/simv6/plot.svg"

# Multi plot layout
set multiplot layout 2, 2 title "Glucose Levels"
unset key

# Plot 1 - 1
set xtic 1440
set ytic 10
set title "Blood Glucose Rate to Time(min)"
set xlabel "Time (min)"
set ylabel "Glucose Rate (?)"
set xr [0: 43200]
set yr [0: 130]
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
set key out above right vert box 
plot  "../output/data/simv6/data.dat" using 1:3 with lines title 'Blood Glucose Rate' lt rgb "red", \
      "../output/data/simv6/data.dat" using 1:4 with lines title 'Glucose Increase from Food' lt rgb "blue"

# Plot 1 - 2
set xtic 1440
set ytic 1000
set title "Tissue Total Glucose to Time(min)"
set xlabel "Time (min)"
set ylabel "Glucose (?)"
set xr [0: 43200]
set yr [0: 15000]
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
set key out above right vert box 
plot  "../output/data/simv6/data.dat" using 1:5 with lines title 'Tissue Total Glucose' lt rgb "red"

# function for range day 15 to day 18
InRange(x)=((x>=(15*24*60)) ? ((x<=(18*24*60)) ? x : 1/0) : 1/0)

# Plot 2 - 1
set xtic 60
set ytic 10
set title "Blood Glucose Rate to Time(min) day 15-18"
set xlabel "Time (min)"
set ylabel "Glucose Rate (?)"
set xr [21600: 25920]
set yr [0: 130]
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
set key out above right vert box 
plot  "../output/data/simv6/data.dat" using (InRange($1)):3 with lines title 'Blood Glucose Rate' lt rgb "red", \
      "../output/data/simv6/data.dat" using (InRange($1)):4 with lines title 'Glucose Increase from Food' lt rgb "blue"

# Plot 2 - 2
set xtic 60
set ytic 1000
set title "Tissue Total Glucose to Time(min) day 15-18"
set xlabel "Time (min)"
set ylabel "Glucose (?)"
set xr [21600: 25920]
set yr [0: 15000]
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
set key out above right vert box 
plot  "../output/data/simv6/data.dat" using (InRange($1)):5 with lines title 'Tissue Total Glucose' lt rgb "red"