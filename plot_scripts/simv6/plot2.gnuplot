# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
unset key

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv6/plot2.svg"

# Plot 1 - 1
set xtic 1440
set ytic 10
set title "Blood Glucose Rate to Time(min)"
set xlabel "Time (min)"
set ylabel "Glucose Rate (?)"
set xr [0: 43200]
set yr [0: 130]
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
set key inside center right vert box 
plot  "../output/data/simv6/data.dat" using 1:3 with lines title 'Blood Glucose Rate' lt rgb "red", \
      "../output/data/simv6/data.dat" using 1:4 with lines title 'Glucose Increase from Food' lt rgb "blue"