# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,14" lw 1.2 background rgb 'white'
set output "../output/plot/simv4/plot.svg"

# Plot 1 - 1
set xtic 60                             # set xtics automatically
set ytic 10                             # set ytics automatically
set title "Glucose Rate to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Glucose Rate (?)"
set xr [0: 1440]
set yr [0: 130]
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
set key right box
plot  "../output/data/simv4/data.dat" using 1:3 with lines title 'Blood Glucose Rate' lt rgb "red", \
      "../output/data/simv4/data.dat" using 1:4 with lines title 'Glucose Increase from Food' lt rgb "blue"