# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# Commons
set key out vert right box
set offset .01,.01,.01,.01
set yrange [0 : *]
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
set tmargin 2

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv8/cells_0_2.svg"

# Multi plot layout
set multiplot layout 2, 1

# Plot 1 - 1
set title "Cell Cycle States [0, 2) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Cell Count"
plot  "../output/data/simv8/cells.dat" using 1:3 with lines title 'G1' lt rgb "#1b9eff", \
      "../output/data/simv8/cells.dat" using 1:4 with lines title 'S' lt rgb "#d95f02", \
      "../output/data/simv8/cells.dat" using 1:5 with lines title 'G2' lt rgb "#7570b3", \
      "../output/data/simv8/cells.dat" using 1:6 with lines title 'M' lt rgb "#e7298a", \
      "../output/data/simv8/cells.dat" using 1:7 with lines title 'G0' lt rgb "#66a61e", \
      "../output/data/simv8/cells.dat" using 1:2 with lines title 'Total' lt rgb "#e6ab02"

# Plot 1 - 2
set title "Contents of cells in distance [0, 2) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Mean in Cells (?)"
plot  "../output/data/simv8/cells.dat" using 1:8 with lines title 'Glucose' lt rgb "#1b9e77", \
      "../output/data/simv8/cells.dat" using 1:9 with lines title 'Glutamine' lt rgb "#d95f02", \
      "../output/data/simv8/cells.dat" using 1:10 with lines title 'Lactate' lt rgb "#7570b3", \
      "../output/data/simv8/cells.dat" using 1:11 with lines title 'Oxygen' lt rgb "#e7298a", \
      "../output/data/simv8/cells.dat" using 1:12 with lines title 'Pyruvate' lt rgb "#66a61e", \
      "../output/data/simv8/cells.dat" using 1:13 with lines title 'Nucleotide' lt rgb "#e6ab02", \
      "../output/data/simv8/cells.dat" using 1:14 with lines title 'ProteinLipid' lt rgb "#a6761d", \
      "../output/data/simv8/cells.dat" using 1:15 with lines title 'ATP' lt rgb "#666666"

# unset
unset multiplot

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv8/cells_2_4.svg"

# Multi plot layout
set multiplot layout 2, 1

# Plot 1 - 1
set title "Cell Cycle States [2, 4) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Cell Count"
plot  "../output/data/simv8/cells.dat" using 1:17 with lines title 'G1' lt rgb "#1b9eff", \
      "../output/data/simv8/cells.dat" using 1:18 with lines title 'S' lt rgb "#d95f02", \
      "../output/data/simv8/cells.dat" using 1:19 with lines title 'G2' lt rgb "#7570b3", \
      "../output/data/simv8/cells.dat" using 1:20 with lines title 'M' lt rgb "#e7298a", \
      "../output/data/simv8/cells.dat" using 1:21 with lines title 'G0' lt rgb "#66a61e", \
      "../output/data/simv8/cells.dat" using 1:16 with lines title 'Total' lt rgb "#e6ab02"

# Plot 1 - 2
set title "Contents of cells in distance [2, 4) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Mean in Cells (?)"
plot  "../output/data/simv8/cells.dat" using 1:22 with lines title 'Glucose' lt rgb "#1b9e77", \
      "../output/data/simv8/cells.dat" using 1:23 with lines title 'Glutamine' lt rgb "#d95f02", \
      "../output/data/simv8/cells.dat" using 1:24 with lines title 'Lactate' lt rgb "#7570b3", \
      "../output/data/simv8/cells.dat" using 1:25 with lines title 'Oxygen' lt rgb "#e7298a", \
      "../output/data/simv8/cells.dat" using 1:26 with lines title 'Pyruvate' lt rgb "#66a61e", \
      "../output/data/simv8/cells.dat" using 1:27 with lines title 'Nucleotide' lt rgb "#e6ab02", \
      "../output/data/simv8/cells.dat" using 1:28 with lines title 'ProteinLipid' lt rgb "#a6761d", \
      "../output/data/simv8/cells.dat" using 1:29 with lines title 'ATP' lt rgb "#666666"

# unset
unset multiplot

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv8/cells_4_6.svg"

# Multi plot layout
set multiplot layout 2, 1

# Plot 1 - 1
set title "Cell Cycle States [4, 6) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Cell Count"
plot  "../output/data/simv8/cells.dat" using 1:31 with lines title 'G1' lt rgb "#1b9eff", \
      "../output/data/simv8/cells.dat" using 1:32 with lines title 'S' lt rgb "#d95f02", \
      "../output/data/simv8/cells.dat" using 1:33 with lines title 'G2' lt rgb "#7570b3", \
      "../output/data/simv8/cells.dat" using 1:34 with lines title 'M' lt rgb "#e7298a", \
      "../output/data/simv8/cells.dat" using 1:35 with lines title 'G0' lt rgb "#66a61e", \
      "../output/data/simv8/cells.dat" using 1:30 with lines title 'Total' lt rgb "#e6ab02"

# Plot 1 - 2
set title "Contents of cells in distance [4, 6) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Mean in Cells (?)"
plot  "../output/data/simv8/cells.dat" using 1:36 with lines title 'Glucose' lt rgb "#1b9e77", \
      "../output/data/simv8/cells.dat" using 1:37 with lines title 'Glutamine' lt rgb "#d95f02", \
      "../output/data/simv8/cells.dat" using 1:38 with lines title 'Lactate' lt rgb "#7570b3", \
      "../output/data/simv8/cells.dat" using 1:39 with lines title 'Oxygen' lt rgb "#e7298a", \
      "../output/data/simv8/cells.dat" using 1:40 with lines title 'Pyruvate' lt rgb "#66a61e", \
      "../output/data/simv8/cells.dat" using 1:41 with lines title 'Nucleotide' lt rgb "#e6ab02", \
      "../output/data/simv8/cells.dat" using 1:42 with lines title 'ProteinLipid' lt rgb "#a6761d", \
      "../output/data/simv8/cells.dat" using 1:43 with lines title 'ATP' lt rgb "#666666"

# unset
unset multiplot

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv8/cells_6_8.svg"

# Multi plot layout
set multiplot layout 2, 1

# Plot 1 - 1
set title "Cell Cycle States [6, 8) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Cell Count"
plot  "../output/data/simv8/cells.dat" using 1:45 with lines title 'G1' lt rgb "#1b9eff", \
      "../output/data/simv8/cells.dat" using 1:46 with lines title 'S' lt rgb "#d95f02", \
      "../output/data/simv8/cells.dat" using 1:47 with lines title 'G2' lt rgb "#7570b3", \
      "../output/data/simv8/cells.dat" using 1:48 with lines title 'M' lt rgb "#e7298a", \
      "../output/data/simv8/cells.dat" using 1:49 with lines title 'G0' lt rgb "#66a61e", \
      "../output/data/simv8/cells.dat" using 1:44 with lines title 'Total' lt rgb "#e6ab02"

# Plot 1 - 2
set title "Contents of cells in distance [6, 8) to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Mean in Cells (?)"
plot  "../output/data/simv8/cells.dat" using 1:50 with lines title 'Glucose' lt rgb "#1b9e77", \
      "../output/data/simv8/cells.dat" using 1:51 with lines title 'Glutamine' lt rgb "#d95f02", \
      "../output/data/simv8/cells.dat" using 1:52 with lines title 'Lactate' lt rgb "#7570b3", \
      "../output/data/simv8/cells.dat" using 1:53 with lines title 'Oxygen' lt rgb "#e7298a", \
      "../output/data/simv8/cells.dat" using 1:54 with lines title 'Pyruvate' lt rgb "#66a61e", \
      "../output/data/simv8/cells.dat" using 1:55 with lines title 'Nucleotide' lt rgb "#e6ab02", \
      "../output/data/simv8/cells.dat" using 1:56 with lines title 'ProteinLipid' lt rgb "#a6761d", \
      "../output/data/simv8/cells.dat" using 1:57 with lines title 'ATP' lt rgb "#666666"

# unset
unset multiplot

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv8/cells_8_10.svg"

# Multi plot layout
set multiplot layout 2, 1

# Plot 1 - 1
set title "Cell Cycle States [8, 10] to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Cell Count"
plot  "../output/data/simv8/cells.dat" using 1:59 with lines title 'G1' lt rgb "#1b9eff", \
      "../output/data/simv8/cells.dat" using 1:60 with lines title 'S' lt rgb "#d95f02", \
      "../output/data/simv8/cells.dat" using 1:61 with lines title 'G2' lt rgb "#7570b3", \
      "../output/data/simv8/cells.dat" using 1:62 with lines title 'M' lt rgb "#e7298a", \
      "../output/data/simv8/cells.dat" using 1:63 with lines title 'G0' lt rgb "#66a61e", \
      "../output/data/simv8/cells.dat" using 1:58 with lines title 'Total' lt rgb "#e6ab02"

# Plot 1 - 2
set title "Contents of cells in distance [8, 10] to Time in Day(min)"
set xlabel "Time (min)"
set ylabel "Mean in Cells (?)"
plot  "../output/data/simv8/cells.dat" using 1:64 with lines title 'Glucose' lt rgb "#1b9e77", \
      "../output/data/simv8/cells.dat" using 1:65 with lines title 'Glutamine' lt rgb "#d95f02", \
      "../output/data/simv8/cells.dat" using 1:66 with lines title 'Lactate' lt rgb "#7570b3", \
      "../output/data/simv8/cells.dat" using 1:67 with lines title 'Oxygen' lt rgb "#e7298a", \
      "../output/data/simv8/cells.dat" using 1:68 with lines title 'Pyruvate' lt rgb "#66a61e", \
      "../output/data/simv8/cells.dat" using 1:69 with lines title 'Nucleotide' lt rgb "#e6ab02", \
      "../output/data/simv8/cells.dat" using 1:70 with lines title 'ProteinLipid' lt rgb "#a6761d", \
      "../output/data/simv8/cells.dat" using 1:71 with lines title 'ATP' lt rgb "#666666"
