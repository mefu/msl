# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,14" lw 1.2 background rgb 'white'
set output "../output/plot/simv8/cell.svg"

# Multi plot layout
set multiplot layout 3, 3 title "Cell Substance Concentrations" font ",14"
set tmargin 2
unset key
set offset .01,.01,.01,.01
set yrange [0 : *]

# Plot 1 - 1
set xlabel "Time (min)"
set ylabel "Glucose in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot  "../output/data/simv8/cell.dat" using 1:3 with lines title 'Glucose' lt rgb "red"

# Plot 1 - 2
set xlabel "Time (min)"
set ylabel "Glutamine in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot  "../output/data/simv8/cell.dat" using 1:4 with lines title 'Glutamine' lt rgb "blue"

# Plot 1 - 3
set xlabel "Time (min)"
set ylabel "Lactate in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot  "../output/data/simv8/cell.dat" using 1:5 with lines title 'Lactate' lt rgb "red"

# Plot 2 - 2
set xlabel "Time (min)"
set ylabel "Oxygen in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot  "../output/data/simv8/cell.dat" using 1:6 with lines title 'Oxygen' lt rgb "blue"

# Plot 2 - 2
set xlabel "Time (min)"
set ylabel "Pyruvate in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot  "../output/data/simv8/cell.dat" using 1:7 with lines title 'Pyruvate' lt rgb "red"

# Plot 2 - 3
set xlabel "Time (min)"
set ylabel "Nucleotide in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot  "../output/data/simv8/cell.dat" using 1:8 with lines title 'Nucleotide' lt rgb "blue"

# Plot 3 - 1
set xlabel "Time (min)"
set ylabel "ProteinLipid in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot  "../output/data/simv8/cell.dat" using 1:9 with lines title 'ProteinLipid' lt rgb "red"

# Plot 3 - 2
set xlabel "Time (min)"
set ylabel "ATP in Cell (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot  "../output/data/simv8/cell.dat" using 1:10 with lines title 'ATP' lt rgb "blue"

# Plot 3 - 3
set xlabel "Time (min)"
set ylabel "Cell Cycle Phase"
set yrange [1: 5]
set ytic 1
set ytics add ("G1" 1)
set ytics add ("S" 2)
set ytics add ("G2" 3)
set ytics add ("M" 4)
set ytics add ("G0" 5)
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
plot  "../output/data/simv8/cell.dat" using 1:11 with lines title 'Cell Cycle Phase' lt rgb "red"

