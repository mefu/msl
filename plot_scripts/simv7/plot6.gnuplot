# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,14" lw 1.2 background rgb 'white'
set output "../output/plot/simv7/plot6.svg"

# Multi plot layout
set multiplot layout 2, 1 title "Circadian Rhythm" font ",14"
set tmargin 2
set key out vert right box

# Plot 1 - 1
set title "Numbers of cells in specific cycles - 30 days"
set xlabel "Time (min)"
set ylabel "Number of cells"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
plot  "../output/data/simv7/data.dat" using 1:8 with lines title 'G1' lt rgb "#e41a1c", \
      "../output/data/simv7/data.dat" using 1:9 with lines title 'S' lt rgb "#377eb8", \
      "../output/data/simv7/data.dat" using 1:10 with lines title 'G2' lt rgb "#4daf4a", \
      "../output/data/simv7/data.dat" using 1:11 with lines title 'M' lt rgb "#984ea3", \
      "../output/data/simv7/data.dat" using 1:7 with lines title 'Total' lt rgb "#ff7f00"


# function for range day 15 to day 18
InRange(x)=((x>=(15*24*60)) ? ((x<=(18*24*60)) ? x : 1/0) : 1/0)

# Plot 2 - 1
set title "Numbers of cells in specific cycles - Day 15 to 18"
set xlabel "Time (min)"
set ylabel "Number of cells"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
plot  "../output/data/simv7/data.dat" using (InRange($1)):8 with lines title 'G1' lt rgb "#e41a1c", \
      "../output/data/simv7/data.dat" using (InRange($1)):9 with lines title 'S' lt rgb "#377eb8", \
      "../output/data/simv7/data.dat" using (InRange($1)):10 with lines title 'G2' lt rgb "#4daf4a", \
      "../output/data/simv7/data.dat" using (InRange($1)):11 with lines title 'M' lt rgb "#984ea3", \
      "../output/data/simv7/data.dat" using (InRange($1)):7 with lines title 'Total' lt rgb "#ff7f00"