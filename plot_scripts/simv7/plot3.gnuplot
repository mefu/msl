# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels
unset key

# output
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,24" lw 1.2 background rgb 'white'
set output "../output/plot/simv7/plot3.svg"

# Plot 1 - 1
set title "Tissue Total Glucose to Time(min)"
set xlabel "Time (min)"
set ylabel "Glucose (?)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
set xtics rotate
set key inside top right vert box 
plot  "../output/data/simv7/data.dat" using 1:5 with lines title 'Tissue Total Glucose' lt rgb "red"