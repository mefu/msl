# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# output
set terminal png size 1920,1080 enhanced font "Helvetica,32" lw 2 background rgb 'white'
set output "../output/plot/simv1/plot2.png"

#set key out vert right box
unset key

# Plot 1 - 1
set xr [0: 1440*3]
set yr [0: 1]
#set xtic 120
#set ytic 0.1
#set xlabel "Time (min)"
#set ylabel "Concentration (%/100)"
set grid ytics lt 0 lw 1 lc rgb "#666666"
set grid xtics lt 0 lw 1 lc rgb "#666666"
#set xtics rotate
plot  "../output/data/simv1/data.dat" using 1:6 with lines title 'C-myc' lw 2 lt rgb "red", \
      "../output/data/simv1/data.dat" using 1:7 with lines title 'Wee1' lw 2 lt rgb "blue"