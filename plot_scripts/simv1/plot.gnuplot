# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# Save to file
set terminal svg size 1920,1080 dynamic enhanced font "Helvetica,14" lw 1 background rgb 'white'
set output "../output/plot/simv1/plot.svg"

# Multi plot layout
set multiplot layout 2, 2 title "Circadian Rhythm" font ",14"
set tmargin 2
unset key

# Plot 1
set title "Circadian Time to Time"
set xtic 1
set ytic 2
set xlabel "Time (day)"
set ylabel "Circadian time (?)"
set key right box
set xr [0: 10]
set yr [0: 24]
plot  "../output/data/simv1/data.dat" using 2:4 with lines title 'Circ to Time' lt rgb "blue"

# Plot 2
set title "Circadian Phase to Time"
set xtic 1
set ytic 12
set xlabel "Time (day)"
set ylabel "Circadian Phase (?)"
set key right box
set xr [0: 10]
set yr [0: 72]
plot  "../output/data/simv1/data.dat" using 2:3 with lines title 'Phase to Time' lt rgb "red"

# Plot 2
set title "Phase Sensitivity to Time"
set xtic 1
set ytic 0.1
set xlabel "Time (day)"
set ylabel "Phase Sensitivity(?)"
set key right box
set xr [0: 10]
set yr [-0.5: 1.3]
plot  "../output/data/simv1/data.dat" using 2:5 with lines title 'Sensitivity' lt rgb "red"

# Plot 4
set title "Per1-Wee1 to Time"
set xtic 720
set ytic 0.1
set xlabel "Time (min)"
set ylabel "Concentration (%/100)"
set key right box
set xr [0: 14400]
set yr [0: 1]
set grid ytics lt 0 lw 1 lc rgb "#bbbbbb"
set grid xtics lt 0 lw 1 lc rgb "#bbbbbb"
plot  "../output/data/simv1/data.dat" using 1:6 with lines title 'Per1' lt rgb "red", \
      "../output/data/simv1/data.dat" using 1:7 with lines title 'Wee1' lt rgb "blue"

# unset multiplot
unset multiplot