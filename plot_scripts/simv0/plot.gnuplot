# Gnuplot script file
set   autoscale                        # scale axes automatically
unset log                              # remove any log-scaling
unset label                            # remove any previous labels

# output
set terminal svg size 1200,768 dynamic enhanced font "Helvetica,14" lw 1 background rgb 'white'
set output "../output/plot/simv0/plot.svg"

# Plot 1
set xtic 3960                           # set xtics automatically
set ytic 1000                           # set ytics automatically
set title "Numbers of cells in specific cycles"
set xlabel "Time (min)"
set ylabel "Number of cells"
set key right box
set xr [-10: 40000]
set yr [-10: 2000]
plot  "../output/data/simv0/data.dat" using 1:2 with lines title 'G1' lt rgb "#e41a1c", \
      "../output/data/simv0/data.dat" using 1:3 with lines title 'S' lt rgb "#377eb8", \
      "../output/data/simv0/data.dat" using 1:4 with lines title 'G2' lt rgb "#4daf4a", \
      "../output/data/simv0/data.dat" using 1:5 with lines title 'M' lt rgb "#984ea3"