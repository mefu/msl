# Mefu Simulation Library (MSL)

This project was first intended to be a library for agent based simulations, just ended up as a huge repo for my research code. Excuse my commit messages and lack of comments in code. I was not intending this repo to be public or shared with someone else.

## Projects

### core
Includes specialized data structures, specialized functions, geometric shapes and randomizer.

### linalg
Includes implementation of tridiagonal matrix algorithm, 2d convolution and f-factod ADI 3d diffusion solver.

### benchmark
Benchmarks of specialized data structure implementations and algorithms in linalg project.

### modeling
Transformation interface and runner for simulations. Includes custom function interface for steps taking a context, modifying it and returning new context. Also includes common actions and data classes.

### slwjgl
Some stuff for easy use of LWJGL in Scala.

### cca
Cell-cycle automaton implementations. Each version has different transformation functions, data classes and parameters.

### vis
Visualizations of simulations in CCA.

### plot_scripts
Scripts for plotting output data of simulations.

## How to run
```
$ sbt
# simulations
sbt> project cca
sbt:msl-cca> run
# visualizations
sbt> project vis
sbt:msl-vis> run
# benchmarks
sbt> project benchmark
sbt:msl-benchmark> jmh:run
```

## Benchmarks
Output of benchmarks on my laptop. Benchmarks ending with `custom` use specialized `Vector`, `Matrix` and `Matrix3D` data structures implemented in this project.

```
[info] Benchmark                                                                (m)      (n)  (t)  Mode  Cnt     Score      Error  Units
[info] c.m.m.b.core.array.CForLoopBenchmark.loop1                               N/A      100  N/A  avgt   20     0.001 ±    0.001  ms/op
[info] c.m.m.b.core.array.CForLoopBenchmark.loop2                               N/A      100  N/A  avgt   20     0.140 ±    0.230  ms/op
[info] c.m.m.b.core.array.CForLoopBenchmark.loop3                               N/A      100  N/A  avgt   20     8.159 ±    0.665  ms/op
[info] c.m.m.b.core.array.ForLoopBenchmark.loop1                                N/A      100  N/A  avgt   20     0.001 ±    0.001  ms/op
[info] c.m.m.b.core.array.ForLoopBenchmark.loop2                                N/A      100  N/A  avgt   20     0.168 ±    0.142  ms/op
[info] c.m.m.b.core.array.ForLoopBenchmark.loop3                                N/A      100  N/A  avgt   20     8.467 ±    0.949  ms/op
[info] c.m.m.b.core.array.WhileLoopBenchmark.loop1                              N/A      100  N/A  avgt   20     0.009 ±    0.032  ms/op
[info] c.m.m.b.core.array.WhileLoopBenchmark.loop2                              N/A      100  N/A  avgt   20     0.071 ±    0.014  ms/op
[info] c.m.m.b.core.array.WhileLoopBenchmark.loop3                              N/A      100  N/A  avgt   20     8.850 ±    0.984  ms/op
[info] c.m.m.b.core.array.a1d.Array1DAccess.accessICustom                       N/A  1000000  N/A  avgt   20     2.233 ±    1.213  ms/op
[info] c.m.m.b.core.array.a1d.Array1DAccess.accessIStd                          N/A  1000000  N/A  avgt   20     1.939 ±    0.778  ms/op
[info] c.m.m.b.core.array.a1d.Array1DAccess.accessRandomCustom                  N/A  1000000  N/A  avgt   20    13.564 ±    1.390  ms/op
[info] c.m.m.b.core.array.a1d.Array1DAccess.accessRandomStd                     N/A  1000000  N/A  avgt   20    13.482 ±    1.532  ms/op
[info] c.m.m.b.core.array.a1d.Array1DMethods.foreachCustom                      N/A  1000000  N/A  avgt   20     1.859 ±    0.644  ms/op
[info] c.m.m.b.core.array.a1d.Array1DMethods.foreachStd                         N/A  1000000  N/A  avgt   20     2.001 ±    0.861  ms/op
[info] c.m.m.b.core.array.a1d.Array1DMethods.sumCustom                          N/A  1000000  N/A  avgt   20     2.466 ±    1.279  ms/op
[info] c.m.m.b.core.array.a1d.Array1DMethods.sumStd                             N/A  1000000  N/A  avgt   20     8.992 ±    3.470  ms/op
[info] c.m.m.b.core.array.a1d.Array1DMethods.transformCustom                    N/A  1000000  N/A  avgt   20     2.413 ±    1.100  ms/op
[info] c.m.m.b.core.array.a1d.Array1DMethods.transformStd                       N/A  1000000  N/A  avgt   20     2.519 ±    1.335  ms/op
[info] c.m.m.b.core.array.a1d.Array1DMethods.varianceCustom                     N/A  1000000  N/A  avgt   20     3.592 ±    1.237  ms/op
[info] c.m.m.b.core.array.a1d.Array1DMethods.varianceStd                        N/A  1000000  N/A  avgt   20    24.879 ±    9.843  ms/op
[info] c.m.m.b.core.array.a1d.Array1DUpdate.updateICustom                       N/A  1000000  N/A  avgt   20     2.347 ±    0.066  ms/op
[info] c.m.m.b.core.array.a1d.Array1DUpdate.updateIStd                          N/A  1000000  N/A  avgt   20     2.334 ±    0.054  ms/op
[info] c.m.m.b.core.array.a1d.Array1DUpdate.updateRandomCustom                  N/A  1000000  N/A  avgt   20    13.337 ±    1.320  ms/op
[info] c.m.m.b.core.array.a1d.Array1DUpdate.updateRandomStd                     N/A  1000000  N/A  avgt   20    13.053 ±    1.210  ms/op
[info] c.m.m.b.core.array.a2d.Array2DAccess.accessIJCustom                      N/A     1000  N/A  avgt   20     1.895 ±    0.729  ms/op
[info] c.m.m.b.core.array.a2d.Array2DAccess.accessIJCustomSub                   N/A     1000  N/A  avgt   20     1.937 ±    0.904  ms/op
[info] c.m.m.b.core.array.a2d.Array2DAccess.accessIJStd                         N/A     1000  N/A  avgt   20     1.862 ±    0.926  ms/op
[info] c.m.m.b.core.array.a2d.Array2DAccess.accessJICustom                      N/A     1000  N/A  avgt   20     3.156 ±    0.974  ms/op
[info] c.m.m.b.core.array.a2d.Array2DAccess.accessJICustomSub                   N/A     1000  N/A  avgt   20     3.482 ±    1.136  ms/op
[info] c.m.m.b.core.array.a2d.Array2DAccess.accessJIStd                         N/A     1000  N/A  avgt   20     7.341 ±    1.425  ms/op
[info] c.m.m.b.core.array.a2d.Array2DAccess.accessRandomCustom                  N/A     1000  N/A  avgt   20    16.151 ±    1.434  ms/op
[info] c.m.m.b.core.array.a2d.Array2DAccess.accessRandomCustomSub               N/A     1000  N/A  avgt   20     2.901 ±    1.166  ms/op
[info] c.m.m.b.core.array.a2d.Array2DAccess.accessRandomStd                     N/A     1000  N/A  avgt   20    18.451 ±    2.032  ms/op
[info] c.m.m.b.core.array.a2d.Array2DMethods.foreachCustom                      N/A     1000  N/A  avgt   20     1.370 ±    0.041  ms/op
[info] c.m.m.b.core.array.a2d.Array2DMethods.foreachStd                         N/A     1000  N/A  avgt   20    10.049 ±    3.418  ms/op
[info] c.m.m.b.core.array.a2d.Array2DMethods.transformCustom                    N/A     1000  N/A  avgt   20     1.504 ±    0.074  ms/op
[info] c.m.m.b.core.array.a2d.Array2DMethods.transformStd                       N/A     1000  N/A  avgt   20    20.846 ±    7.256  ms/op
[info] c.m.m.b.core.array.a2d.Array2DUpdate.updateIJCustom                      N/A     1000  N/A  avgt   20     2.374 ±    0.117  ms/op
[info] c.m.m.b.core.array.a2d.Array2DUpdate.updateIJCustomSub                   N/A     1000  N/A  avgt   20     2.408 ±    0.099  ms/op
[info] c.m.m.b.core.array.a2d.Array2DUpdate.updateIJStd                         N/A     1000  N/A  avgt   20     2.452 ±    0.063  ms/op
[info] c.m.m.b.core.array.a2d.Array2DUpdate.updateJICustom                      N/A     1000  N/A  avgt   20    11.700 ±    0.400  ms/op
[info] c.m.m.b.core.array.a2d.Array2DUpdate.updateJICustomSub                   N/A     1000  N/A  avgt   20    12.981 ±    1.181  ms/op
[info] c.m.m.b.core.array.a2d.Array2DUpdate.updateJIStd                         N/A     1000  N/A  avgt   20    21.784 ±    0.984  ms/op
[info] c.m.m.b.core.array.a2d.Array2DUpdate.updateRandomCustom                  N/A     1000  N/A  avgt   20    28.631 ±    0.813  ms/op
[info] c.m.m.b.core.array.a2d.Array2DUpdate.updateRandomStd                     N/A     1000  N/A  avgt   20    33.208 ±    0.726  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessIJKCustom                     N/A      100  N/A  avgt   20     2.058 ±    0.721  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessIJKCustomSub                  N/A      100  N/A  avgt   20     2.531 ±    1.011  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessIJKStd                        N/A      100  N/A  avgt   20     2.847 ±    1.105  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessIKJCustom                     N/A      100  N/A  avgt   20     2.817 ±    0.834  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessIKJCustomSub                  N/A      100  N/A  avgt   20     3.622 ±    1.336  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessIKJStd                        N/A      100  N/A  avgt   20     5.406 ±    1.384  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessJIKCustom                     N/A      100  N/A  avgt   20     2.983 ±    1.512  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessJIKCustomSub                  N/A      100  N/A  avgt   20     2.739 ±    1.003  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessJIKStd                        N/A      100  N/A  avgt   20     3.323 ±    1.180  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessJKICustom                     N/A      100  N/A  avgt   20     3.638 ±    0.979  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessJKICustomSub                  N/A      100  N/A  avgt   20     3.983 ±    1.187  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessJKIStd                        N/A      100  N/A  avgt   20     7.354 ±    0.879  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessKIJCustom                     N/A      100  N/A  avgt   20     6.681 ±    2.113  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessKIJCustomSub                  N/A      100  N/A  avgt   20     6.359 ±    1.744  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessKIJStd                        N/A      100  N/A  avgt   20    12.241 ±    1.819  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessKJICustom                     N/A      100  N/A  avgt   20     5.382 ±    1.188  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessKJICustomSub                  N/A      100  N/A  avgt   20     6.559 ±    1.657  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessKJIStd                        N/A      100  N/A  avgt   20    14.397 ±    1.085  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessRandomCustom                  N/A      100  N/A  avgt   20    19.570 ±    1.286  ms/op
[info] c.m.m.b.core.array.a3d.Array3DAccess.accessRandomStd                     N/A      100  N/A  avgt   20    26.908 ±    1.853  ms/op
[info] c.m.m.b.core.array.a3d.Array3DMethods.foreachCustom                      N/A      100  N/A  avgt   20     1.565 ±    0.143  ms/op
[info] c.m.m.b.core.array.a3d.Array3DMethods.foreachStd                         N/A      100  N/A  avgt   20    12.335 ±    4.116  ms/op
[info] c.m.m.b.core.array.a3d.Array3DMethods.transformCustom                    N/A      100  N/A  avgt   20     1.464 ±    0.056  ms/op
[info] c.m.m.b.core.array.a3d.Array3DMethods.transformStd                       N/A      100  N/A  avgt   20    10.199 ±    4.134  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateIJKCustom                     N/A      100  N/A  avgt   20     2.692 ±    0.086  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateIJKStd                        N/A      100  N/A  avgt   20     3.065 ±    0.164  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateIKJCustom                     N/A      100  N/A  avgt   20     4.295 ±    0.442  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateIKJStd                        N/A      100  N/A  avgt   20     4.989 ±    0.399  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateJIKCustom                     N/A      100  N/A  avgt   20     3.022 ±    0.085  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateJIKStd                        N/A      100  N/A  avgt   20     3.375 ±    0.161  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateJKICustom                     N/A      100  N/A  avgt   20     6.795 ±    0.172  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateJKIStd                        N/A      100  N/A  avgt   20     9.908 ±    0.230  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateKIJCustom                     N/A      100  N/A  avgt   20    26.535 ±    0.833  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateKIJStd                        N/A      100  N/A  avgt   20    38.352 ±    4.585  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateKJICustom                     N/A      100  N/A  avgt   20    23.601 ±    1.402  ms/op
[info] c.m.m.b.core.array.a3d.Array3DUpdate.updateKJIStd                        N/A      100  N/A  avgt   20    40.652 ±    4.307  ms/op
[info] c.m.m.b.core.array.collection.CollectionsAccess.accessIArray             N/A  1000000  N/A  avgt   20     1.788 ±    0.631  ms/op
[info] c.m.m.b.core.array.collection.CollectionsAccess.accessIArrayBuffer       N/A  1000000  N/A  avgt   20     8.605 ±    1.592  ms/op
[info] c.m.m.b.core.array.collection.CollectionsAccess.accessIBuffer            N/A  1000000  N/A  avgt   20     8.435 ±    1.472  ms/op
[info] c.m.m.b.core.array.collection.CollectionsAccess.accessISPVec             N/A  1000000  N/A  avgt   20     2.292 ±    0.860  ms/op
[info] c.m.m.b.core.array.collection.CollectionsAccess.accessIVector            N/A  1000000  N/A  avgt   20    11.543 ±    2.019  ms/op
[info] c.m.m.b.core.array.collection.CollectionsAccess.accessRandomArray        N/A  1000000  N/A  avgt   20    13.015 ±    1.283  ms/op
[info] c.m.m.b.core.array.collection.CollectionsAccess.accessRandomArrayBuffer  N/A  1000000  N/A  avgt   20    30.712 ±    1.562  ms/op
[info] c.m.m.b.core.array.collection.CollectionsAccess.accessRandomBuffer       N/A  1000000  N/A  avgt   20    27.920 ±    2.249  ms/op
[info] c.m.m.b.core.array.collection.CollectionsAccess.accessRandomSPVec        N/A  1000000  N/A  avgt   20     9.865 ±    0.513  ms/op
[info] c.m.m.b.core.array.collection.CollectionsAccess.accessRandomVector       N/A  1000000  N/A  avgt   20    68.187 ±    1.471  ms/op
[info] c.m.m.b.core.math.TriDiagonalSolverBenchmark.tdma                        N/A       50  N/A  avgt   20     0.105 ±    0.067  ms/op
[info] c.m.m.b.core.math.TriDiagonalSolverBenchmark.tdma                        N/A      100  N/A  avgt   20     0.150 ±    0.078  ms/op
[info] c.m.m.b.core.math.TriDiagonalSolverBenchmark.tdma                        N/A      200  N/A  avgt   20     0.384 ±    0.084  ms/op
[info] c.m.m.b.core.math.TriDiagonalSolverBenchmark.tdmaI                       N/A       50  N/A  avgt   20     0.038 ±    0.028  ms/op
[info] c.m.m.b.core.math.TriDiagonalSolverBenchmark.tdmaI                       N/A      100  N/A  avgt   20     0.098 ±    0.049  ms/op
[info] c.m.m.b.core.math.TriDiagonalSolverBenchmark.tdmaI                       N/A      200  N/A  avgt   20     0.242 ±    0.107  ms/op
[info] c.m.m.b.linalg.diff.Diffusion3DBenchmark.d3dFFADI                        N/A       20   10  avgt    5    14.585 ±   14.510  ms/op
[info] c.m.m.b.linalg.diff.Diffusion3DBenchmark.d3dFFADI                        N/A       20   50  avgt    5    45.673 ±   53.315  ms/op
[info] c.m.m.b.linalg.diff.Diffusion3DBenchmark.d3dFFADI                        N/A       20  100  avgt    5    77.148 ±    9.498  ms/op
[info] c.m.m.b.linalg.diff.Diffusion3DBenchmark.d3dFFADI                        N/A       50   10  avgt    5   119.336 ±   11.806  ms/op
[info] c.m.m.b.linalg.diff.Diffusion3DBenchmark.d3dFFADI                        N/A       50   50  avgt    5   532.929 ±   28.670  ms/op
[info] c.m.m.b.linalg.diff.Diffusion3DBenchmark.d3dFFADI                        N/A       50  100  avgt    5  1099.833 ±  161.928  ms/op
[info] c.m.m.b.linalg.diff.Diffusion3DBenchmark.d3dFFADI                        N/A      100   10  avgt    5   849.648 ±   13.227  ms/op
[info] c.m.m.b.linalg.diff.Diffusion3DBenchmark.d3dFFADI                        N/A      100   50  avgt    5  4216.233 ±  125.502  ms/op
[info] c.m.m.b.linalg.diff.Diffusion3DBenchmark.d3dFFADI                        N/A      100  100  avgt    5  8849.777 ± 1544.851  ms/op
[info] c.m.m.b.linalg.signal.Convolution2DBenchmark.conv2d                        3       50  N/A  avgt   20     1.317 ±    0.991  ms/op
[info] c.m.m.b.linalg.signal.Convolution2DBenchmark.conv2d                        3      100  N/A  avgt   20     0.687 ±    0.186  ms/op
[info] c.m.m.b.linalg.signal.Convolution2DBenchmark.conv2d                        5       50  N/A  avgt   20     0.303 ±    0.053  ms/op
[info] c.m.m.b.linalg.signal.Convolution2DBenchmark.conv2d                        5      100  N/A  avgt   20     1.108 ±    0.141  ms/op
[info] c.m.m.b.linalg.signal.Convolution2DBenchmark.conv2dI                       3       50  N/A  avgt   20     0.569 ±    0.671  ms/op
[info] c.m.m.b.linalg.signal.Convolution2DBenchmark.conv2dI                       3      100  N/A  avgt   20     0.477 ±    0.104  ms/op
[info] c.m.m.b.linalg.signal.Convolution2DBenchmark.conv2dI                       5       50  N/A  avgt   20     0.472 ±    0.417  ms/op
[info] c.m.m.b.linalg.signal.Convolution2DBenchmark.conv2dI                       5      100  N/A  avgt   20     1.024 ±    0.113  ms/op
```
