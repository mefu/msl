package co.mefu.msl.cca.data

case class Time(var minute: Int) {
  override def toString: String = minute.toString
}