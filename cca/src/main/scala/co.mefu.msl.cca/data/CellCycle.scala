package co.mefu.msl.cca.data

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

object CellCycle {

  sealed trait CellCycleState
  case object G1 extends CellCycleState
  case object S extends CellCycleState
  case object G2 extends CellCycleState
  case object M extends CellCycleState
  case object G0 extends CellCycleState
  case object MOTILE extends CellCycleState

  object CellCycleState {
    def next(state: CellCycleState): CellCycleState =
      state match {
        case G1 => S
        case S => G2
        case G2 => M
        case M => G1
        case _ => state
      }

    def toInt(state: CellCycleState): Int =
      state match {
        case G1 => 1
        case S => 2
        case G2 => 3
        case M => 4
        case G0 => 5
        case MOTILE => 6
      }
  }

  case class CellCycleDurations(val durG1: Double,
                                val durS: Double,
                                val durG2: Double,
                                val durM: Double) {
    val total: Double = (durG1 + durS + durG2 + durM)
    val inverseTotal: Double = 1 / total
    def getRandomized(state: CellCycleState, sway: Double = 0.2) =
      get(state) * (1 + (2 * Random.next[Double] - 1) * sway)

    def get(state: CellCycleState): Double =
      state match {
        case G1 => durG1
        case S => durS
        case G2 => durG2
        case M => durM
        case _ => 0
      }
  }

  object CellCycleDurations {

    val short: CellCycleDurations =
      CellCycleDurations(4 * 60, 11 * 60, 1 * 60, 1 * 60)
    val normal: CellCycleDurations =
      CellCycleDurations(9 * 60, 11 * 60, 1 * 60, 1 * 60)
    val long: CellCycleDurations =
      CellCycleDurations(14 * 60, 11 * 60, 1 * 60, 1 * 60)

  }
}
