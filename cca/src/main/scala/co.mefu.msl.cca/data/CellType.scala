package co.mefu.msl.cca.data

import co.mefu.msl.cca.data.CellCycle._

sealed trait CellType {
  def deathRate: Double
  def repairRate: Double
  def canPhaseResponse: Boolean
  def canMove: Boolean
  def ccd: CellCycleDurations
}

object CellType {

  case object NormalCT extends CellType {
    val deathRate: Double = 0.53
    val repairRate: Double = 0.2
    val canPhaseResponse: Boolean = true
    val canMove: Boolean = false
    val ccd: CellCycleDurations = CellCycleDurations.normal
  }

  case object CancerShortCT extends CellType {
    val deathRate: Double = 0.515
    val repairRate: Double = 0.2
    val canPhaseResponse: Boolean = false
    val canMove: Boolean = true
    val ccd: CellCycleDurations = CellCycleDurations.short
  }

  case object CancerLongCT extends CellType {
    val deathRate: Double = 0.535
    val repairRate: Double = 0.2
    val canPhaseResponse: Boolean = false
    val canMove: Boolean = true
    val ccd: CellCycleDurations = CellCycleDurations.long
  }

}
