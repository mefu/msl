package co.mefu.msl.cca.sim
package simv6

import java.io.PrintWriter

import scala.collection.mutable.Buffer

import spire.implicits._

import co.mefu.msl.core.Random

import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.ops.SPMat3DOps._
import co.mefu.msl.core.math.Fitter
import co.mefu.msl.linalg.diff.Diffusion3DFFactor

import co.mefu.msl.modeling._

import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

import Data._

object Action {

  case class CircadianTimeUpdate(val period: Double)
      extends Transformation[(Time, CircadianRhythm)] {

    def transform(tcr: (Time, CircadianRhythm)): Unit = {
      val (t, cr) = tcr
      cr.ts = t.minute % 1440
      cr.th = (t.minute % 1440) / 60
      cr.tc = (t.minute % 1440d) / 60d
    }

    override val toString: String = "CircadianTimeUpdate"

  }

  case class GlucoseUpdate(
      val basalGlucoseRate: Double,
      val feedingTimes: Array[Int],
      val glPeak: Double,
      val tpeak: Int,
      val tgl: Int
  ) extends Transformation[(Glucose, CircadianRhythm)] {

    var lastFt: Int = feedingTimes.max
    var lastGlc: Double = 0

    val points1 = Array[(Double, Double)](
      (0d, 0d),
      (tpeak * 0.25, glPeak * 0.5),
      (tpeak * 0.5, glPeak * 0.75),
      (tpeak * 0.75, glPeak * 0.88),
      (tpeak, glPeak)
    )

    val points2 = Array[(Double, Double)](
      (tpeak, glPeak),
      (tpeak + (tgl - tpeak) * 0.25, glPeak * 0.5),
      (tpeak + (tgl - tpeak) * 0.5, glPeak * 0.32),
      (tpeak + (tgl - tpeak) * 0.75, glPeak * 0.12),
      (tgl, 0d),
      (tgl * 1.1, -1 * glPeak * 0.02)
    )

    val xs1 = points1.map(_._1)
    val zs1 = points1.map(_._2)
    val xs2 = points2.map(_._1)
    val zs2 = points2.map(_._2)

    val poly1 = Fitter.fitPoly(3, xs1, zs1)
    val poly2 = Fitter.fitPoly(3, xs2, zs2)

    def transform(input: (Glucose, CircadianRhythm)): Unit = {
      val (glc, cr) = input

      if (feedingTimes.contains(cr.ts))
        lastFt = cr.ts

      val absTime = cr.ts - lastFt

      lastGlc = glc.inc
      glc.inc = absTime match {
        case x if x < 0 => 0
        case x if x > tgl => 0
        // case x => 1.776148528 * Math.pow(x, 1.064961569) * Math.exp(-1.831533454e-2*x)
        case x if x <= tpeak => Math.max(poly1(x), 0d)
        case x if x > tpeak => Math.max(poly2(x), 0d)
      }

      glc.blood = basalGlucoseRate + glc.inc
    }

    override val toString: String = "GlucoseUpdate"
  }

  case object Eject
      extends Transformation[(VesselCell, (Glucose, SPMat3D[Double]))] {
    def transform(vcglca3d: (VesselCell, (Glucose, SPMat3D[Double]))): Unit = {
      val (vc, (glc, a3d)) = vcglca3d
      a3d(vc.loc) = glc.blood
    }

    override val toString: String = "Ejection"
  }

  case class Diffuse(val diffuser: Diffusion3DFFactor, val dt: Int)
      extends Transformation[SPMat3D[Double]] {
    def transform(substance: SPMat3D[Double]): Unit =
      diffuser.diffuse(substance, dt)
    override val toString: String = "Diffusion"
  }

  case class Uptake(val amount: Double, val limit: Double)
      extends Transformation[(BodyCell, SPMat3D[Double])] {
    def transform(bcgluc: (BodyCell, SPMat3D[Double])): Unit = {
      val (bc, gluc) = bcgluc
      val a = Math.min(limit - bc.nutrition, Math.min(amount, gluc(bc.loc)))
      gluc(bc.loc) -= a
      bc.nutrition += a
    }

    override val toString: String = "Uptake"
  }

  case class Consume(val amount: Double) extends Transformation[BodyCell] {
    def transform(bc: BodyCell): Unit = {
      bc.nutrition -= amount
    }

    override val toString: String = "Consume"
  }

  case class Decay(val amount: Double)
      extends Transformation[SPMat3D[Double]] {
    def transform(substance: SPMat3D[Double]): Unit =
      substance.transform(d => if (d > amount) d - amount else 0)

    override val toString: String = "Decay"
  }

  // LOGGING TRANSFORMATIONS
  case class LogData(val pw: PrintWriter) extends Transformation[SimData] {

    def transform(data: SimData): Unit = {

      // make vessel cell locations zero so we can get accurate logging of tissue
      data.cells.list.collect { case vc: VesselCell => vc }.foreach {
        case vc => data.nutritions.glucose(vc.loc) = 0d
      }

      // println(
      //   f"${data.t.minute}%d ${data.cr.tc}%f ${data.glc.blood}%f ${data.glc.inc}%f " +
      //   f"${data.nutritions.glucose.sum}%f ${data.nutritions.glucose.mean}%f")
      pw.println(
        f"${data.t.minute}%d ${data.cr.tc}%f ${data.glc.blood}%f ${data.glc.inc}%f " +
          f"${data.nutritions.glucose.sum}%f ${data.nutritions.glucose.mean}%f")
    }

    override val toString: String = "Logging"
  }
}
