package co.mefu.msl.cca.sim
package simv6

import java.io.PrintWriter

import scala.collection.mutable.Buffer

import co.mefu.msl.core.io.IO
import co.mefu.msl.core.Random
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.geometry.shape._

import co.mefu.msl.linalg.diff.Diffusion3DFFactor

import co.mefu.msl.modeling._
import co.mefu.msl.modeling.Transformation._
import co.mefu.msl.modeling.Substitution._
import co.mefu.msl.modeling.SubbedTransformation._

import co.mefu.msl.cca.action.PassTime
import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

import Data._
import Action._

class SimV6(pw: PrintWriter) extends Simulation[SimData] {

  // IMPLICIT SUBSTITUTIONS
  implicit val timeSubsSim: Substitution[Time, SimData] =
    Substitution("timeSubsSim", sd => Array(sd.t))

  implicit val crSubsSim: Substitution[CircadianRhythm, SimData] =
    Substitution("crSubsSim", sd => Array(sd.cr))

  implicit val glcSubsSim: Substitution[Glucose, SimData] =
    Substitution("glcSubsSim", sd => Array(sd.glc))

  implicit val vcsSubsSim: Substitution[VesselCell, SimData] =
    Substitution("vcsSubsSim",
                 sd => sd.cells.list.collect { case vc: VesselCell => vc })

  implicit val bcsSubsSim: Substitution[BodyCell, SimData] =
    Substitution("bcsSubsSim",
                 sd => sd.cells.list.collect { case bc: BodyCell => bc })

  // EXPLICIT SUBSTITUTIONS
  val glucSubsSim: Substitution[SPMat3D[Double], SimData] =
    Substitution("glucSubsSim", sd => Array(sd.nutritions.glucose))

  // DATA INITIALIZATION
  val n = 20
  val nhalf = n / 2
  val nquarter = n / 4

  val size = SPVec.S3(n, n, n)
  val halfSize = SPVec.S3(nhalf, nhalf, nhalf)
  val quarterSize = SPVec.S3(nquarter, nquarter, nquarter)

  val bvshape =
    new Line(SPVec.S3(nhalf, 0, nhalf), SPVec.S3(nhalf, n - 1, nhalf), 1)
  val sphereShape = new Sphere(halfSize, nhalf - 1)

  val vesselCells = (bvshape.getPoints).map(VesselCell(_))
  val bodyCells = sphereShape.getPoints
    .filterNot(bvshape.getPoints.toSet)
    .map(i => BodyCell(i))
  val cells = new Cells(EmptyCell, size)
  cells.add(vesselCells: _*)
  cells.add(bodyCells: _*)

  val data =
    SimData(
      Time(0),
      CircadianRhythm(),
      Glucose(),
      cells,
      Nutritions(
        SPMat3D.fill[Double](n, n, n)(0)
      )
    )

  // SIMULATION AND TRANSFORMATIONS INITIALIZATION
  val glucDiff3D = Diffusion3DFFactor(n, 0.0503)

  val sim = ChainedTransformation[SimData](
    PassTime,
    CircadianTimeUpdate(24 * 60),
    GlucoseUpdate(80, Array[Int](7 * 60, 13 * 60, 19 * 60), 50, 60, 300),
    sub(Eject)(
      Substitution.combine(vcsSubsSim,
                           Substitution.combine(glcSubsSim, glucSubsSim))),
    sub(Diffuse(glucDiff3D, 1))(glucSubsSim),
    sub(Uptake(0.3, 1))(Substitution.combine(bcsSubsSim, glucSubsSim)),
    Consume(0.15),
    sub(Decay(0.05))(glucSubsSim),
    LogData(pw)
  )

  def transform(sd: SimData): Unit = sim(sd)
}

object SimV6 extends App {

  // create required folders
  IO.createFolder("../output/data/simv6/")
  IO.createFolder("../output/plot/simv6/")

  val pw = IO.getOutputWriter("../output/data/simv6/data.dat")

  val simv6 = new SimV6(pw)

  simv6.runWhile(data => data.t.minute <= 30 * 24 * 60)

  pw.close
}
