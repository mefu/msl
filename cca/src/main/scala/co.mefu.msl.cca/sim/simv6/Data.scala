package co.mefu.msl.cca.sim
package simv6

import co.mefu.msl.core.ds.Map3D
import co.mefu.msl.core.ds.Map3D.HasLoc
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ds.SPMat3D

import co.mefu.msl.cca.data.Time

object Data {

  // CELL DATA STRUCTURE
  sealed abstract class Cell extends HasLoc {
    val uuid: String = java.util.UUID.randomUUID.toString

    override def equals(that: Any): Boolean = {
      that match {
        case that: Cell => uuid.equals(that.uuid)
        case _ => false
      }
    }
  }

  case object EmptyCell extends Cell {
    val loc: SPVec.S3[Int] = SPVec.S3(0, 0, 0) // dummy
  }

  case class VesselCell(val loc: SPVec.S3[Int]) extends Cell

  case class BodyCell(
      var loc: SPVec.S3[Int],
      var nutrition: Double = 0
  ) extends Cell

  case class Nutritions(
      val glucose: SPMat3D[Double]
  )

  case class CircadianRhythm(
      var ts: Int = 0,
      var th: Int = 0,
      var tc: Double = 0
  )

  case class Glucose(
      var blood: Double = 0,
      var inc: Double = 0
  )

  type Cells = Map3D[Cell]

  case class SimData(val t: Time,
                     val cr: CircadianRhythm,
                     val glc: Glucose,
                     val cells: Cells,
                     val nutritions: Nutritions)

}
