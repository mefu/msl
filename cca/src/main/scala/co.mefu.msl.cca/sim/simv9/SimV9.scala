package co.mefu.msl.cca.sim
package simv9

import java.io._

import scala.collection.mutable.Buffer
import scala.collection.mutable.{Map => MMap}

import spire.math.Interval
import spire.implicits._

import co.mefu.msl.core.io.IO
import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ops.SPVecOps._
import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.ops.SPMat3DOps._
import co.mefu.msl.core.geometry.shape._

import co.mefu.msl.linalg.diff.Diffusion3DFFactor

import co.mefu.msl.modeling._
import co.mefu.msl.modeling.Transformation._
import co.mefu.msl.modeling.Substitution._
import co.mefu.msl.modeling.SubbedTransformation._

import co.mefu.msl.cca.action.PassTime
import co.mefu.msl.cca.data.Time

import Data._
import Action._
import Params._

class SimV9(pws: List[PrintWriter], params: Params) extends Simulation[SimData] {

  // DATA INITIALIZATION
  val n = params.size
  val size = SPVec.S3(n, n, n)

  val vesselCells = params.bvshape.getPoints.map(VesselCell(_))
  val tissueCells = params.tshape.getPoints
    .filterNot(params.bvshape.getPoints.toSet)
    .filter(_ => Random.next[Double] < params.fullness)
    .map(i => TissueCell(i, ct = params.ct))
  val cells = new Cells(EmptyCell, size)
  cells.add(vesselCells: _*)
  cells.add(tissueCells: _*)

  val data =
    SimData(
      Time(0),
      CircadianTime(0, 0, 0),
      CircadianClock(0, 0),
      cells,
      Map (
        Glucose ->  ESubstanceData(
                      SPMat3D.fill(n, n, n)(0d), 
                      Diffusion3DFFactor(n, 0.2),
                      BloodLevelStrategy.constant(1d),
                      0.1
                    ),
        Glutamine ->  ESubstanceData(
                        SPMat3D.fill(n, n, n)(0d), 
                        Diffusion3DFFactor(n, 0.2),
                        BloodLevelStrategy.constant(0d),
                        0.1
                      ),
        Lactate ->  ESubstanceData(
                      SPMat3D.fill(n, n, n)(0d), 
                      Diffusion3DFFactor(n, 0.2),
                      BloodLevelStrategy.constant(0d),
                      0.01
                    ),
        Oxygen -> ESubstanceData(
                    SPMat3D.fill(n, n, n)(0d), 
                    Diffusion3DFFactor(n, 0.4),
                    BloodLevelStrategy.constant(10d),
                    0.12
                  )
      ),
      MMap (
        Glucose -> 0d,
        Glutamine -> 0d,
        Lactate -> 0d,
        Oxygen -> 0d
      )
    )

  // Simulation initialization

  // subbers
  implicit val timeSubsSim: Substitution[Time, SimData] =
    Substitution("timeSubsSim", sd => Array(sd.t))

  implicit val ctSubsSim: Substitution[CircadianTime, SimData] =
    Substitution("ctSubsSim", sd => Array(sd.ct))

  implicit val cclkSubsSim: Substitution[CircadianClock, SimData] =
    Substitution("cclkSubsSim", sd => Array(sd.cclk))

  implicit val ssSubsSim: Substitution[ESubstances, SimData] =
    Substitution("ssSubsSim", sd => Array(sd.ss))

  implicit val bcSubsSim: Substitution[BloodContent, SimData] =
    Substitution("bcSubsSim", sd => Array(sd.bc))

  implicit val vcsSubsSim: Substitution[VesselCell, SimData] =
    Substitution("vcsSubsSim",
                 sd => sd.cells.list.collect { case vc: VesselCell => vc })

  implicit val tcsSubsSim: Substitution[TissueCell, SimData] =
    Substitution("tcsSubsSim",
                 sd => sd.cells.list.collect { case tc: TissueCell => tc })

  implicit val cellsSubsSim: Substitution[Cells, SimData] =
    Substitution("cellsSubsSim", sd => Array(sd.cells))


  val trackingCellLoc = tissueCells.map(tc => tc.loc).map(loc =>
    (loc, params.bvshape.getPoints.map(bvloc => loc.distance(bvloc)).min)
  ).sortWith(_._2 < _._2).apply(0)._1

  val distRanges =  List(
                      Interval.openUpper(0d, 2d),
                      Interval.openUpper(2d, 4d),
                      Interval.openUpper(4d, 6d),
                      Interval.openUpper(6d, 8d),
                      Interval(8d, 10d)
                    )
  val sim = ChainedTransformation[SimData](
    PassTime,
    CircadianTimeUpdate,
    CircadianClockUpdate(
      CircadianRhythm(params.cmycPeak), 
      CircadianRhythm(params.wee1Peak)
    ),
    BloodContentUpdate,
    BloodToTissue,
    Diffuse,
    Uptake,
    AvailabilityExtCheck,
    React,
    AvailabilityIntCheck,
    Consume,
    Secrete,
    AgeCell,
    CircadianClockCheck,
    StateUpdate,
    Decay,
    LogData(pws(0)),
    LogSubstance(Glucose, pws(1)),
    LogSubstance(Glutamine, pws(2)),
    LogSubstance(Lactate, pws(3)),
    LogSubstance(Oxygen, pws(4)),
    LogCell(trackingCellLoc, pws(5)),
    LogCells(List(Glucose, Glutamine, Lactate, Oxygen, ATP, Biomass), distRanges, pws(6)),
    PrintProgress
  )

  def transform(sd: SimData): Unit = sim(sd)

  override def report = super.report + sim.report
}

object SimV9 extends App {

  // create required folders
  IO.createFolder("../output/data/simv9/")
  IO.createFolder("../output/plot/simv9/")

  val pws = List(
    IO.getOutputWriter("../output/data/simv9/data.dat"),
    IO.getOutputWriter("../output/data/simv9/glucose.dat"),
    IO.getOutputWriter("../output/data/simv9/glutamine.dat"),
    IO.getOutputWriter("../output/data/simv9/lactate.dat"),
    IO.getOutputWriter("../output/data/simv9/oxygen.dat"),
    IO.getOutputWriter("../output/data/simv9/cell.dat"),
    IO.getOutputWriter("../output/data/simv9/cells.dat")
  )

  val simv9 = new SimV9(pws, new Params1())
  simv9.runWhile(data => data.t.minute <= 10 * 24 * 60)

  pws.foreach(_.close)
}
