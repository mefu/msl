package co.mefu.msl.cca.sim
package simv9

import scala.collection.mutable.{Map => MMap}

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._
import co.mefu.msl.core.ds.Map3D
import co.mefu.msl.core.ds.Map3D.HasLoc
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.math.Fitter

import co.mefu.msl.linalg.diff.Diffusion3DFFactor

import co.mefu.msl.cca.data.Time


object Data {
  // cell cycle
  sealed trait CellCycleState
  case object G1R extends CellCycleState
  case object RS extends CellCycleState
  case object S extends CellCycleState
  case object G2 extends CellCycleState
  case object M extends CellCycleState
  case object G0 extends CellCycleState

  object CellCycleState {
    def next(state: CellCycleState): CellCycleState =
      state match {
        case G1R => RS
        case RS => S
        case S => G2
        case G2 => M
        case M => G1R
        case G0 => G1R
      }

    def toInt(state: CellCycleState): Int =
      state match {
        case G1R => 1
        case RS => 1
        case S => 2
        case G2 => 3
        case M => 4
        case G0 => 5
      }
  }

  case class CellCycleDurations(durG1: Double,
                                durS: Double,
                                durG2: Double,
                                durM: Double,
                                rpoint: Double) {
    val total: Double = (durG1 + durS + durG2 + durM)
    val inverseTotal: Double = 1 / total
    def getRandomized(state: CellCycleState, sway: Double = 0.2) =
      get(state) * (1 + (2 * Random.next[Double] - 1) * sway)

    def get(state: CellCycleState): Double =
      state match {
        case G1R => durG1 * rpoint
        case RS => durG1 * (1 - rpoint)
        case S => durS
        case G2 => durG2
        case M => durM
        case G0 => 0
      }
  }


  // Time related
  case class CircadianTime (
    var minute: Int,
    var hour: Int,
    var hourd: Double
  )

  case class CircadianClock (
    var cmyc: Double,
    var wee1: Double
  )

  // Substances
  sealed trait Substance
  sealed trait ESubstance extends Substance
  sealed trait ISubstance extends Substance

  case object Glucose extends ESubstance
  case object Glutamine extends ESubstance
  case object Lactate extends ESubstance
  case object Oxygen extends ESubstance
  case object ATP extends ISubstance
  case object Biomass extends ISubstance

  case class ESubstanceData (
    val map: SPMat3D[Double],
    val diffuser: Diffusion3DFFactor,
    val bloodLevel: BloodLevelStrategy,
    val decay: Double
  )

  trait BloodLevelStrategy {
    def apply(ct: CircadianTime): Double
  }

  object BloodLevelStrategy {

    def sigmoid(base: Double, pulses: Array[Int], 
                peak: Double, tpeak: Double,
                tlength: Double): BloodLevelStrategy = 
      new BloodLevelStrategy {

        val (poly1, poly2) = {
          val points1 = Array[(Double, Double)](
            (0d, 0d),
            (tpeak * 0.25, peak * 0.5),
            (tpeak * 0.5, peak * 0.75),
            (tpeak * 0.75, peak * 0.88),
            (tpeak, peak)
          )

          val points2 = Array[(Double, Double)](
            (tpeak, peak),
            (tpeak + (tlength - tpeak) * 0.25, peak * 0.5),
            (tpeak + (tlength - tpeak) * 0.5, peak * 0.32),
            (tpeak + (tlength - tpeak) * 0.75, peak * 0.12),
            (tlength, 0d),
            (tlength * 1.1, -1 * peak * 0.02)
          )

          val xs1 = points1.map(_._1)
          val zs1 = points1.map(_._2)
          val xs2 = points2.map(_._1)
          val zs2 = points2.map(_._2)

          val poly1 = Fitter.fitPoly(3, xs1, zs1)
          val poly2 = Fitter.fitPoly(3, xs2, zs2)

          (poly1, poly2)
        }

        var lastPulse: Int = pulses.max

        def apply(ct: CircadianTime): Double = {
          if (pulses.contains(ct.minute))
            lastPulse = ct.minute

          val t = ct.minute - lastPulse

          base + (t match {
                      case x if x < 0 => 0
                      case x if x > tlength => 0
                      case x if x <= tpeak => Math.max(poly1(x), 0d)
                      case x if x > tpeak => Math.max(poly2(x), 0d)
                    })
        }
      }

    def constant(base: Double): BloodLevelStrategy =
      ct => base
  }

  type ESubstances = Map[ESubstance, ESubstanceData]

  // CELL DATA STRUCTURE
  type CellContent = MMap[Substance, Double]

  class Reaction (
    val in: List[(Substance, Double)],
    val out: List[(Substance, Double)],
    val smax: Double,
    val name: String
  ) {
    
    def apply(target: Substance, tA: Double, cc: CellContent, crl: ReactionLog): Unit = {
      var scale = smax

      in.foreach { case (s, a) =>
        scale = Math.min(a * scale, cc(s)) / a
      }

      if(tA > cc(target)) {
        out.find(_._1 == target) match {
          case Some((s, a)) =>
            scale = Math.min(a * scale, tA - cc(s)) / a
          case None => Unit
        }
      } else 
        scale = 0

      in.foreach { case (s, a) => cc(s) -= a * scale }
      out.foreach { case (s, a) => cc(s) += a * scale }

      crl(name) += scale
    }

    def potential(target: Substance, cc: CellContent): Double = {
      var scale = smax
      in.foreach { case (s, a) =>
        scale = Math.min(a * scale, cc(s)) / a
      }

      out.find(_._1 == target).get._2 * scale
    }

  }

  case class LimitData (
    val max: Double, val min: Double, val in: Double, val out: Double
  )

  type Targets = Map[CellCycleState, Map[Substance, Double]]
  type Consumptions = Map[CellCycleState, Map[ISubstance, Double]]
  type Preferences = Map[ISubstance, List[Reaction]]
  type Limits = Map[Substance, LimitData]

  trait CellType {
    // dna damage
    def pDD: Double
    def pDDR: Double

    // circadian coupling
    def ccStr: Double

    def targets: Targets
    def consumptions: Consumptions
    def prefs: Preferences

    def limits: Limits

    def gf: Boolean

    def ccd: CellCycleDurations
  }

  sealed abstract class Cell extends HasLoc {
    val uuid: String = java.util.UUID.randomUUID.toString

    override def equals(that: Any): Boolean = {
      that match {
        case that: Cell => uuid.equals(that.uuid)
        case _ => false
      }
    }
  }

  case object EmptyCell extends Cell {
    val loc: SPVec.S3[Int] = SPVec.S3(0, 0, 0) // dummy
  }

  case class VesselCell(val loc: SPVec.S3[Int]) extends Cell

  type ReactionLog = MMap[String, Double]

  case class TissueCell (
    var loc: SPVec.S3[Int],
    var state: CellCycleState = G0,
    var age: Double = 0,
    var tr: Double = 0,
    var extNA: MMap[ESubstance, Double] = MMap(Glucose -> 0.1, Glutamine -> 0.1,
                                               Lactate -> 0.1, Oxygen -> 0.1),
    var intNA: MMap[ISubstance, Double] = MMap(ATP -> 0.1, Biomass -> 0.1),
    var df: Boolean = false,
    var die: Boolean = false,
    val ct: CellType
  ) extends Cell {
    var cc: CellContent = MMap(
      Glucose -> 0.06, Glutamine -> 0.0, 
      Lactate -> 0.0, Oxygen -> 0.3,
      ATP -> 0.2, Biomass -> 0.002
    )
    var next: Double = ct.ccd.getRandomized(state)
    var dr: Double = 1 - Math.pow(1 - ct.pDD, ct.ccd.inverseTotal)
    val crl: ReactionLog = MMap(
      "CR" -> 0, "LAF" -> 0, "GLTMNLYS" -> 0,
      "LR" -> 0, "CRLAF" -> 0, "GlcGltBM" -> 0,
      "GlcBM" -> 0, "GltBM" -> 0, "LctBM" -> 0
    )
  }

  type Cells = Map3D[Cell]

  // Blood Content
  type BloodContent = MMap[Substance, Double]

  // Simulation
  case class SimData(val t: Time,
                     val ct: CircadianTime,
                     val cclk: CircadianClock,
                     val cells: Cells,
                     val ss: ESubstances,
                     val bc: BloodContent)

}
