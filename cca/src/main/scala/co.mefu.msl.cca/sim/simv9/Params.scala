package co.mefu.msl.cca.sim
package simv9

import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ops.SPVecOps._
import co.mefu.msl.core.geometry.shape._

import Data._


trait Params {
  
  // Total simulation size
  def size: Int = 20
  def sized: Double = size.toDouble

  def sizehalf: Int = size / 2

  // shape of blood vessel
  def bvshape: Shape3D = 
    new Line(SPVec.S3(sizehalf, 0, sizehalf), SPVec.S3(sizehalf, size - 1, sizehalf), 1)

  // shape of tissue
  def tshape: Shape3D =
    new Sphere(SPVec.S3(sizehalf, sizehalf, sizehalf), sizehalf - 1)

  // fullness of tissue
  def fullness: Double = 0.7

  // circadian rhythm peaks
  def cmycPeak: Int = 12
  def wee1Peak: Int = 22

  // cell type of simulation
  def ct: CellType

}

object Params {

  object CCDs {

    val short: CellCycleDurations =
      CellCycleDurations(4 * 60, 11 * 60, 1 * 60, 1 * 60, 0.7)
    val normal: CellCycleDurations =
      CellCycleDurations(9 * 60, 11 * 60, 1 * 60, 1 * 60, 0.7)
    val long: CellCycleDurations =
      CellCycleDurations(14 * 60, 11 * 60, 1 * 60, 1 * 60, 0.7)
    val normal2: CellCycleDurations =
      CellCycleDurations(11 * 60, 8 * 60, 4 * 60, 1 * 60, 0.7)

  }

  case class CellRespiration(rmax: Double)
    extends Reaction(List((Glucose, 1), (Oxygen, 6)), List((ATP, 32)), rmax, "CR") 

  case class LacticAcidFermentation(rmax: Double)
    extends Reaction(List((Glucose, 1)), List((ATP, 2), (Lactate, 2)), rmax, "LAF") 

  case class Glutaminolysis(rmax: Double)
    extends Reaction(List((Glutamine, 2), (Oxygen, 3)), List((ATP, 15)), rmax, "GLTMNLYS") 

  case class LactateShuttle(rmax: Double)
    extends Reaction(List((Lactate, 2), (Oxygen, 6)), List((ATP, 25)), rmax, "LR") 

  case class CRLAF(rmax: Double)
    extends Reaction(List((Glucose, 21), (Oxygen, 6)), List((ATP, 72), (Lactate, 40)), rmax, "CRLAF") 
    
  case class GlcGltBM(rmax: Double)
    extends Reaction(List((Glucose, 1), (Glutamine, 1)), List((Biomass, 1)), rmax, "GlcGltBM")

  case class GlcBM(rmax: Double)
    extends Reaction(List((Glucose, 1)), List((Biomass, 1)), rmax, "GlcBM")

  case class GltBM(rmax: Double)
    extends Reaction(List((Glutamine, 1)), List((Biomass, 1)), rmax, "GltBM")

  case class LctBM(rmax: Double)
    extends Reaction(List((Lactate, 1)), List((Biomass, 1)), rmax, "LctBM")

  class Params1 extends Params {
    val ns = 1

    val glc_glt_bm_eff = 0.001 * ns
    val glc_bm_eff = 0.0009 * ns
    val glt_bm_eff = 0.0007 * ns
    val lct_bm_eff = 0.001 * ns

    val resp_max = 0.0005 * ns // 0.016 ATP
    val laf_max = 0.01 * ns // 0.02 ATP
    val gltmnlys_max = 0.0005 * ns // 0.015 ATP 
    val lctsht_max = 0.0005 * ns // 0.025 ATP
    val crlaf_max = 0.00025 * ns

    class NormalCT() extends CellType {
      // dna damage
      val pDD: Double = 0.3
      val pDDR: Double = 0.2

      // circadian coupling
      val ccStr: Double = 1

      // ayy
      val gf = true
      
      // target amount to stabilize material around
      val targets: Targets = Map[CellCycleState, Map[Substance, Double]] (
        G0 -> Map(
          Glucose -> 0.1, Glutamine -> 0.015, 
          Lactate -> 0.05, Oxygen -> 0.3,
          ATP -> 0.2, Biomass -> 0.01
        ),
        G1R -> Map(
          Glucose -> 0.2, Glutamine -> 0.05, 
          Lactate -> 0.1, Oxygen -> 0.45,
          ATP -> 0.3, Biomass -> 0.2
        ),
        RS -> Map(
          Glucose -> 0.2, Glutamine -> 0.05, 
          Lactate -> 0.1, Oxygen -> 0.45,
          ATP -> 0.4, Biomass -> 0.4
        ),
        S -> Map(
          Glucose -> 0.2, Glutamine -> 0.25, 
          Lactate -> 0.1, Oxygen -> 0.45,
          ATP -> 0.45, Biomass -> 0.6
        ),
        G2 -> Map(
          Glucose -> 0.2, Glutamine -> 0.05, 
          Lactate -> 0.1, Oxygen -> 0.45,
          ATP -> 0.4, Biomass -> 0.3
        ),
        M -> Map(
          Glucose -> 0.2, Glutamine -> 0.25, 
          Lactate -> 0.1, Oxygen -> 0.45,
          ATP -> 0.45, Biomass -> 0.3
        )
      ).map{ case (state, ts) => 
              state -> ts.map{ case (s, amount) => s -> amount * ns } }

      // consumptions
      val consumptions: Consumptions = Map[CellCycleState, Map[ISubstance, Double]] (
        G0 -> Map(ATP -> 0.01, Biomass -> 0.00001),
        G1R -> Map(ATP -> 0.015, Biomass -> 0.00001),
        RS -> Map(ATP -> 0.015, Biomass -> 0.00001),
        S -> Map(ATP -> 0.0162, Biomass -> 0.0011),
        G2 -> Map(ATP -> 0.015, Biomass -> 0.00001),
        M -> Map(ATP -> 0.02, Biomass -> 0.001)
      ).map{ case (state, cs) => 
              state -> cs.map{ case (s, amount) => s -> amount * ns } }

      // preference order 
      val prefs: Preferences = Map (
        ATP -> List (
          // CRLAF(crlaf_max),
          CellRespiration(resp_max),
          LactateShuttle(laf_max),
          Glutaminolysis(gltmnlys_max),
          LacticAcidFermentation(lctsht_max)
        ),
        Biomass -> List (
          GlcGltBM(glc_glt_bm_eff),
          GlcBM(glc_bm_eff),
          GltBM(glt_bm_eff),
          LctBM(lct_bm_eff)
        )
      )

      // max amount in cell
      val limits: Limits = Map[Substance, LimitData] (
        Glucose -> LimitData(1, 0.09, 0.01, 0.012), 
        Glutamine -> LimitData(1, 0.1, 0.1, 0.1), 
        Lactate -> LimitData(1, 0.05, 0.1, 0.1),
        Oxygen -> LimitData(1, 0.1, 0.1, 0.1),
        ATP -> LimitData(1, 0.01, 0, 0),
        Biomass -> LimitData(1, 0.01, 0, 0)
      ).map{ case (s, ld) => s -> LimitData(ld.max * ns, ld.min * ns, ld.in * ns, ld.out * ns) }

      val ccd: CellCycleDurations = CCDs.normal
    }

    val ct: CellType = new NormalCT()

  }

  class Params2 extends Params1 {

    override def tshape = new Shape3D {
      def getPoints = List(
        SPVec.S3(sizehalf, sizehalf - 1, sizehalf)
      )
    }

    override def fullness = 1d

    class SingleCT() extends NormalCT {
      // dna damage
      override val pDD: Double = 0d
      override val pDDR: Double = 0d

      override val ccd = CCDs.normal2
    }

    override val ct: CellType = new SingleCT()
  }
}

// object WTF extends App {
//   import scala.collection.mutable.{Map => MMap}
//   Params.Params1.ct.prods(S).foreach { 
//     case (s, prod) =>
//       Params.Params1.ct.prefs(s).foreach {
//         case (r, _) =>
//           val cc: CellContent = MMap(
//             Glucose -> 0.5, Glutamine -> 0.5, Lactate -> 0.5, Oxygen -> 0.5,
//             ATP -> 0.5, Biomass -> 0.5
//           )
//           r(s, prod, cc, Map (
//               Glucose -> 100, Glutamine -> 100, Lactate -> 100,
//               Oxygen -> 100, ATP -> 100, Biomass -> 100
//             ))
//           println(r)
//           r.in.foreach {
//             case (ss, _) => println(f"Used ${0.5 - cc(ss)}%.4f ${ss}")
//           }
//           r.out.foreach {
//             case (ss, _) => println(f"Produced ${cc(ss) - 0.5}%.4f == ${prod}%.4f ${ss}")
//           }
//       }
//   }
// }