package co.mefu.msl.cca.sim
package simv10

import scala.collection.mutable.Buffer

import enumeratum._

import co.mefu.msl.core.{EnumStorage => Storage}
import co.mefu.msl.core.ExtendedEnumEntry
import co.mefu.msl.core.ExtendedEnum

import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ds.SPMat3D

object Model {

  sealed trait CellType extends EnumEntry
  sealed trait PassiveCT extends CellType
  sealed trait ActiveCT extends CellType

  object CellType extends Enum[CellType] {
    val values = findValues

    case object Vessel extends PassiveCT
    case object Normal extends ActiveCT
    case object Cancer extends ActiveCT
  }

  object PassiveCT extends Enum[PassiveCT] {
    import CellType._
    val values = Vector(Vessel)
  }

  object ActiveCT extends Enum[ActiveCT] {
    import CellType._
    val values = Vector(Normal, Cancer)
  }

  sealed trait CellCycleState extends ExtendedEnumEntry

  object CellCycleState extends ExtendedEnum[CellCycleState] {
    val values = findValues

    case object G1 extends CellCycleState {
      lazy val toInt = indexOf(G1)
    }
    case object R extends CellCycleState {
      lazy val toInt = indexOf(R)
    }
    case object S extends CellCycleState {
      lazy val toInt = indexOf(S)
    }
    case object G2 extends CellCycleState {
      lazy val toInt = indexOf(G2)
    }
    case object M extends CellCycleState {
      lazy val toInt = indexOf(M)
    }
    case object G0 extends CellCycleState {
      lazy val toInt = indexOf(G0)
    }
  }

  sealed trait Substance extends ExtendedEnumEntry
  sealed trait Diffusable extends Substance
  sealed trait Consumable extends Substance

  object Substance extends ExtendedEnum[Substance] {
    val values = findValues

    case object Glucose extends Diffusable {
      lazy val toInt = indexOf(Glucose)
    }
    case object Glutamine extends Diffusable {
      lazy val toInt = indexOf(Glutamine)
    }
    case object Lactate extends Diffusable {
      lazy val toInt = indexOf(Lactate)
    }
    case object Oxygen extends Diffusable {
      lazy val toInt = indexOf(Oxygen)
    }

    case object ATP extends Consumable {
      lazy val toInt = indexOf(ATP)
    }
    case object Biomass extends Consumable {
      lazy val toInt = indexOf(Biomass)
    }
  }

  object Diffusable extends ExtendedEnum[Diffusable] {
    import Substance._
    val values = Vector(Glucose, Glutamine, Lactate, Oxygen)
  }

  object Consumable extends ExtendedEnum[Consumable] {
    import Substance._
    val values = Vector(ATP, Biomass)
  }

  class Time (
    var minute: Double
  )

  class CircadianTime (
    var minute: Double,
    var hour: Double,
    var hourd: Double
  )

  class CircadianClock (
    var cmyc: Double,
    var wee1: Double
  )

  sealed trait Cell {
    def loc: SPVec.S3[Int]
  }

  sealed abstract class PassiveCell (
    var loc: SPVec.S3[Int]
  ) extends Cell

  class VesselCell(
    loc: SPVec.S3[Int]
  ) extends PassiveCell(loc)

  sealed abstract class ActiveCell (
    var alive: Boolean,
    var loc: SPVec.S3[Int],
    var state: CellCycleState,
    var age: Int,
    var ct: CellType,

    val ss: Storage[Substance, Double]
  ) extends Cell

  class TissueCell(
    alive: Boolean, loc: SPVec.S3[Int], 
    state: CellCycleState, age:Int,
    ss: Storage[Substance, Double]
  ) extends ActiveCell(alive, loc, state, age, CellType.Normal, ss)

  class CancerCell(
    alive: Boolean, loc: SPVec.S3[Int], 
    state: CellCycleState, age:Int,
    ss: Storage[Substance, Double]
  ) extends ActiveCell(alive, loc, state, age, CellType.Cancer, ss)

  class SModel (
    val time: Time,
    val ct: CircadianTime,
    val cclk: CircadianClock,
    var cells: Buffer[Cell],
    val ss: Storage[Diffusable, SPMat3D[Double]],
    val bc: Storage[Diffusable, Double]
  )

}