package co.mefu.msl.cca.sim
package simv10

import scala.collection.mutable.Buffer

import co.mefu.msl.core.{EnumStorage => Storage}
import co.mefu.msl.core.ds.SPMat3D

import Model._
import Action._
import Params._
import Context._

object Stats {

  type DensityMaps = Storage[Diffusable, SPMat3D[Double]]
  type CellNumbers = Storage[CellCycleState, Int]

  class Stats(
    val dms: Buffer[DensityMaps],
    val cns: Buffer[CellNumbers]
  )
}