package co.mefu.msl.cca.sim
package simv10

import scala.collection.mutable.Buffer

import spire.implicits._

import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.ops.SPMat3DOps._

import Model._
import Action._
import Params._
import Stats._
import Context._

import Substance._
import CellCycleState._

object Context {

  class Cache (
    var vcs: Array[VesselCell],
    var acs: Array[ActiveCell]
  )

  class SContext(
    val params: SParams,
    val model: SModel,
    val cache: Cache,
    val stats: Stats
  )
}

class Simulation[T](
  val actions: List[T => Unit]
) {
  def run(ctx: T): Unit = actions.foreach(_(ctx))
  def runWhile(ctx: T, terminate: T => Boolean): Unit =
    while(!terminate(ctx)) run(ctx)
}

class SimV10(val params: SParams) {

  val vcells = params.vshape.getPoints.map(p => new VesselCell(p))
  val tcells = params.tshape.getPoints.map(
    p => new TissueCell(true, p, G0, 0, params.cell_start.copy))

  val model = new SModel(
    new Time(0),
    new CircadianTime(0, 0, 0),
    new CircadianClock(0, 0),
    Buffer() ++ vcells ++ tcells, // cells
    Diffusable.arrayFill(_ => SPMat3D.fill(params.n, params.n, params.n)(0d)),
    Diffusable.arrayFill(_ => 0d)
  )

  val cache = new Cache(
    vcells.toArray, // vessel cells
    tcells.toArray // normal cells
  )

  val stats = new Stats(
    Buffer(),
    Buffer()
  )

  val ctx = new SContext(params, model, cache, stats)

  UpdateCache(ctx)
  val sim = new Simulation[SContext](
    List(
      ClearModel,
      TimeUpdate,
      CircadianTimeUpdate,
      CircadianClockUpdate,
      BloodContentUpdate,
      BloodToTissue,
      Diffuse,
      AgeCell,
      Uptake,
      // Availability Check
      // React
      // Availability Check
      Consume,
      // Secrete
      Decay,
      // Circadian Clock Check
      // State Update
      SaveStats,
      // Log Stats
      UpdateCache,
      // Print Progress
    )
  )

  def run: Unit = sim.run(ctx)

  def runUntil(t: Int): Unit = 
    sim.runWhile(ctx, ctx => ctx.model.time.minute == t)
}

object SimV10 extends App {

  val sv10 = new SimV10(params1)
  sv10.runUntil(24 * 60)
  
}