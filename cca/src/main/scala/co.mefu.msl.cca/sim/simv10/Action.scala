package co.mefu.msl.cca.sim
package simv10

import spire.implicits._

import co.mefu.msl.core.ops.SPMat3DOps._

import co.mefu.msl.linalg.diff.Diffusion3DFFactor

import Model._
import Context._
import Params._

import CellType._

object Action {

  object ClearModel extends (SContext => Unit) {
    def apply(ctx: SContext): Unit = {
      ctx.model.cells = ctx.model.cells.filter{
        case ac: ActiveCell => ac.alive 
        case _ => true
      }
    }
  }

  object UpdateCache extends (SContext => Unit) {
    def apply(ctx: SContext): Unit = {
      ctx.cache.acs = 
        ctx.model.cells.collect{ case ac: ActiveCell => ac }.toArray
    }
  }

  object TimeUpdate extends (SContext => Unit) {
    def apply(ctx: SContext): Unit = ctx.model.time.minute += 1
  }

  object CircadianTimeUpdate extends (SContext => Unit) {
    def apply(ctx: SContext): Unit = {
      ctx.model.ct.minute = ctx.model.time.minute % 1440
      ctx.model.ct.hour = ctx.model.ct.minute / 60
      ctx.model.ct.hourd = ctx.model.ct.minute / 60d
    }
  }

  object CircadianClockUpdate extends (SContext => Unit) {
    def apply(ctx: SContext): Unit = {
      ctx.model.cclk.cmyc = ctx.params.cmyc(ctx.model.ct.hourd)
      ctx.model.cclk.wee1 = ctx.params.wee1(ctx.model.ct.hourd)
    }
  }

  object BloodContentUpdate extends (SContext => Unit) {
    def apply(ctx: SContext): Unit = {
      Diffusable.values.foreach { 
        d => ctx.model.bc(d) = ctx.params.bl(d)(ctx.model.ct)
      }
    }
  }

  object BloodToTissue extends (SContext => Unit) {

    def apply(ctx: SContext): Unit = {
      ctx.cache.vcs.foreach { 
        vc => 
          Diffusable.values.foreach {
            d => ctx.model.ss(d)(vc.loc) = ctx.model.bc(d)
          }
      }
    }

  }

  object Diffuse extends (SContext => Unit) {
    def apply(ctx: SContext): Unit = {
      Diffusable.values.foreach {
        d => ctx.params.diff(d).diffuse(ctx.model.ss(d), 1)
      }
    }
  }

  object AgeCell extends (SContext => Unit) {
    def apply(ctx: SContext): Unit = {
      ctx.cache.acs.foreach {
        ac => ac.age += 1
      }
    }
  }

  object Uptake extends (SContext => Unit) {
    def apply(ctx: SContext): Unit = {
      ctx.cache.acs.foreach {
        ac => Diffusable.values.foreach { d =>
          val target = ctx.params.target(ac.state)(d)
          var a = Math.min(target - ac.ss(d), ctx.model.ss(d)(ac.loc))
          a = Math.min(a, ctx.params.uptake(d))
          if (a > 0) {
            ctx.model.ss(d)(ac.loc) -= a
            ac.ss(d) += a
          }
        }
      }
    }
  }

  object Consume extends (SContext => Unit) {

    def apply(ctx: SContext): Unit = {
      ctx.cache.acs.foreach {
        ac => Consumable.values.foreach { c => 
          val consumption = ctx.params.consumption(ac.state)(c)
          ac.ss(c) -= consumption
        }
      }
    }

  }

  object Decay extends (SContext => Unit) {
    def apply(ctx: SContext): Unit = {
      Diffusable.values.foreach {
        d => ctx.model.ss(d).transform(_ * (1 - ctx.params.decay(d)))
      }
    }
  }

  object SaveStats extends (SContext => Unit) {

    def apply(ctx: SContext): Unit = {
      ctx.stats.dms += ctx.model.ss
    }
  }


}