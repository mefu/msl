package co.mefu.msl.cca.sim
package simv10

import co.mefu.msl.core.{EnumStorage => Storage}
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.geometry.shape._

import co.mefu.msl.linalg.diff.Diffusion3DFFactor

import Model._

import Substance._
import CellCycleState._

object Params {

  class CircadianRhythm (val peak: Double) {
    def apply(t: Double): Double =
      (1 - Math.cos((t - peak - 12) * Math.PI / 12)) / 2
  }

  case class CircadianClockParams(cmyc: Double, wee1: Double)

  type BloodLevelParams = Storage[Diffusable, CircadianTime => Double]

  type DiffusionParams = Storage[Diffusable, Double]

  type DecayParams = Storage[Diffusable, Double]


  type TargetParams = Storage[CellCycleState, Storage[Substance, Double]]

  type ConsumptionParams = Storage[CellCycleState, Storage[Consumable, Double]]

  
  type UptakeParams = Storage[Diffusable, Double]

  type SecreteParams = Storage[Diffusable, Double]

  type CellStartParams = Storage[Substance, Double]

  class SParams (
    val n: Int,

    cclkp: CircadianClockParams,

    val bl: BloodLevelParams,
    diffc: DiffusionParams,
    val decay: DecayParams,

    val cell_start: CellStartParams,

    val target: TargetParams,
    val consumption: ConsumptionParams,

    val uptake: UptakeParams,
    val secrete: SecreteParams
  ) {
    val nd: Double = n.toDouble
    val nhalf: Int = n / 2 

    val size: SPVec.S3[Int] = SPVec.S3[Int](n, n, n)
    val sizeHalf: SPVec.S3[Int] = SPVec.S3[Int](nhalf, nhalf, nhalf)

    val vshape: Shape3D = 
      new Line(SPVec.S3(nhalf, 0, nhalf), SPVec.S3(nhalf, n - 1, nhalf), 1)

    val tshape: Shape3D =
      new Sphere(sizeHalf, nhalf - 1)

    val cmyc: CircadianRhythm = new CircadianRhythm(cclkp.cmyc)
    val wee1: CircadianRhythm = new CircadianRhythm(cclkp.wee1)

    val diff: Storage[Diffusable, Diffusion3DFFactor] = 
      Diffusable.arrayFill{ case d => Diffusion3DFFactor(n, diffc(d)) }

    def scale(s: Double): SParams = new SParams(
      n, cclkp, bl, diffc, decay,
      cell_start.map(_ * s),
      target.map(el => el.map(_ * s)),
      consumption.map(el => el.map(_ * s)),
      uptake.map(_ * s),
      secrete.map(_ * s) 
    )
  }

  val params1 = new SParams(
    20,
    CircadianClockParams(12, 22),
    // blood level
    Diffusable.arrayFill {
      case Glucose => _ => 50d
      case Glutamine => _ => 0d
      case Lactate => _ => 0d
      case Oxygen => _ => 0d
    },
    // diff constants
    Diffusable.arrayFill {
      case Glucose | Glutamine | Lactate => 0.1
      case Oxygen => 0.2
    },
    // decay params
    Diffusable.arrayFill {
      case Glucose | Glutamine | Lactate => 0.01
      case Oxygen => 0.02
    },
    // start
    Substance.arrayFill {
      case Glucose => 0.1
      case ATP => 0.1
      case Biomass => 0.1
      case _ => 0
    },
    // target
    CellCycleState.arrayFill {
      case G0 => Substance.arrayFill {
        case Glucose => 0.1
        case Glutamine => 0.015
        case Lactate => 0.05
        case Oxygen => 0.3
        case ATP => 0.2
        case Biomass => 0.01
      }
      case G1 => Substance.arrayFill {
        case Glucose => 0.2
        case Glutamine => 0.05
        case Lactate => 0.1
        case Oxygen => 0.45
        case ATP => 0.3
        case Biomass => 0.2
      }
      case R => Substance.arrayFill {
        case Glucose => 0.2
        case Glutamine => 0.05
        case Lactate => 0.1
        case Oxygen => 0.45
        case ATP => 0.4
        case Biomass => 0.4
      }
      case S => Substance.arrayFill {
        case Glucose => 0.2
        case Glutamine => 0.05
        case Lactate => 0.1
        case Oxygen => 0.45
        case ATP => 0.4
        case Biomass => 0.3
      }
      case G2 => Substance.arrayFill {
        case Glucose => 0.2
        case Glutamine => 0.05
        case Lactate => 0.1
        case Oxygen => 0.45
        case ATP => 0.4
        case Biomass => 0.3
      }
      case M => Substance.arrayFill {
        case Glucose => 0.2
        case Glutamine => 0.25
        case Lactate => 0.1
        case Oxygen => 0.45
        case ATP => 0.45
        case Biomass => 0.3
      }
    },
    // consumption
    CellCycleState.arrayFill {
      case G0 => Consumable.arrayFill {
        case ATP => 0.01
        case Biomass => 0.00001
      }
      case G1 => Consumable.arrayFill {
        case ATP => 0.015
        case Biomass => 0.00001
      }
      case R => Consumable.arrayFill {
        case ATP => 0.015
        case Biomass => 0.00001
      }
      case S => Consumable.arrayFill {
        case ATP => 0.0162
        case Biomass => 0.0011
      }
      case G2 => Consumable.arrayFill {
        case ATP => 0.015
        case Biomass => 0.00001
      }
      case M => Consumable.arrayFill {
        case ATP => 0.02
        case Biomass => 0.001
      }
    }, 
    // uptake
    Diffusable.arrayFill {
      case Glucose => 0.01
      case Glutamine => 0.1
      case Lactate => 0.1
      case Oxygen => 0.1
    },
    // secrete
    Diffusable.arrayFill {
      case Glucose => 0.012
      case Glutamine => 0.1
      case Lactate => 0.1
      case Oxygen => 0.1
    }
  )

}