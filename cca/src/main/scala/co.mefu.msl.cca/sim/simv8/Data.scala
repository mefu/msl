package co.mefu.msl.cca.sim
package simv8

import scala.collection.mutable.{Map => MMap}

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._
import co.mefu.msl.core.ds.Map3D
import co.mefu.msl.core.ds.Map3D.HasLoc
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.math.Fitter

import co.mefu.msl.linalg.diff.Diffusion3DFFactor

import co.mefu.msl.cca.data.Time


object Data {
  // cell cycle
  sealed trait CellCycleState
  case object G1R extends CellCycleState
  case object RG1 extends CellCycleState
  case object S extends CellCycleState
  case object G2 extends CellCycleState
  case object M extends CellCycleState
  case object G0 extends CellCycleState

  object CellCycleState {
    def next(state: CellCycleState): CellCycleState =
      state match {
        case G1R => RG1
        case RG1 => S
        case S => G2
        case G2 => M
        case M => G1R
        case G0 => G1R
      }

    def toInt(state: CellCycleState): Int =
      state match {
        case G1R => 1
        case RG1 => 1
        case S => 2
        case G2 => 3
        case M => 4
        case G0 => 5
      }
  }

  case class CellCycleDurations(val durG1: Double,
                                val durS: Double,
                                val durG2: Double,
                                val durM: Double) {
    val total: Double = (durG1 + durS + durG2 + durM)
    val inverseTotal: Double = 1 / total
    def getRandomized(state: CellCycleState, sway: Double = 0.2) =
      get(state) * (1 + (2 * Random.next[Double] - 1) * sway)

    def get(state: CellCycleState): Double =
      state match {
        case G1R => durG1 - 3 * 60
        case RG1 => 3 * 60
        case S => durS
        case G2 => durG2
        case M => durM
        case G0 => 0
      }
  }

  object CellCycleDurations {

    val short: CellCycleDurations =
      CellCycleDurations(4 * 60, 11 * 60, 1 * 60, 1 * 60)
    val normal: CellCycleDurations =
      CellCycleDurations(9 * 60, 11 * 60, 1 * 60, 1 * 60)
    val long: CellCycleDurations =
      CellCycleDurations(14 * 60, 11 * 60, 1 * 60, 1 * 60)

  }


  // Time related
  case class CircadianTime (
    var minute: Int,
    var hour: Int,
    var hourd: Double
  )

  case class ExternalElem(val peak: Double, val strength: Double) {
    def apply(t: Double): Double =
      (1 - Math.cos((t - peak - 12) * Math.PI / 12)) * strength / 2
  }

  case class CircadianClock(peak_p1: Double = 12, peak_w1: Double = 22) {
    val per1: ExternalElem = ExternalElem(peak_p1, 1)
    val wee1: ExternalElem = ExternalElem(peak_w1, 1)
  }

  // Substances
  sealed trait Substance
  case object Glucose extends Substance
  case object Glutamine extends Substance
  case object Lactate extends Substance
  case object Oxygen extends Substance
  case object Pyruvate extends Substance
  case object Nucleotide extends Substance
  case object ProteinLipid extends Substance
  case object ATP extends Substance

  case class SubstanceData (
    val map: SPMat3D[Double],
    val diffuser: Diffusion3DFFactor,
    val bloodLevel: BloodLevelStrategy,
    val decay: Double
  )

  trait BloodLevelStrategy {
    def apply(ct: CircadianTime): Double
  }

  object BloodLevelStrategy {

    def sigmoid(base: Double, pulses: Array[Int], 
                peak: Double, tpeak: Double,
                tlength: Double): BloodLevelStrategy = 
      new BloodLevelStrategy {

        val (poly1, poly2) = {
          val points1 = Array[(Double, Double)](
            (0d, 0d),
            (tpeak * 0.25, peak * 0.5),
            (tpeak * 0.5, peak * 0.75),
            (tpeak * 0.75, peak * 0.88),
            (tpeak, peak)
          )

          val points2 = Array[(Double, Double)](
            (tpeak, peak),
            (tpeak + (tlength - tpeak) * 0.25, peak * 0.5),
            (tpeak + (tlength - tpeak) * 0.5, peak * 0.32),
            (tpeak + (tlength - tpeak) * 0.75, peak * 0.12),
            (tlength, 0d),
            (tlength * 1.1, -1 * peak * 0.02)
          )

          val xs1 = points1.map(_._1)
          val zs1 = points1.map(_._2)
          val xs2 = points2.map(_._1)
          val zs2 = points2.map(_._2)

          val poly1 = Fitter.fitPoly(3, xs1, zs1)
          val poly2 = Fitter.fitPoly(3, xs2, zs2)

          (poly1, poly2)
        }

        var lastPulse: Int = pulses.max

        def apply(ct: CircadianTime): Double = {
          if (pulses.contains(ct.minute))
            lastPulse = ct.minute

          val t = ct.minute - lastPulse

          base + (t match {
                      case x if x < 0 => 0
                      case x if x > tlength => 0
                      case x if x <= tpeak => Math.max(poly1(x), 0d)
                      case x if x > tpeak => Math.max(poly2(x), 0d)
                    })
        }
      }

    def constant(base: Double): BloodLevelStrategy =
      ct => base
  }

  type Substances = Map[Substance, SubstanceData]

  // CELL DATA STRUCTURE
  type CellContent = MMap[Substance, Double]

  class Reaction (
    val in: List[Substance],
    val out: List[Substance],
    tA: Double 
  ) {
    
    def apply(cc: CellContent, limits: LimitDatas) = {
      var a = Math.min(tA, in.map(cc(_)).min)
      a = Math.min(a, out.map(s => limits(s).max - cc(s)).min)

      in.foreach(cc(_) -= a)
      out.foreach(cc(_) += a)
    }
  }

  case class Glycolysis(tA: Double)
    extends Reaction(List(Glucose), List(Pyruvate), tA)

  case class PPP(tA: Double)
    extends Reaction(List(Glucose, Glutamine), List(Nucleotide), tA)

  case class TCA_ETC(tA: Double)
    extends Reaction(List(Pyruvate, Oxygen), List(ATP), tA)

  case class LactateToPyruvate(tA: Double)
    extends Reaction(List(Lactate), List(Pyruvate), tA)

  case class PyruvateToLactate(tA: Double)
    extends Reaction(List(Pyruvate), List(Lactate), tA)

  case class ProteinLipidSynth(tA: Double)
    extends Reaction(List(Pyruvate, Glutamine, Oxygen), List(ProteinLipid), tA)

  case class LimitData (
    val max: Double,
    val vin: Double,
    val vout: Double,
    val min: Double
  )

  type Reactions = Map[CellCycleState, List[Reaction]]
  type LimitDatas = Map[Substance, LimitData]
  type TransmitDatas = Map[CellCycleState, Map[Substance, (Double, Double)]]
  type ConsumptionDatas = Map[CellCycleState, Map[Substance, Double]]

  trait CellType {
    def deathRate: Double
    def repairRate: Double
    def reactions: Reactions
    def limits: LimitDatas
    def transmits: TransmitDatas
    def consumptions: ConsumptionDatas
    def initCC: CellContent
    def ccd: CellCycleDurations
  }

  val ns = 0.25

  case object NormalCT extends CellType {
    val deathRate: Double = 0.30
    val repairRate: Double = 0.2

    val limits: LimitDatas = Map (
      Glucose -> LimitData(1 * ns, 1 * ns, 1 * ns, 0 * ns),
      Glutamine -> LimitData(1 * ns, 1 * ns, 1 * ns, 0 * ns),
      Lactate -> LimitData(1 * ns, 0.1 * ns, 0.1 * ns, -1 * ns),
      Oxygen -> LimitData(1 * ns, 1 * ns, 1 * ns, 0 * ns),
      Pyruvate -> LimitData(1 * ns, 0 * ns, 0 * ns, 0 * ns),
      Nucleotide -> LimitData(5 * ns, 0 * ns, 0 * ns, 0 * ns),
      ProteinLipid -> LimitData(5 * ns, 0 * ns, 0 * ns, 0 * ns),
      ATP -> LimitData(5 * ns, 0 * ns, 0 * ns, 0 * ns)
    )

    val transmits: TransmitDatas = Map (
      G1R -> Map (
        Glucose -> (0.025 * ns, 0 * ns),
        Glutamine -> (0.015 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.025 * ns, 0 * ns)
      ),
      RG1 -> Map (
        Glucose -> (0.105 * ns, 0 * ns),
        Glutamine -> (0.055 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.055 * ns, 0 * ns)
      ),
      S -> Map (
        Glucose -> (0.205 * ns, 0 * ns),
        Glutamine -> (0.105 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.105 * ns, 0 * ns)
      ),
      G2 -> Map (
        Glucose -> (0.025 * ns, 0 * ns),
        Glutamine -> (0.015 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.025 * ns, 0 * ns)
      ),
      M -> Map (
        Glucose -> (0.025 * ns, 0 * ns),
        Glutamine -> (0.005 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.015 * ns, 0 * ns)
      ),
      G0 -> Map (
        Glucose -> (0.01 * ns, 0 * ns),
        Glutamine -> (0.005 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.01 * ns, 0 * ns)
      )
    )
    val reactions: Reactions = Map (
      G1R -> List(
        Glycolysis(0.021 * ns), // -0.021 Glucose, +0.021 Pyruvate
        TCA_ETC(0.01 * ns), // -0.01 Pyruvate, -0.01 Oxygen, +0.01 ATP
        ProteinLipidSynth(0.01 * ns)  // -0.01 Pyruvate, -0.01 Glutamine, -0.01 Oxygen, 
                                 // +0.01 ProteinLipid
        // -0.021 Glucose, -0.01 Glutamine, -0.02 Oxygen
        // +0.01 ATP, +0.01 ProteinLipid, +0.001 Pyruvate
      ),
      RG1 -> List(
        Glycolysis(0.051 * ns), // -0.051 Glucose, +0.051 Pyruvate 
        PPP(0.05 * ns), // -0.05 Glucose, -0.05 Glutamine, +0.05 Nucleotide
        TCA_ETC(0.05 * ns) // -0.05 Pyruvate, -0.05 Oxygen, +0.05 ATP
        // -0.101 Glucose, -0.05 Glutamine, -0.05 Oxygen
        // +0.05 ATP, +0.05 Nucleotide, +0.001 Pyruvate
      ),
      S -> List(
        Glycolysis(0.101 * ns), // -0.101 Glucose, +0.101 Pyruvate 
        PPP(0.1 * ns), // -0.1 Glucose, -0.1 Glutamine, +0.1 Nucleotide
        TCA_ETC(0.1 * ns) // -0.1 Pyruvate, -0.1 Oxygen, +0.1 ATP
        // -0.201 Glucose, -0.1 Glutamine, -0.1 Oxygen
        // +0.1 ATP, +0.1 Nucleotide, +0.001 Pyruvate
      ),
      G2 -> List(
        Glycolysis(0.021 * ns), // -0.021 Glucose, +0.021 Pyruvate
        TCA_ETC(0.01 * ns), // -0.01 Pyruvate, -0.01 Oxygen, +0.01 ATP
        ProteinLipidSynth(0.01 * ns)  // -0.01 Pyruvate, -0.01 Glutamine, -0.01 Oxygen, 
                                 // +0.01 ProteinLipid
        // -0.021 Glucose, -0.01 Glutamine, -0.02 Oxygen
        // +0.01 ATP, +0.01 ProteinLipid, +0.001 Pyruvate
      ),
      M -> List(
        Glycolysis(0.021 * ns), // -0.021 Glucose, +0.021 Pyruvate
        TCA_ETC(0.01 * ns), // -0.01 Pyruvate, -0.01 Oxygen, +0.01 ATP
        // -0.021 Glucose, -0.01 Oxygen
        // +0.01 ATP, +0.001 Pyruvate
      ),
      G0 -> List(
        Glycolysis(0.006 * ns), // -0.006 Glucose, +0.006 Pyruvate
        TCA_ETC(0.005 * ns), // -0.005 Pyruvate, -0.005 Oxygen, +0.005 ATP
        // -0.006 Glucose, -0.005 Oxygen
        // +0.005 ATP, +0.001 Pyruvate
      )
    )

    val consumptions: ConsumptionDatas = Map (
      G1R -> Map (
        ATP -> 0.009 * ns
      ),
      RG1 -> Map (
        ATP -> 0.049 * ns
      ),
      S -> Map (
        ATP -> 0.099 * ns,
        Nucleotide -> 0.099 * ns
      ),
      G2 -> Map (
        ATP -> 0.009 * ns
      ),
      M -> Map (
        ATP -> 0.009 * ns,
        ProteinLipid -> 0.001 * ns
      ),
      G0 -> Map (
        ATP -> 0.004 * ns
      )
    )

    val initCC: CellContent = MMap (
                                Glucose -> 1d * ns,
                                Glutamine -> 1d * ns,
                                Lactate -> 0d * ns,
                                Oxygen -> 1d * ns,
                                Pyruvate -> 1d * ns,
                                Nucleotide -> 5d * ns,
                                ProteinLipid -> 5d * ns,
                                ATP -> 5d * ns
                              )

    val ccd: CellCycleDurations = CellCycleDurations.normal
  }

  case object CancerShortCT extends CellType {
    val deathRate: Double = 0.30
    val repairRate: Double = 0.2
    val reactions: Reactions = Map (
      G1R -> List(
        Glycolysis(0.021 * ns), // -0.021 Glucose, +0.021 Pyruvate
        TCA_ETC(0.01 * ns), // -0.01 Pyruvate, -0.01 Oxygen, +0.01 ATP
        ProteinLipidSynth(0.01 * ns)  // -0.01 Pyruvate, -0.01 Glutamine, -0.01 Oxygen, 
                                 // +0.01 ProteinLipid
        // -0.021 Glucose, -0.01 Glutamine, -0.02 Oxygen
        // +0.01 ATP, +0.01 ProteinLipid, +0.001 Pyruvate
      ),
      RG1 -> List(
        Glycolysis(0.051 * ns), // -0.051 Glucose, +0.051 Pyruvate 
        PPP(0.05 * ns), // -0.05 Glucose, -0.05 Glutamine, +0.05 Nucleotide
        TCA_ETC(0.05 * ns) // -0.05 Pyruvate, -0.05 Oxygen, +0.05 ATP
        // -0.101 Glucose, -0.05 Glutamine, -0.05 Oxygen
        // +0.05 ATP, +0.05 Nucleotide, +0.001 Pyruvate
      ),
      S -> List(
        Glycolysis(0.101 * ns), // -0.101 Glucose, +0.101 Pyruvate 
        PPP(0.1 * ns), // -0.1 Glucose, -0.1 Glutamine, +0.1 Nucleotide
        TCA_ETC(0.1 * ns) // -0.1 Pyruvate, -0.1 Oxygen, +0.1 ATP
        // -0.201 Glucose, -0.1 Glutamine, -0.1 Oxygen
        // +0.1 ATP, +0.1 Nucleotide, +0.001 Pyruvate
      ),
      G2 -> List(
        Glycolysis(0.021 * ns), // -0.021 Glucose, +0.021 Pyruvate
        TCA_ETC(0.01 * ns), // -0.01 Pyruvate, -0.01 Oxygen, +0.01 ATP
        ProteinLipidSynth(0.01 * ns)  // -0.01 Pyruvate, -0.01 Glutamine, -0.01 Oxygen, 
                                 // +0.01 ProteinLipid
        // -0.021 Glucose, -0.01 Glutamine, -0.02 Oxygen
        // +0.01 ATP, +0.01 ProteinLipid, +0.001 Pyruvate
      ),
      M -> List(
        Glycolysis(0.021 * ns), // -0.021 Glucose, +0.021 Pyruvate
        TCA_ETC(0.01 * ns), // -0.01 Pyruvate, -0.01 Oxygen, +0.01 ATP
        // -0.021 Glucose, -0.01 Oxygen
        // +0.01 ATP, +0.001 Pyruvate
      ),
      G0 -> List(
        Glycolysis(0.006 * ns), // -0.006 Glucose, +0.006 Pyruvate
        TCA_ETC(0.005 * ns), // -0.005 Pyruvate, -0.005 Oxygen, +0.005 ATP
        // -0.006 Glucose, -0.005 Oxygen
        // +0.005 ATP, +0.001 Pyruvate
      )
    )

    val limits: LimitDatas = Map (
      Glucose -> LimitData(1 * ns, 1 * ns, 1 * ns, 0 * ns),
      Glutamine -> LimitData(1 * ns, 1 * ns, 1 * ns, 0 * ns),
      Lactate -> LimitData(1 * ns, 0.1 * ns, 0.1 * ns, -1 * ns),
      Oxygen -> LimitData(1 * ns, 1 * ns, 1 * ns, 0 * ns),
      Pyruvate -> LimitData(1 * ns, 0 * ns, 0 * ns, 0 * ns),
      Nucleotide -> LimitData(5 * ns, 0 * ns, 0 * ns, 0 * ns),
      ProteinLipid -> LimitData(5 * ns, 0 * ns, 0 * ns, 0 * ns),
      ATP -> LimitData(5 * ns, 0 * ns, 0 * ns, 0 * ns)
    )

    val transmits: TransmitDatas = Map (
      G1R -> Map (
        Glucose -> (0.025 * ns, 0 * ns),
        Glutamine -> (0.015 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.025 * ns, 0 * ns)
      ),
      RG1 -> Map (
        Glucose -> (0.105 * ns, 0 * ns),
        Glutamine -> (0.055 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.055 * ns, 0 * ns)
      ),
      S -> Map (
        Glucose -> (0.205 * ns, 0 * ns),
        Glutamine -> (0.105 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.105 * ns, 0 * ns)
      ),
      G2 -> Map (
        Glucose -> (0.025 * ns, 0 * ns),
        Glutamine -> (0.015 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.025 * ns, 0 * ns)
      ),
      M -> Map (
        Glucose -> (0.025 * ns, 0 * ns),
        Glutamine -> (0.005 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.015 * ns, 0 * ns)
      ),
      G0 -> Map (
        Glucose -> (0.01 * ns, 0 * ns),
        Glutamine -> (0.005 * ns, 0 * ns),
        Lactate -> (0 * ns, 0 * ns),
        Oxygen -> (0.01 * ns, 0 * ns)
      )
    )

    val consumptions: ConsumptionDatas = Map (
      G1R -> Map (
        ATP -> 0.009 * ns
      ),
      RG1 -> Map (
        ATP -> 0.049 * ns
      ),
      S -> Map (
        ATP -> 0.099 * ns,
        Nucleotide -> 0.099 * ns
      ),
      G2 -> Map (
        ATP -> 0.009 * ns
      ),
      M -> Map (
        ATP -> 0.009 * ns,
        ProteinLipid -> 0.001 * ns
      ),
      G0 -> Map (
        ATP -> 0.004 * ns
      )
    )

    val initCC: CellContent = MMap (
                                Glucose -> 1d * ns,
                                Glutamine -> 1d * ns,
                                Lactate -> 0d * ns,
                                Oxygen -> 1d * ns,
                                Pyruvate -> 1d * ns,
                                Nucleotide -> 5d * ns,
                                ProteinLipid -> 5d * ns,
                                ATP -> 5d * ns
                              )

    val ccd: CellCycleDurations = CellCycleDurations.normal
  }

  sealed abstract class Cell extends HasLoc {
    val uuid: String = java.util.UUID.randomUUID.toString

    override def equals(that: Any): Boolean = {
      that match {
        case that: Cell => uuid.equals(that.uuid)
        case _ => false
      }
    }
  }

  case object EmptyCell extends Cell {
    val loc: SPVec.S3[Int] = SPVec.S3(0, 0, 0) // dummy
  }

  case class VesselCell(val loc: SPVec.S3[Int]) extends Cell

  case class TissueCell (
    var loc: SPVec.S3[Int],
    var state: CellCycleState = G1R,
    var age: Double = 0,
    var tr: Double = 0,
    var na: Double = 0,
    var df: Boolean = false,
    val ct: CellType
  ) extends Cell {
    var cc: CellContent = ct.initCC.clone
    var next: Double = ct.ccd.getRandomized(state)
    var dr: Double = 1 - Math.pow(1 - ct.deathRate, ct.ccd.inverseTotal)
  }

  type Cells = Map3D[Cell]

  // Blood Content
  type BloodContent = MMap[Substance, Double]

  // Simulation
  case class SimData(val t: Time,
                     val ct: CircadianTime,
                     val cclk: CircadianClock,
                     val cells: Cells,
                     val ss: Substances,
                     val bc: BloodContent)

}
