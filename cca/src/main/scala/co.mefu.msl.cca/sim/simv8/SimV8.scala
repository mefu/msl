package co.mefu.msl.cca.sim
package simv8

import java.io.PrintWriter

import scala.collection.mutable.Buffer
import scala.collection.mutable.{Map => MMap}

import spire.math.Interval
import spire.implicits._

import co.mefu.msl.core.io.IO
import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ops.SPVecOps._
import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.geometry.shape._

import co.mefu.msl.linalg.diff.Diffusion3DFFactor

import co.mefu.msl.modeling._
import co.mefu.msl.modeling.Transformation._
import co.mefu.msl.modeling.Substitution._
import co.mefu.msl.modeling.SubbedTransformation._

import co.mefu.msl.cca.action.PassTime
import co.mefu.msl.cca.data.Time

import Data._
import Action._

class SimV8(pws: List[PrintWriter]) extends Simulation[SimData] {

  // DATA INITIALIZATION
  val n = 20
  val nd = n.toDouble
  val nhalf = n / 2
  val nquarter = n / 4

  val size = SPVec.S3(n, n, n)
  val halfSize = SPVec.S3(nhalf, nhalf, nhalf)
  val quarterSize = SPVec.S3(nquarter, nquarter, nquarter)

  val bvshape =
    new Line(SPVec.S3(nhalf, 0, nhalf), SPVec.S3(nhalf, n - 1, nhalf), 1)
  val sphereShape = new Sphere(halfSize, nhalf - 1)

  val vesselCells = (bvshape.getPoints).map(VesselCell(_))
  val tissueCells = sphereShape.getPoints
    .filterNot(bvshape.getPoints.toSet)
    .filter(_ => Random.next[Double] < 0.5)
    .map(i => TissueCell(i, ct = NormalCT))
  val cells = new Cells(EmptyCell, size)
  cells.add(vesselCells: _*)
  cells.add(tissueCells: _*)

  val data =
    SimData(
      Time(0),
      CircadianTime(0, 0, 0),
      CircadianClock(12, 22),
      cells,
      Map (
        Glucose ->  SubstanceData(
                      SPMat3D.fill(n, n, n)(1d), 
                      Diffusion3DFFactor(n, 0.2),
                      BloodLevelStrategy.sigmoid(
                        80d, Array(7 * 60, 13 * 60, 19 * 60),
                        50d, 60, 300),
                      0.1
                    ),
        Glutamine ->  SubstanceData(
                      SPMat3D.fill(n, n, n)(0d), 
                      Diffusion3DFFactor(n, 0.2),
                      BloodLevelStrategy.constant(80d),
                      0.1
                    ),
        Lactate ->  SubstanceData(
                      SPMat3D.fill(n, n, n)(0d), 
                      Diffusion3DFFactor(n, 0.2),
                      BloodLevelStrategy.constant(10d),
                      0.01
                    ),
        Oxygen -> SubstanceData(
                    SPMat3D.fill(n, n, n)(1d), 
                    Diffusion3DFFactor(n, 0.4),
                    BloodLevelStrategy.constant(80d),
                    0.12
                  )
      ),
      MMap (
        Glucose -> 0d,
        Glutamine -> 0d,
        Lactate -> 0d,
        Oxygen -> 0d
      )
    )

  // Simulation initialization

  // subbers
  implicit val timeSubsSim: Substitution[Time, SimData] =
    Substitution("timeSubsSim", sd => Array(sd.t))

  implicit val ctSubsSim: Substitution[CircadianTime, SimData] =
    Substitution("ctSubsSim", sd => Array(sd.ct))

  implicit val cclkSubsSim: Substitution[CircadianClock, SimData] =
    Substitution("cclkSubsSim", sd => Array(sd.cclk))

  implicit val ssSubsSim: Substitution[Substances, SimData] =
    Substitution("ssSubsSim", sd => Array(sd.ss))

  implicit val bcSubsSim: Substitution[BloodContent, SimData] =
    Substitution("bcSubsSim", sd => Array(sd.bc))

  implicit val vcsSubsSim: Substitution[VesselCell, SimData] =
    Substitution("vcsSubsSim",
                 sd => sd.cells.list.collect { case vc: VesselCell => vc })

  implicit val tcsSubsSim: Substitution[TissueCell, SimData] =
    Substitution("tcsSubsSim",
                 sd => sd.cells.list.collect { case tc: TissueCell => tc })

  implicit val cellsSubsSim: Substitution[Cells, SimData] =
    Substitution("cellsSubsSim", sd => Array(sd.cells))


  val trackingCellLoc = tissueCells.map(tc => tc.loc).map(loc =>
    (loc, bvshape.getPoints.map(bvloc => loc.distance(bvloc)).min)
  ).sortWith(_._2 < _._2).apply(3)._1

  val distRanges =  List(
                      Interval.openUpper(0d, 2d),
                      Interval.openUpper(2d, 4d),
                      Interval.openUpper(4d, 6d),
                      Interval.openUpper(6d, 8d),
                      Interval(8d, 10d)
                    )

  val sim = ChainedTransformation[SimData](
    PassTime,
    CircadianTimeUpdate,
    BloodContentUpdate,
    BloodToTissue,
    Diffuse,
    Uptake,
    React,
    Consume,
    Secrete,
    AgeCell,
    CircadianClockCheck,
    StateUpdate,
    Decay,
    LogData(pws(0)),
    LogSubstance(Glucose, pws(1)),
    LogSubstance(Glutamine, pws(2)),
    LogSubstance(Lactate, pws(3)),
    LogSubstance(Oxygen, pws(4)),
    LogCell(trackingCellLoc, pws(5)),
    LogCells(List(Glucose, Glutamine, Lactate, Oxygen, 
                  Pyruvate, Nucleotide, ProteinLipid, ATP), distRanges, pws(6)),
    PrintProgress
  )

  def transform(sd: SimData): Unit = sim(sd)
}

object SimV8 extends App {

  // create required folders
  IO.createFolder("../output/data/simv8/")
  IO.createFolder("../output/plot/simv8/")

  val pws = List(
    IO.getOutputWriter("../output/data/simv8/data.dat"),
    IO.getOutputWriter("../output/data/simv8/glucose.dat"),
    IO.getOutputWriter("../output/data/simv8/glutamine.dat"),
    IO.getOutputWriter("../output/data/simv8/lactate.dat"),
    IO.getOutputWriter("../output/data/simv8/oxygen.dat"),
    IO.getOutputWriter("../output/data/simv8/cell.dat"),
    IO.getOutputWriter("../output/data/simv8/cells.dat")
  )

  val simv8 = new SimV8(pws)

  simv8.runWhile(data => data.t.minute <= 30 * 24 * 60)

  pws.foreach(_.close)
}
