package co.mefu.msl.cca.sim
package simv8

import java.io.PrintWriter

import scala.collection.mutable.Buffer
import scala.collection.mutable.{Map => MMap}

import spire.math.Interval
import spire.implicits._

import co.mefu.msl.core.Random
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.ops.SPMat3DOps._
import co.mefu.msl.core.ops.SPVecOps._

import co.mefu.msl.linalg.diff.Diffusion3DFFactor

import co.mefu.msl.modeling._

import co.mefu.msl.cca.data.Time

import Data._

object Action {

  case object CircadianTimeUpdate
      extends Transformation[(Time, CircadianTime)] {

    def transform(input: (Time, CircadianTime)): Unit = {
      val (t, ct) = input
      ct.minute = t.minute % 1440
      ct.hour = (t.minute % 1440) / 60
      ct.hourd = (t.minute % 1440d) / 60d
    }

    override val toString: String = "CircadianTimeUpdate"

  }

  case object BloodContentUpdate 
    extends Transformation[(BloodContent, (Substances, CircadianTime))] {

    def transform(input: (BloodContent, (Substances, CircadianTime))): Unit = {
      val (bc, (ss, ct)) = input
      ss.foreach { case (s, sd) => bc(s) = sd.bloodLevel(ct) }
    }

    override val toString: String = "BloodContentUpdate"
  }

  case object BloodToTissue
    extends Transformation[(VesselCell, (BloodContent, Substances))] {

    def transform(input: (VesselCell, (BloodContent, Substances))): Unit = {
      val (vc, (bc, ss)) = input
      ss.foreach { case (s, sd) => sd.map(vc.loc) = bc(s) }
    }

    override val toString: String = "BloodToTissue"
  }

  case object Diffuse extends Transformation[Substances] {

    def transform(ss: Substances): Unit = {
      ss.foreach { case (s, sd) => sd.diffuser.diffuse(sd.map, 1) }
    }

    override val toString: String = "Diffuse"
  }

  case object Uptake extends Transformation[(TissueCell, Substances)] {

    def transform(input: (TissueCell, Substances)): Unit = {
      val (tc, ss) = input

      tc.ct.transmits(tc.state).foreach {
        case (s, (in, _)) => 
          var a = Math.min(in, tc.ct.limits(s).max - tc.cc(s))
          a = Math.min(a, ss(s).map(tc.loc))
          ss(s).map(tc.loc) -= a
          tc.cc(s) += a
      }
    }

    override val toString: String = "Uptake"

  }

  case object React extends Transformation[TissueCell] {

    def transform(tc: TissueCell): Unit = {
      tc.ct.reactions(tc.state).foreach {
        case r => r(tc.cc, tc.ct.limits)
      }
    }

    override val toString: String = "React"

  }

  case object Consume extends Transformation[TissueCell] {

    def transform(tc: TissueCell): Unit = {
      tc.ct.consumptions(tc.state).foreach {
        case (s, a) => {
          tc.cc(s) -= a

          if(tc.cc(s) < 0)
            tc.cc(s) = 0
        }
      }
    }

    override val toString: String = "Consume"

  }

  case object Secrete extends Transformation[(TissueCell, Substances)] {

    def transform(input: (TissueCell, Substances)): Unit = {
      val (tc, ss) = input

      tc.ct.transmits(tc.state).foreach {
        case (s, (_, out)) => 
          var a = Math.min(out, tc.cc(s))
          ss(s).map(tc.loc) += a
          tc.cc(s) -= a
      }
    }

    override val toString: String = "Secrete"

  }

  case object AgeCell extends Transformation[TissueCell] {
    def transform(tc: TissueCell): Unit = {
      tc.age += 1
      tc.state match {
        case G0 => Unit
        case _ => tc.next -= 1
      }
    }

    override val toString: String = "AgeCell"
  }

  case object CircadianClockCheck
      extends Transformation[(TissueCell, (CircadianTime, CircadianClock))] {

    def transform(input: (TissueCell, (CircadianTime, CircadianClock))): Unit = {
      val (tc, (ct, cclk)) = input
      tc.tr = tc.state match {
        case G1R if tc.next <= 0 => if(tc.na >= 0.7) tc.na else 0
        case RG1 if tc.next <= 0 => 1 - cclk.per1(ct.hourd)
        case G2 if tc.next <= 0 => 1 - cclk.wee1(ct.hourd)
        case G0 => if(tc.na >= 0.7) tc.na else 0
        case _ if tc.next <= 0 => 1
        case _ => 0
      }
    }

    override val toString: String = "CircadianClockCheck"
  }

  case object StateUpdate extends Transformation[(TissueCell, (Substances, Cells))] {

    def death(cells: Cells, tc: TissueCell, ss: Substances): Unit = {
      ss.foreach { case (s, sd) => sd.map(tc.loc) += tc.cc(s) }
      cells.remove(tc)
    }

    def movePhase(tc: TissueCell): Unit = {
      tc.state = CellCycleState.next(tc.state)
      tc.next = tc.ct.ccd.getRandomized(tc.state)
    }

    def transform(input: (TissueCell, (Substances, Cells))): Unit = {
      val (tc, (ss, cells)) = input

      if (tc.cc.exists{ case (s, amount) => amount <= tc.ct.limits(s).min }) {
        if(tc.state == G1R && tc.na > 0) {
          tc.na -= 0.01
        } else {
          death(cells, tc, ss)
        }
      } else if (tc.state == G1R && tc.na < 1) {
        tc.na += 0.01
      }

      if (tc.tr > 0 && Random.roll(tc.tr)) {
        tc.state match {
          case RG1 | G2 if tc.df =>
            if (Random.roll(tc.ct.repairRate)) {
              tc.state = G1R
              tc.next = tc.ct.ccd.getRandomized(G1R) * 1.1
            } else {
              death(cells, tc, ss)
            }
          case G1R => 
            tc.state = CellCycleState.next(tc.state)
            tc.next = tc.ct.ccd.get(tc.state)
          case G0 | RG1 | S | G2 => movePhase(tc)
          case M => 
            movePhase(tc)
            Random
              .choose(cells.getEmptyNeighbours(tc.loc))
              .fold(())(loc => {
                val ntc = TissueCell(loc, ct = tc.ct)
                ntc.cc =  tc.cc.map{ case (s, sa) => (s, sa/2) }
                cells.add(ntc)
              })

            tc.cc.transform{ case (s, sa) => sa/2 }
        }

      } else if(tc.state == G1R && tc.next <= 0) {
        // failed to pass R
        tc.state = G0
        tc.next = 0
      } else if (tc.next <= 0) {
        tc.next += 10
      }

      if (Random.roll(tc.dr)) {
        tc.df = true
      }

    }

    override val toString: String = "StateUpdate"

  }

  case object Decay extends Transformation[Substances] {

    def transform(ss: Substances): Unit = {
      ss.foreach { 
        case (s, sd) => 
          sd.map.transform(_ * (1-sd.decay))
          // sd.map.transform(el => if(el >= sd.decay) el - sd.decay else 0) 
      }
    }

    override val toString: String = "Decay"
  }

  // LOGGING TRANSFORMATIONS
  case class LogCell(
    val loc: SPVec.S3[Int], val pw: PrintWriter) extends Transformation[SimData] {

    def transform(data: SimData): Unit = {

      val tmp = data.cells.list.collect{ case tc: TissueCell if tc.loc == loc => tc }
      
      if(tmp.length > 0) {
        val cell: TissueCell = tmp(0)

        pw.println(
          f"${data.t.minute}%d ${data.ct.hourd}%f ${cell.cc(Glucose)}%f " +
          f"${cell.cc(Glutamine)}%f ${cell.cc(Lactate)}%f ${cell.cc(Oxygen)}%f " +
          f"${cell.cc(Pyruvate)}%f ${cell.cc(Nucleotide)}%f ${cell.cc(ProteinLipid)}%f " +
          f"${cell.cc(ATP)}%f ${CellCycleState.toInt(cell.state)}%d")
      } 
    }

    override val toString: String = "LogCell"

  }

  case class LogCells(
    val ss: List[Substance], 
    val distRanges: List[Interval[Double]],
    val pw: PrintWriter)
    extends Transformation[SimData] {
    val out = new scala.collection.mutable.StringBuilder()

    def transform(data: SimData): Unit = {
      out.clear()

      val vcs = data.cells.list.collect { case vc: VesselCell => vc }
      val tcs = data.cells.list.collect { case tc: TissueCell => tc }

      val distCells = 
        tcs.groupBy(tc => vcs.map(vc => tc.loc.distance(vc.loc)).min)

      out ++= f"${data.t.minute}%d "

      distRanges.foreach {
        r => {
          val cells = 
            distCells.filter {
              case (dist, _) => r.contains(dist)
            }.flatMap {
              case (dist, tcss) => tcss
            }.toBuffer

          val sc = countStates(cells)

          out ++= f"${sc.total}%d ${sc.g1}%d ${sc.s}%d "
          out ++= f"${sc.g2}%d ${sc.m}%d ${sc.g0}%d "

          ss.foreach { s => 
            val amount = cells.map(cell => cell.cc(s)).sum / cells.size
            out ++= f"${amount}%f "
          }
        }
      }

      pw.println(out.toString)
    }

    case class StateCounts(var g1: Int = 0,
                           var s: Int = 0,
                           var g2: Int = 0,
                           var m: Int = 0,
                           var g0: Int = 0) {
      def total: Int = g1 + s + g2 + m + g0
    }

    def countStates(cells: Buffer[TissueCell]): StateCounts = {
      val sc = StateCounts()
      cells.foreach {
        case tc if tc.state == G1R || tc.state == RG1 => sc.g1 += 1
        case tc if tc.state == S => sc.s += 1
        case tc if tc.state == G2 => sc.g2 += 1
        case tc if tc.state == M => sc.m += 1
        case tc if tc.state == G0 => sc.g0 += 1
      }
      sc
    }

    override val toString: String = "LogCells"
  }

  case class LogSubstance(
    val s: Substance, 
    val pw: PrintWriter) extends Transformation[SimData] {

    val out = new scala.collection.mutable.StringBuilder()

    def transform(data: SimData): Unit = {
      out.clear()

      // make vessel cell locations zero so we can get accurate logging of tissue
      data.cells.list.collect { case vc: VesselCell => vc }.foreach {
        case vc => data.ss.get(s).foreach(sd => sd.map(vc.loc) = 0d) 
      }

      out ++= f"${data.t.minute}%d ${data.ct.hourd}%f ${data.bc.getOrElse(s, 0d)}%f "
      out ++= f"${data.ss.get(s).flatMap(sd => Some(sd.map.sum)).getOrElse(0d)}%f "
      out ++= f"${data.ss.get(s).flatMap(sd => Some(sd.map.mean)).getOrElse(0d)}%f "

      pw.println(out.toString)
    }

    override val toString: String = "LogSubstance"

  }

  case class LogData(val pw: PrintWriter) extends Transformation[SimData] {

    def transform(data: SimData): Unit = {
      val sc = countStates(data.cells.list)

      pw.println(
        f"${data.t.minute}%d ${data.ct.hourd}%f " +
        f"${sc.total}%d ${sc.g1}%d ${sc.s}%d ${sc.g2}%d ${sc.m}%d ${sc.g0}%d")
    }

    case class StateCounts(var g1: Int = 0,
                           var s: Int = 0,
                           var g2: Int = 0,
                           var m: Int = 0,
                           var g0: Int = 0,
                           var vc: Int = 0) {
      def total: Int = g1 + s + g2 + m
    }

    def countStates(cells: Buffer[Cell]): StateCounts = {
      val sc = StateCounts()
      cells.foreach {
        case tc: TissueCell if tc.state == G1R || tc.state == RG1 => sc.g1 += 1
        case tc: TissueCell if tc.state == S => sc.s += 1
        case tc: TissueCell if tc.state == G2 => sc.g2 += 1
        case tc: TissueCell if tc.state == M => sc.m += 1
        case tc: TissueCell if tc.state == G0 => sc.g0 += 1
        case vc: VesselCell => sc.vc += 1
      }
      sc
    }

    override val toString: String = "LogData"
  }

  case object PrintProgress extends Transformation[SimData] {
    def transform(sd: SimData): Unit = {
      if(sd.t.minute != 1) 
        print("\u001b[2K\u001b[1A\u001b[2K")
      println(f"Time (min) = ${sd.t.minute}%d Cell NO: ${sd.cells.list.size}")
    }

    override val toString: String = "PrintProgress"
  }
}
