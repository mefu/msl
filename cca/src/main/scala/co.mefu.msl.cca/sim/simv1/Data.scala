package co.mefu.msl.cca.sim
package simv1

import co.mefu.msl.cca.data.Time

object Data {

  case class ExternalElem(val peak: Double, val strength: Double) {
    def apply(t: Double): Double =
      (1 - Math.cos((t - peak - 12) * Math.PI / 12)) * strength / 2
  }

  case class CircadianRhythm(
      var th: Double = 0,
      var tc: Double = 0,
      var phase: Double = 0,
      var sens: Double = 0,
      val per1: ExternalElem = ExternalElem(12, 1),
      val wee1: ExternalElem = ExternalElem(22, 1)
  )

  case class SimData(val t: Time, val cr: CircadianRhythm) {
    override def toString: String = t.toString
  }

}
