package co.mefu.msl.cca.sim
package simv1

import java.io.PrintWriter

import co.mefu.msl.core.io.IO

import co.mefu.msl.modeling._
import co.mefu.msl.modeling.Substitution._
import co.mefu.msl.modeling.SubbedTransformation._

import co.mefu.msl.cca.action.PassTime
import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

import Data._
import Action._

class SimV1(val output: PrintWriter) extends Simulation[SimData] {

  implicit val timeSubsSim: Substitution[Time, SimData] =
    Substitution("timeSubsSim", sd => Array(sd.t))

  implicit val crSubsSim: Substitution[CircadianRhythm, SimData] =
    Substitution("crSubsSim", sd => Array(sd.cr))

  val data = SimData(Time(0), CircadianRhythm())

  val sim = ChainedTransformation[SimData](
    PassTime,
    CircadianTimeUpdate(24 * 60),
    LogData(output)
  )

  def transform(sd: SimData): Unit = sim(sd)
}

object SimV1 extends App {

  // create required folders
  IO.createFolder("../output/data/simv1/")
  IO.createFolder("../output/plot/simv1/")

  val pw = IO.getOutputWriter("../output/data/simv1/data.dat")

  val simv1 = new SimV1(pw)

  simv1.runWhile(data => data.t.minute <= 10 * 24 * 60)

  pw.close
}
