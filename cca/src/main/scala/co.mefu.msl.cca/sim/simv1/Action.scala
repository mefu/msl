package co.mefu.msl.cca.sim
package simv1

import java.io.PrintWriter

import co.mefu.msl.modeling._

import co.mefu.msl.cca.data.Time

import Data._

object Action {

  case class CircadianTimeUpdate(val period: Double)
      extends Transformation[(Time, CircadianRhythm)] {

    val Ps_a0 = 2.328
    val Ps_a1 = -0.2472
    val Ps_b1 = 1.581
    val Ps_a2 = 1.046
    val Ps_b2 = -0.8355
    val Ps_a3 = 0.3293
    val Ps_b3 = -0.2569
    val Ps_a4 = 0.347
    val Ps_b4 = 0.309
    val Ps_a5 = -0.05678
    val Ps_b5 = 0.1758
    val Ps_w = 0.2718

    def transform(tcr: (Time, CircadianRhythm)): Unit = {
      val (t, cr) = tcr
      cr.th = (t.minute % 1440) / 60

      // cr.tc = (data.cr.phase % (2 * Math.PI)) * 12 / Math.PI
      // cr.tc = (t.minute/60.0)-24*Math.floor(t.minute/1440.0)
      cr.tc = (t.minute % 1440d) / 60d

      // cr.phase += 2 * Math.PI / period
      cr.phase = t.minute * 2 * Math.PI / period

      val Ps_x = cr.phase * 12 / Math.PI

      cr.sens = Ps_a0 + Ps_a1 * Math.cos(Ps_x * Ps_w) + Ps_b1 * Math.sin(
        Ps_x * Ps_w)
      cr.sens += Ps_a2 * Math.cos(2 * Ps_x * Ps_w) + Ps_b2 * Math.sin(
        2 * Ps_x * Ps_w)
      cr.sens += Ps_a3 * Math.cos(3 * Ps_x * Ps_w) + Ps_b3 * Math.sin(
        3 * Ps_x * Ps_w)
      cr.sens += Ps_a4 * Math.cos(4 * Ps_x * Ps_w) + Ps_b4 * Math.sin(
        4 * Ps_x * Ps_w)
      cr.sens += Ps_a5 * Math.cos(5 * Ps_x * Ps_w) + Ps_b5 * Math.sin(
        5 * Ps_x * Ps_w)
      cr.sens = cr.sens * Math.PI / 12
    }

    override val toString: String = "CircadianTimeUpdate"

  }

  case class LogData(val pw: PrintWriter) extends Transformation[SimData] {

    def transform(data: SimData): Unit = {
      pw.println(
        s"${data.t.minute} ${data.t.minute / (60 * 24d)} ${data.cr.phase} ${data.cr.tc} ${data.cr.sens} " +
          s"${data.cr.per1(data.cr.tc)} ${data.cr.wee1(data.cr.tc)}")
    }

    override val toString: String = "Logging"
  }

}
