package co.mefu.msl.cca.sim
package simv7

import java.io.PrintWriter

import scala.collection.mutable.Buffer

import spire.implicits._

import co.mefu.msl.core.Random
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.ops.SPMat3DOps._

import co.mefu.msl.core.math.Fitter
import co.mefu.msl.linalg.diff.Diffusion3DFFactor

import co.mefu.msl.modeling._

import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

import Data._

object Action {

  case class CircadianTimeUpdate(val period: Double)
      extends Transformation[(Time, CircadianRhythm)] {

    val Ps_a0 = 2.328
    val Ps_a1 = -0.2472
    val Ps_b1 = 1.581
    val Ps_a2 = 1.046
    val Ps_b2 = -0.8355
    val Ps_a3 = 0.3293
    val Ps_b3 = -0.2569
    val Ps_a4 = 0.347
    val Ps_b4 = 0.309
    val Ps_a5 = -0.05678
    val Ps_b5 = 0.1758
    val Ps_w = 0.2718

    def transform(tcr: (Time, CircadianRhythm)): Unit = {
      val (t, cr) = tcr
      cr.ts = t.minute % 1440
      cr.th = (t.minute % 1440) / 60
      cr.tc = (t.minute % 1440d) / 60d

      cr.phase = t.minute * 2 * Math.PI / period

      val Ps_x = cr.phase * 12 / Math.PI

      cr.sens = Ps_a0 + Ps_a1 * Math.cos(Ps_x * Ps_w) + Ps_b1 * Math.sin(
        Ps_x * Ps_w)
      cr.sens += Ps_a2 * Math.cos(2 * Ps_x * Ps_w) + Ps_b2 * Math.sin(
        2 * Ps_x * Ps_w)
      cr.sens += Ps_a3 * Math.cos(3 * Ps_x * Ps_w) + Ps_b3 * Math.sin(
        3 * Ps_x * Ps_w)
      cr.sens += Ps_a4 * Math.cos(4 * Ps_x * Ps_w) + Ps_b4 * Math.sin(
        4 * Ps_x * Ps_w)
      cr.sens += Ps_a5 * Math.cos(5 * Ps_x * Ps_w) + Ps_b5 * Math.sin(
        5 * Ps_x * Ps_w)
      cr.sens = cr.sens * Math.PI / 12
    }

    override val toString: String = "CircadianTimeUpdate"

  }

  case class GlucoseUpdate(
      val basalGlucoseRate: Double,
      val feedingTimes: Array[Int],
      val glPeak: Double,
      val tpeak: Int,
      val tgl: Int
  ) extends Transformation[(Glucose, CircadianRhythm)] {

    var lastFt: Int = feedingTimes.max
    var lastGlc: Double = 0

    val points1 = Array[(Double, Double)](
      (0d, 0d),
      (tpeak * 0.25, glPeak * 0.5),
      (tpeak * 0.5, glPeak * 0.75),
      (tpeak * 0.75, glPeak * 0.88),
      (tpeak, glPeak)
    )

    val points2 = Array[(Double, Double)](
      (tpeak, glPeak),
      (tpeak + (tgl - tpeak) * 0.25, glPeak * 0.5),
      (tpeak + (tgl - tpeak) * 0.5, glPeak * 0.32),
      (tpeak + (tgl - tpeak) * 0.75, glPeak * 0.12),
      (tgl, 0d),
      (tgl * 1.1, -1 * glPeak * 0.02)
    )

    val xs1 = points1.map(_._1)
    val zs1 = points1.map(_._2)
    val xs2 = points2.map(_._1)
    val zs2 = points2.map(_._2)

    val poly1 = Fitter.fitPoly(3, xs1, zs1)
    val poly2 = Fitter.fitPoly(3, xs2, zs2)

    def transform(input: (Glucose, CircadianRhythm)): Unit = {
      val (glc, cr) = input

      if (feedingTimes.contains(cr.ts))
        lastFt = cr.ts

      val absTime = cr.ts - lastFt

      lastGlc = glc.inc
      glc.inc = absTime match {
        case x if x < 0 => 0
        case x if x > tgl => 0
        // case x => 1.776148528 * Math.pow(x, 1.064961569) * Math.exp(-1.831533454e-2*x)
        case x if x <= tpeak => Math.max(poly1(x), 0d)
        case x if x > tpeak => Math.max(poly2(x), 0d)
      }

      glc.blood = basalGlucoseRate + glc.inc
    }

    override val toString: String = "GlucoseUpdate"
  }

  case object Eject
      extends Transformation[(VesselCell, (Glucose, SPMat3D[Double]))] {
    def transform(vcglca3d: (VesselCell, (Glucose, SPMat3D[Double]))): Unit = {
      val (vc, (glc, a3d)) = vcglca3d
      a3d(vc.loc) = glc.blood
    }

    override val toString: String = "Ejection"
  }

  case class Diffuse(val diffuser: Diffusion3DFFactor, val dt: Int)
      extends Transformation[SPMat3D[Double]] {
    def transform(substance: SPMat3D[Double]): Unit =
      diffuser.diffuse(substance, dt)
    override val toString: String = "Diffusion"
  }

  case class Uptake(val amount: Double, val limit: Double)
      extends Transformation[(BodyCell, SPMat3D[Double])] {
    def transform(bcgluc: (BodyCell, SPMat3D[Double])): Unit = {
      val (bc, gluc) = bcgluc
      val a = Math.min(limit - bc.nutrition, Math.min(amount, gluc(bc.loc)))
      gluc(bc.loc) -= a
      bc.nutrition += a
    }

    override val toString: String = "Uptake"
  }

  case class Consume(val amount: Double) extends Transformation[BodyCell] {
    def transform(bc: BodyCell): Unit = {
      bc.state match {
        case G1 | S | G2 | M => bc.nutrition -= amount
        case _ => Unit
      }
    }

    override val toString: String = "Consume"
  }

  case object AgeCell extends Transformation[BodyCell] {
    def transform(bc: BodyCell): Unit = {
      bc.age += 1
      bc.state match {
        case G1 | S | G2 | M => bc.next -= 1
        case _ => Unit
      }
    }

    override val toString: String = "AgeCell"
  }

  case object CircadianClockCheck
      extends Transformation[(BodyCell, CircadianRhythm)] {

    def transform(input: (BodyCell, CircadianRhythm)): Unit = {
      val (bc, cr) = input
      bc.transitionRate = bc.state match {
        case G1 if bc.next <= 0 => 1 - cr.per1(cr.tc)
        case G2 if bc.next <= 0 => 1 - cr.wee1(cr.tc)
        case _ if bc.next <= 0 => 1
        case _ => 0
      }
    }

    override val toString: String = "CircadianClockCheck"
  }

  case class StateUpdate(val nutReqForG0toG1: Double)
      extends Transformation[(BodyCell, Cells)] {

    def transform(input: (BodyCell, Cells)): Unit = {
      val (bc, cells) = input

      if (bc.nutrition < 0 && bc.state != G0) {
        bc.state = G0
        bc.next = 0
        bc.nutrition = 0
      }

      if (Random.roll(bc.transitionRate)) {
        bc.state match {
          case G1 | G2 if bc.deathFlag =>
            if (Random.roll(bc.ct.repairRate)) {
              bc.state = G1
              bc.next = 13 * 60
            } else {
              cells.remove(bc)
            }
          case G1 | S | G2 =>
            bc.state = CellCycleState.next(bc.state)
            bc.next = bc.ct.ccd.getRandomized(bc.state)
          case M =>
            bc.state = CellCycleState.next(bc.state)
            bc.next = bc.ct.ccd.getRandomized(bc.state)
            Random
              .choose(cells.getEmptyNeighbours(bc.loc))
              .fold(())(loc => cells.add(BodyCell(loc)))
          case _ => Unit
        }

      } else if (bc.next <= 0) {
        bc.next += 10
      }

      if (bc.state == G0) {
        if (bc.nutrition >= nutReqForG0toG1) {
          bc.state = G1
          bc.next = bc.ct.ccd.getRandomized(bc.state)
        }
      }

      if (Random.roll(bc.deathRate)) {
        bc.state match {
          case G0 => cells.remove(bc)
          case _ => bc.deathFlag = true
        }
      }

    }

    override val toString: String = "StateUpdate"

  }

  case class Decay(val amount: Double)
      extends Transformation[SPMat3D[Double]] {
    def transform(substance: SPMat3D[Double]): Unit =
      substance.transform(d => if (d > amount) d - amount else 0)

    override val toString: String = "Decay"
  }

  // LOGGING TRANSFORMATIONS
  case class LogData(val pw: PrintWriter) extends Transformation[SimData] {

    def transform(data: SimData): Unit = {
      val sc = countStates(data.cells.list)

      // make vessel cell locations zero so we can get accurate logging of tissue
      data.cells.list.collect { case vc: VesselCell => vc }.foreach {
        case vc => data.nutritions.glucose(vc.loc) = 0d
      }

      // println(
      //   f"${data.t.minute}%d ${data.cr.tc}%f ${data.glc.blood}%f ${data.glc.inc}%f " +
      //   f"${data.nutritions.glucose.sum}%f ${data.nutritions.glucose.mean}%f")
      pw.println(
        f"${data.t.minute}%d ${data.cr.tc}%f ${data.glc.blood}%f ${data.glc.inc}%f " +
          f"${data.nutritions.glucose.sum}%f ${data.nutritions.glucose.mean}%f " +
          f"${sc.total}%d ${sc.g1}%d ${sc.s}%d ${sc.g2}%d ${sc.m}%d ${sc.g0}%d")
    }

    case class StateCounts(var g1: Int = 0,
                           var s: Int = 0,
                           var g2: Int = 0,
                           var m: Int = 0,
                           var g0: Int = 0,
                           var vc: Int = 0) {
      def total: Int = g1 + s + g2 + m
    }

    def countStates(cells: Buffer[Cell]): StateCounts = {
      val sc = StateCounts()
      cells.foreach {
        case bc: BodyCell if bc.state == G1 => sc.g1 += 1
        case bc: BodyCell if bc.state == S => sc.s += 1
        case bc: BodyCell if bc.state == G2 => sc.g2 += 1
        case bc: BodyCell if bc.state == M => sc.m += 1
        case bc: BodyCell if bc.state == G0 => sc.g0 += 1
        case vc: VesselCell => sc.vc += 1
      }
      sc
    }

    override val toString: String = "Logging"
  }
}
