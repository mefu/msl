package co.mefu.msl.cca.sim
package simv2

import java.io.PrintWriter

import co.mefu.msl.core.Random

import co.mefu.msl.modeling._

import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

import Data._

object Action {

  case object StateUpdate extends Transformation[BodyCell] {

    def transform(bc: BodyCell): Unit = bc match {
      case bc if Random.roll(bc.transitionRate) =>
        bc.state = CellCycleState.next(bc.state)
        bc.next = bc.ccd.getRandomized(bc.state)
      case bc if bc.next <= 0 => bc.next += 10
      case _ => Unit
    }

    override val toString: String = "StateUpdate"
  }

  case object AgeCell extends Transformation[BodyCell] {
    def transform(bc: BodyCell): Unit = {
      bc.age += 1
      bc.next -= 1
    }

    override val toString: String = "AgeCell"
  }

  case class CircadianTimeUpdate(val period: Double)
      extends Transformation[(Time, CircadianRhythm)] {

    val Ps_a0 = 2.328
    val Ps_a1 = -0.2472
    val Ps_b1 = 1.581
    val Ps_a2 = 1.046
    val Ps_b2 = -0.8355
    val Ps_a3 = 0.3293
    val Ps_b3 = -0.2569
    val Ps_a4 = 0.347
    val Ps_b4 = 0.309
    val Ps_a5 = -0.05678
    val Ps_b5 = 0.1758
    val Ps_w = 0.2718

    def transform(tcr: (Time, CircadianRhythm)): Unit = {
      val (t, cr) = tcr
      cr.th = (t.minute % 1440) / 60

      // cr.tc = (data.cr.phase % (2 * Math.PI)) * 12 / Math.PI
      // cr.tc = (t.minute/60.0)-24*Math.floor(t.minute/1440.0)
      cr.tc = (t.minute % 1440d) / 60d

      // cr.phase += 2 * Math.PI / period
      cr.phase = t.minute * 2 * Math.PI / period

      val Ps_x = cr.phase * 12 / Math.PI

      cr.sens = Ps_a0 + Ps_a1 * Math.cos(Ps_x * Ps_w) + Ps_b1 * Math.sin(
        Ps_x * Ps_w)
      cr.sens += Ps_a2 * Math.cos(2 * Ps_x * Ps_w) + Ps_b2 * Math.sin(
        2 * Ps_x * Ps_w)
      cr.sens += Ps_a3 * Math.cos(3 * Ps_x * Ps_w) + Ps_b3 * Math.sin(
        3 * Ps_x * Ps_w)
      cr.sens += Ps_a4 * Math.cos(4 * Ps_x * Ps_w) + Ps_b4 * Math.sin(
        4 * Ps_x * Ps_w)
      cr.sens += Ps_a5 * Math.cos(5 * Ps_x * Ps_w) + Ps_b5 * Math.sin(
        5 * Ps_x * Ps_w)
      cr.sens = cr.sens * Math.PI / 12
    }

    override val toString: String = "CircadianTimeUpdate"

  }

  case object CircadianClockCheck
      extends Transformation[(BodyCell, CircadianRhythm)] {

    def transform(input: (BodyCell, CircadianRhythm)): Unit = {
      val (bc, cr) = input
      bc.transitionRate = bc.state match {
        case G1 if bc.next <= 0 => 1 - cr.per1(cr.tc)
        case G2 if bc.next <= 0 => 1 - cr.wee1(cr.tc)
        case _ if bc.next <= 0 => 1
        case _ => 0
      }
    }

    override val toString: String = "CircadianClockCheck"
  }

  case class LogData(val pw: PrintWriter) extends Transformation[SimData] {

    def transform(data: SimData): Unit = {
      val sc = countStates(data.cells)

      // println(f"${data.t.minute}%d ${sc.total}%d ${sc.g1}%d ${sc.s}%d ${sc.g2}%d ${sc.m}%d")
      pw.println(
        f"${data.t.minute}%d ${sc.g1}%d ${sc.s}%d ${sc.g2}%d ${sc.m}%d")
    }

    case class StateCounts(var g1: Int = 0,
                           var s: Int = 0,
                           var g2: Int = 0,
                           var m: Int = 0)

    def countStates(cells: Array[Cell]): StateCounts = {
      val sc = StateCounts()
      cells.foreach {
        case bc: BodyCell if bc.state == G1 => sc.g1 += 1
        case bc: BodyCell if bc.state == S => sc.s += 1
        case bc: BodyCell if bc.state == G2 => sc.g2 += 1
        case bc: BodyCell if bc.state == M => sc.m += 1
      }
      sc
    }

    override val toString: String = "Logging"
  }

}
