package co.mefu.msl.cca.sim
package simv2

import java.io.PrintWriter

import co.mefu.msl.core.io.IO

import co.mefu.msl.modeling._
import co.mefu.msl.modeling.Substitution._
import co.mefu.msl.modeling.SubbedTransformation._

import co.mefu.msl.cca.action.PassTime
import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

import Data._
import Action._

class SimV2(val output: PrintWriter) extends Simulation[SimData] {

  implicit val bcsSubsSim: Substitution[BodyCell, SimData] =
    Substitution("bcsSubsSim",
                 sd => sd.cells.collect { case bc: BodyCell => bc })

  implicit val timeSubsSim: Substitution[Time, SimData] =
    Substitution("timeSubsSim", sd => Array(sd.t))

  implicit val crSubsSim: Substitution[CircadianRhythm, SimData] =
    Substitution("crSubsSim", sd => Array(sd.cr))

  val data = SimData(Time(0),
                     CircadianRhythm(),
                     (0 until 3500).map(_ => BodyCell()).toArray)

  val sim = ChainedTransformation[SimData](
    PassTime,
    CircadianTimeUpdate(24 * 60),
    ChainedTransformation[(BodyCell, CircadianRhythm)](
      AgeCell,
      CircadianClockCheck,
      StateUpdate
    ),
    LogData(output)
  )

  def transform(sd: SimData): Unit = sim(sd)

}

object SimV2 extends App {

  // create required folders
  IO.createFolder("../output/data/simv2/")
  IO.createFolder("../output/plot/simv2/")

  val pw = IO.getOutputWriter("../output/data/simv2/data.dat")

  val simv2 = new SimV2(pw)

  simv2.runWhile(data => data.t.minute <= 30 * 24 * 60)

  pw.close
}
