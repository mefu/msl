package co.mefu.msl.cca.sim
package simv2

import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

object Data {

  sealed abstract class Cell {
    val uuid: String = java.util.UUID.randomUUID.toString

    override def equals(that: Any): Boolean = {
      that match {
        case that: Cell => uuid.equals(that.uuid)
        case _ => false
      }
    }
  }

  case class BodyCell(
      var state: CellCycleState = G1,
      var age: Double = 0,
      var next: Double = CellCycleDurations.normal.getRandomized(G1),
      var transitionRate: Double = 0,
      val ccd: CellCycleDurations = CellCycleDurations.normal
  ) extends Cell

  case class ExternalElem(val peak: Double, val strength: Double) {
    def apply(t: Double): Double =
      (1 - Math.cos((t - peak - 12) * Math.PI / 12)) * strength / 2
  }

  case class CircadianRhythm(
      var th: Double = 0,
      var tc: Double = 0,
      var phase: Double = 0,
      var sens: Double = 0,
      val per1: ExternalElem = ExternalElem(12, 1),
      val wee1: ExternalElem = ExternalElem(22, 1)
  )

  case class SimData(val t: Time,
                     val cr: CircadianRhythm,
                     val cells: Array[Cell]) {
    override def toString: String = t.toString
  }

}
