package co.mefu.msl.cca.sim
package simv3

import co.mefu.msl.core.ds.Map3D
import co.mefu.msl.core.ds.Map3D.HasLoc
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.geometry.shape._

import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

object Data {

  // CELL DATA STRUCTURE
  sealed trait CellType {
    def deathRate: Double
    def repairRate: Double
    def canPhaseResponse: Boolean
    def canMove: Boolean
    def ccd: CellCycleDurations
  }

  case object NormalCT extends CellType {
    val deathRate: Double = 0.473
    val repairRate: Double = 0.2
    val canPhaseResponse: Boolean = true
    val canMove: Boolean = false
    val ccd: CellCycleDurations = CellCycleDurations.normal
  }

  sealed abstract class Cell extends HasLoc {
    val uuid: String = java.util.UUID.randomUUID.toString

    override def equals(that: Any): Boolean = {
      that match {
        case that: Cell => uuid.equals(that.uuid)
        case _ => false
      }
    }
  }

  case object EmptyCell extends Cell {
    val loc: SPVec.S3[Int] = SPVec.S3(0, 0, 0) // dummy
  }

  case class BloodCell(val loc: SPVec.S3[Int]) extends Cell

  case class BodyCell(
      var loc: SPVec.S3[Int],
      // cell cycle
      var state: CellCycleState = G1,
      var age: Double = 0,
      var next: Double = NormalCT.ccd.getRandomized(G1),
      // rates
      var transitionRate: Double = 0,
      var deathRate: Double = 1 - Math.pow(1 - NormalCT.deathRate,
                                           NormalCT.ccd.inverseTotal),
      // flags
      var deathFlag: Boolean = false,
      // static data
      val ct: CellType = NormalCT
  ) extends Cell

  case class ExternalElem(val peak: Double, val strength: Double) {
    def apply(t: Double): Double =
      (1 - Math.cos((t - peak - 12) * Math.PI / 12)) * strength / 2
  }

  // OVERALL DATA STRUCTURE
  case class CircadianRhythm(
      var th: Double = 0,
      var tc: Double = 0,
      var phase: Double = 0,
      var sens: Double = 0,
      val per1: ExternalElem = ExternalElem(12, 1),
      val wee1: ExternalElem = ExternalElem(22, 1)
  )

  type Cells = Map3D[Cell]

  case class SimData(val t: Time, val cr: CircadianRhythm, val cells: Cells)

}
