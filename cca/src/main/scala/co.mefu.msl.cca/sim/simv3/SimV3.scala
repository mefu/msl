package co.mefu.msl.cca.sim
package simv3

import java.io.PrintWriter

import scala.collection.mutable.Buffer

import spire.implicits._

import co.mefu.msl.core.io.IO
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.geometry.shape._

import co.mefu.msl.modeling._
import co.mefu.msl.modeling.Transformation._
import co.mefu.msl.modeling.Substitution._
import co.mefu.msl.modeling.SubbedTransformation._

import co.mefu.msl.cca.action.PassTime
import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

import Data._
import Action._

class SimV3(val output: PrintWriter) extends Simulation[SimData] {

  // IMPLICIT SUBSTITUTIONS
  implicit val bcsSubsSim: Substitution[BodyCell, SimData] =
    Substitution("bcsSubsSim",
                 sd => sd.cells.list.collect { case bc: BodyCell => bc })

  implicit val blcsSubsSim: Substitution[BloodCell, SimData] =
    Substitution("blcsSubsSim",
                 sd => sd.cells.list.collect { case blc: BloodCell => blc })

  implicit val timeSubsSim: Substitution[Time, SimData] =
    Substitution("timeSubsSim", sd => Array(sd.t))

  implicit val crSubsSim: Substitution[CircadianRhythm, SimData] =
    Substitution("crSubsSim", sd => Array(sd.cr))

  implicit val cellsSubsSim: Substitution[Cells, SimData] =
    Substitution("cellsSubsSim", sd => Array(sd.cells))

  // DATA INITIALIZATION
  val n = 20
  val nhalf = n / 2

  val size = SPVec.S3(n, n, n)
  val halfSize = SPVec.S3(nhalf, nhalf, nhalf)

  val lineShape =
    new Line(SPVec.S3(nhalf, 0, nhalf), SPVec.S3(nhalf, n - 1, nhalf), 1)
  val sphereShape = new Sphere(halfSize, nhalf - 1)

  val bloodCells = lineShape.getPoints.map(BloodCell(_))
  val bodyCells = sphereShape.getPoints
    .filterNot(lineShape.getPoints.toSet)
    .map(i => BodyCell(i))
  val cells = new Cells(EmptyCell, size)
  cells.add(bloodCells: _*)
  cells.add(bodyCells: _*)

  val data = SimData(Time(0), CircadianRhythm(), cells)

  // SIMULATION AND TRANSFORMATIONS INITIALIZATION
  val sim = ChainedTransformation[SimData](
    PassTime,
    CircadianTimeUpdate(24 * 60),
    ChainedTransformation[(BodyCell, (CircadianRhythm, Cells))](
      AgeCell,
      CircadianClockCheck,
      StateUpdate
    ),
    LogData(output)
  )

  def transform(sd: SimData): Unit = sim(sd)

}

object SimV3 extends App {

  // create required folders
  IO.createFolder("../output/data/simv3/")
  IO.createFolder("../output/plot/simv3/")

  val pw = IO.getOutputWriter("../output/data/simv3/data.dat")

  val simv3 = new SimV3(pw)

  simv3.runWhile(data => data.t.minute <= 30 * 24 * 60)

  pw.close
}
