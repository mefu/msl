package co.mefu.msl.cca.sim
package simv4

import java.io.PrintWriter

import co.mefu.msl.core.io.IO
import co.mefu.msl.core.geometry._
import co.mefu.msl.core.geometry.shape._

import co.mefu.msl.modeling._
import co.mefu.msl.modeling.Transformation._
import co.mefu.msl.modeling.Substitution._
import co.mefu.msl.modeling.SubbedTransformation._

import co.mefu.msl.cca.action.PassTime
import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

import Data._
import Action._

class SimV4(val output: PrintWriter) extends Simulation[SimData] {

  implicit val timeSubsSim: Substitution[Time, SimData] =
    Substitution("timeSubsSim", sd => Array(sd.t))

  implicit val crSubsSim: Substitution[CircadianRhythm, SimData] =
    Substitution("crSubsSim", sd => Array(sd.cr))

  implicit val glcSubsSim: Substitution[Glucose, SimData] =
    Substitution("glcSubsSim", sd => Array(sd.glc))

  // DATA INITIALIZATION
  val data = SimData(Time(0), CircadianRhythm(), Glucose())

  val sim = ChainedTransformation[SimData](
    PassTime,
    CircadianTimeUpdate(24 * 60),
    GlucoseUpdate(80, Array[Int](7 * 60, 13 * 60, 19 * 60), 50, 60, 300),
    LogData(output)
  )

  def transform(sd: SimData): Unit = sim(sd)
}

object SimV4 extends App {

  // create required folders
  IO.createFolder("../output/data/simv4/")
  IO.createFolder("../output/plot/simv4/")

  val pw = IO.getOutputWriter("../output/data/simv4/data.dat")

  val simv4 = new SimV4(pw)

  simv4.runWhile(data => data.t.minute <= 24 * 60)

  pw.close
}
