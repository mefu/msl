package co.mefu.msl.cca.sim
package simv4

import co.mefu.msl.core.ds.Map3D
import co.mefu.msl.core.ds.Map3D.HasLoc
import co.mefu.msl.core.geometry._
import co.mefu.msl.core.geometry.shape._

import co.mefu.msl.cca.data.Time

object Data {

  case class CircadianRhythm(
      var ts: Int = 0,
      var th: Int = 0,
      var tc: Double = 0
  )

  case class Glucose(
      var blood: Double = 0,
      var inc: Double = 0
  )

  case class SimData(val t: Time, val cr: CircadianRhythm, val glc: Glucose)

}
