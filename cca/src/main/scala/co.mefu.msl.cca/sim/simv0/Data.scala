package co.mefu.msl.cca.sim
package simv0

import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

object Data {

  sealed abstract class Cell {
    val uuid: String = java.util.UUID.randomUUID.toString

    override def equals(that: Any): Boolean = {
      that match {
        case that: Cell => uuid.equals(that.uuid)
        case _ => false
      }
    }
  }

  case class BodyCell(
      var state: CellCycleState = G1,
      var age: Double = 0,
      var next: Double = CellCycleDurations.normal.getRandomized(G1),
      val ccd: CellCycleDurations = CellCycleDurations.normal
  ) extends Cell

  case class SimData(var t: Time, val cells: Array[Cell])

}
