package co.mefu.msl.cca.sim
package simv0

import java.io.PrintWriter

import co.mefu.msl.modeling._

import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

import Data._

object Action {

  case class StateUpdate(val randomized: Boolean = false)
      extends Transformation[BodyCell] {
    def transform(bc: BodyCell): Unit = {
      if (bc.next <= 0) {
        bc.state = CellCycleState.next(bc.state)
        bc.next =
          if (randomized) bc.ccd.getRandomized(bc.state)
          else bc.ccd.get(bc.state)
      }
    }

    override val toString: String = "StateUpdate"
  }

  case object AgeCell extends Transformation[BodyCell] {
    def transform(bc: BodyCell): Unit = {
      bc.age += 1
      bc.next -= 1
    }

    override val toString: String = "AgeCell"
  }

  case class LogData(val pw: PrintWriter) extends Transformation[SimData] {

    def transform(data: SimData): Unit = {
      val sc = countStates(data.cells)

      // println(s"TIME: ${data.t}, G1: ${g1}, S: ${s}, G2: ${g2}, M: ${m}")
      pw.println(s"${data.t} ${sc.g1} ${sc.s} ${sc.g2} ${sc.m}")
    }

    case class StateCounts(var g1: Int = 0,
                           var s: Int = 0,
                           var g2: Int = 0,
                           var m: Int = 0)

    def countStates(cells: Array[Cell]): StateCounts = {
      val sc = StateCounts()
      cells.foreach {
        case bc: BodyCell if bc.state == G1 => sc.g1 += 1
        case bc: BodyCell if bc.state == S => sc.s += 1
        case bc: BodyCell if bc.state == G2 => sc.g2 += 1
        case bc: BodyCell if bc.state == M => sc.m += 1
      }
      sc
    }

    override val toString: String = "Logging"
  }

}
