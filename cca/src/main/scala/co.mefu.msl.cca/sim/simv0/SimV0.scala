package co.mefu.msl.cca.sim
package simv0

import java.io.PrintWriter

import co.mefu.msl.core.io.IO

import co.mefu.msl.modeling._
import co.mefu.msl.modeling.Substitution._
import co.mefu.msl.modeling.SubbedTransformation._

import co.mefu.msl.cca.action.PassTime
import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

import Data._
import Action._

class SimV0(val output: PrintWriter) extends Simulation[SimData] {

  implicit val timeSubsSim: Substitution[Time, SimData] =
    Substitution("timeSubsSim", sd => Array(sd.t))

  implicit val bcsSubsSim: Substitution[BodyCell, SimData] =
    Substitution("bcsSubsSim",
                 sd => sd.cells.collect { case bc: BodyCell => bc })

  val data = SimData(Time(0), (0 until 2000).map(_ => BodyCell()).toArray)

  val sim = ChainedTransformation[SimData](
    PassTime,
    ChainedTransformation[BodyCell](
      AgeCell,
      StateUpdate(true)
    ),
    LogData(output)
  )

  def transform(sd: SimData): Unit = sim(sd)

}

object SimV0 extends App {

  // create required folders
  IO.createFolder("../output/data/simv0/")
  IO.createFolder("../output/plot/simv0/")

  val pw = IO.getOutputWriter("../output/data/simv0/data.dat")

  val simv0 = new SimV0(pw)

  simv0.runWhile(data => data.t.minute <= CellCycleDurations.normal.total * 30)

  pw.close
}
