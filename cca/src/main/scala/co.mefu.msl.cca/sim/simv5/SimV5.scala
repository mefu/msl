package co.mefu.msl.cca.sim
package simv5

import java.io.PrintWriter

import scala.collection.mutable.Buffer

import co.mefu.msl.core.Random
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.geometry.shape._

import co.mefu.msl.modeling._
import co.mefu.msl.modeling.Transformation._
import co.mefu.msl.modeling.Substitution._
import co.mefu.msl.modeling.SubbedTransformation._

import co.mefu.msl.cca.action.PassTime
import co.mefu.msl.cca.data.Time
import co.mefu.msl.cca.data.CellCycle._

import Data._
import Action._

object SimV5 extends Simulation[SimData] {

  // DATA INITIALIZATION
  val n = 20
  val nhalf = n / 2
  val nquarter = n / 4

  val size = SPVec.S3(n, n, n)
  val halfSize = SPVec.S3(nhalf, nhalf, nhalf)
  val quarterSize = SPVec.S3(nquarter, nquarter, nquarter)

  val bvshape1 = new RandomLine3D(SPVec.S3(0, nquarter, nquarter),
                                  SPVec.S3(n - 1, nquarter, nquarter),
                                  0,
                                  3)
  val bvshape2 = new RandomLine3D(SPVec.S3(nquarter, 0, nhalf + nquarter),
                                  SPVec.S3(nquarter, n - 1, nhalf + nquarter),
                                  1,
                                  3)
  val bvshape3 = new RandomLine3D(
    SPVec.S3(nhalf + nquarter, nhalf + nquarter, 0),
    SPVec.S3(nhalf + nquarter, nhalf + nquarter, n - 1),
    2,
    3)

  val bloodCells =
    (bvshape1.getPoints ++ bvshape2.getPoints ++ bvshape3.getPoints)
      .map(VesselCell(_))
  val cells = new Cells(EmptyCell, size)
  cells.add(bloodCells: _*)

  val data = SimData(Time(0), cells)

  // SIMULATION AND TRANSFORMATIONS INITIALIZATION
  def transform(sd: SimData): Unit = Unit
}
