package co.mefu.msl.cca.action

import co.mefu.msl.modeling.Transformation

import co.mefu.msl.cca.data.Time

case object PassTime extends Transformation[Time] {
  def transform(t: Time): Unit = t.minute += 1
  override val toString: String = "PassTime"
}