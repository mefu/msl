// package co.mefu.msl.cca.action

// import goggles._

// import co.mefu.msl.core.geometry.P3DInt
// import co.mefu.msl.core.geometry.Array3D

// import co.mefu.msl.cca.data.CellTypes._
// import co.mefu.msl.cca.data.CellCycleTypes._

// class DrugCheck(val dRateMax: Double) {

//   def apply(cell: Cell, drug1: Double, drug2: Double): Cell =
//     cell match {
//       case bc: BodyCell[_] if bc.state == S =>
//         set"$bc.deathRate" := delta(drug1, bc.ccData.deathRate, bc.ccData.ccDur.inverseTotal)
//       case bc: BodyCell[_] if bc.state == G1 =>
//         set"$bc.deathRate" := delta(drug2, bc.ccData.deathRate, bc.ccData.ccDur.inverseTotal)
//       case bc: BodyCell[_] =>
//         set"$bc.deathRate" := delta(0, bc.ccData.deathRate, bc.ccData.ccDur.inverseTotal)
//       case _ => cell
//     }

//   def delta(d: Double, dr: Double, i: Double) = 1 - Math.pow(1 - (dRateMax * d + dr), i)

// }
