import sbt._
import Keys._

object Commons {
  val appVersion = "0.0.1"
  val scalaVer = "2.12.2"
  val org = "co.mefu"

  val settings: Seq[Def.Setting[_]] = Seq(
    version := appVersion,
    scalaVersion := scalaVer,
    organization := org,
    fork := true,
    cancelable := true,
    // connectInput in run := true,
    logBuffered in Test := false,
    scalacOptions ++= Seq(
      "-feature",
      "-deprecation",
      "-Yrangepos",
      "-language:higherKinds",
      "-language:implicitConversions",
      "-language:postfixOps"
    ),
    javaOptions in run ++= Seq(
      // "-agentlib:hprof=cpu=samples,depth=10",
      "-Dlogback.configurationFile=../project/logback.xml",
      "-Xms1024m",
      "-Xmx4096m"
    ),
    classpathTypes += "maven-plugin",
    resolvers += Opts.resolver.mavenLocalFile,
    resolvers += Resolver.sonatypeRepo("releases"),
    resolvers += Resolver.sonatypeRepo("snapshots"),
    resolvers += Resolver.jcenterRepo,
    libraryDependencies ++= Dependencies.common
    // initialCommands in (Test, console) := """ammonite.Main().run()"""
  )
}
