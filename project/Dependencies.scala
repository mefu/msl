import sbt._

object Dependencies {

  lazy val scalaReflect = "org.scala-lang" % "scala-reflect" % Commons.scalaVer
  lazy val scalaTest = Seq(
    "org.scalactic" %% "scalactic" % "3.0.1",
    "org.scalatest" %% "scalatest" % "3.0.1" % "test"
  )
  lazy val logback = "ch.qos.logback" % "logback-classic" % "1.1.8"
  lazy val logging = "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0"
  lazy val shapeless = "com.chuusai" %% "shapeless" % "2.3.2"
  lazy val spire = "org.typelevel" %% "spire" % "0.14.1"
  lazy val cats = "org.typelevel" %% "cats" % "0.9.0"
  lazy val enumeratum = "com.beachape" %% "enumeratum" % "1.5.10"
  lazy val ammonite = "com.lihaoyi" % "ammonite" % "0.8.2" % "test" cross CrossVersion.full

  lazy val fitting = "org.orangepalantir" % "leastsquares" % "1.0.0"

  lazy val goggles = Seq(
    "com.github.kenbot" %% "goggles-dsl" % "1.0",
    "com.github.kenbot" %% "goggles-macros" % "1.0"
  )

  lazy val nd4j = Seq(
    "org.nd4j" % "nd4j" % "0.8.0",
    "org.nd4j" % "nd4j-native-platform" % "0.8.0",
    "org.nd4j" % "nd4j-native" % "0.8.0" classifier "linux-x86_64"
  )

  lazy val lwjgl = Seq(
    "org.lwjgl" % "lwjgl" % "3.1.1",
    "org.lwjgl" % "lwjgl-glfw" % "3.1.1",
    "org.lwjgl" % "lwjgl-jemalloc" % "3.1.1",
    "org.lwjgl" % "lwjgl-openal" % "3.1.1",
    "org.lwjgl" % "lwjgl-opengl" % "3.1.1",
    "org.lwjgl" % "lwjgl-stb" % "3.1.1",
    "org.lwjgl" % "lwjgl" % "3.1.1" classifier "natives-linux",
    "org.lwjgl" % "lwjgl-glfw" % "3.1.1" classifier "natives-linux",
    "org.lwjgl" % "lwjgl-jemalloc" % "3.1.1" classifier "natives-linux",
    "org.lwjgl" % "lwjgl-openal" % "3.1.1" classifier "natives-linux",
    "org.lwjgl" % "lwjgl-opengl" % "3.1.1" classifier "natives-linux",
    "org.lwjgl" % "lwjgl-stb" % "3.1.1" classifier "natives-linux",
    "org.joml" % "joml" % "1.9.2"
  )

  lazy val common = scalaTest ++ Seq(
    logback,
    logging,
    cats,
    spire,
    shapeless,
    enumeratum
  )

  lazy val core = Seq(fitting)

  lazy val linalg = Seq()

  lazy val slwjgl = lwjgl ++ Seq()

  lazy val vis = Seq()

  lazy val modeling = Seq()

  lazy val cca = Seq()

  lazy val benchmark = Seq()
}
