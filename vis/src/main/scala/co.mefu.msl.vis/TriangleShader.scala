package co.mefu.msl.vis

import org.lwjgl._
import org.lwjgl.glfw._
import org.lwjgl.opengl._
import org.lwjgl.system._

import java.nio._

import org.lwjgl.glfw.Callbacks._
import org.lwjgl.glfw.GLFW._
import org.lwjgl.system.MemoryStack._
import org.lwjgl.system.MemoryUtil._

import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL20._
import org.lwjgl.opengl.GL30._

import org.joml.Matrix4f

import co.mefu.msl.slwjgl.engine.Window
import co.mefu.msl.slwjgl.engine.IGameLogic
import co.mefu.msl.slwjgl.engine.GameEngine
import co.mefu.msl.slwjgl.engine.Timer

object TriangleShader extends App {

  val gameLogic = new IGameLogic {
    val vertexSource = """
      #version 150 core

      in vec3 position;
      in vec3 color;

      out vec3 vertexColor;

      uniform mat4 model;
      uniform mat4 view;
      uniform mat4 projection;
      uniform float timer;

      void main() {
          mat4 rotation = mat4(
            vec4(cos(timer),    sin(timer),   0.0, 0.0),
            vec4(-sin(timer),   cos(timer),   0.0, 0.0),
            vec4(0.0,           0.0,          1.0, 0.0),
            vec4(0.0,           0.0,          0.0, 1.0)
          );
          vertexColor = color;
          mat4 mvp = projection * view * model;
          gl_Position = mvp * rotation * vec4(position, 1.0);
      }
    """

    val fragmentSource = """
      #version 150 core

      in vec3 vertexColor;

      out vec4 fragColor;

      void main() {
          fragColor = vec4(vertexColor, 1.0);
      }
    """

    val fb: FloatBuffer = MemoryUtil.memAllocFloat(16)
    var vao: Int = 0
    var vbo: Int = 0
    var vertexShader: Int = 0
    var fragmentShader: Int = 0
    var shaderProgram: Int = 0
    var uniTimer: Int = 0
    var width: Int = 0
    var height: Int = 0

    def init(): Unit = {
      vao = glGenVertexArrays()
      glBindVertexArray(vao)

      val stack: MemoryStack = MemoryStack.stackPush()
      val vertices: FloatBuffer = stack.mallocFloat(3 * 6)
      vertices.put(-0.6f).put(-0.4f).put(0f).put(1f).put(0f).put(0f)
      vertices.put(0.6f).put(-0.4f).put(0f).put(0f).put(1f).put(0f)
      vertices.put(0f).put(0.6f).put(0f).put(0f).put(0f).put(1f)
      vertices.flip()
      MemoryStack.stackPop()

      vbo = glGenBuffers()
      glBindBuffer(GL_ARRAY_BUFFER, vbo)
      glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW)

      vertexShader = glCreateShader(GL_VERTEX_SHADER)
      glShaderSource(vertexShader, vertexSource)
      glCompileShader(vertexShader)

      fragmentShader = glCreateShader(GL_FRAGMENT_SHADER)
      glShaderSource(fragmentShader, fragmentSource)
      glCompileShader(fragmentShader)

      shaderProgram = glCreateProgram()
      glAttachShader(shaderProgram, vertexShader)
      glAttachShader(shaderProgram, fragmentShader)
      glBindFragDataLocation(shaderProgram, 0, "fragColor")
      glLinkProgram(shaderProgram)

      glUseProgram(shaderProgram)

      val floatSize: Int = 4

      val posAttrib: Int = glGetAttribLocation(shaderProgram, "position")
      glEnableVertexAttribArray(posAttrib)
      glVertexAttribPointer(posAttrib, 3, GL_FLOAT, false, 6 * floatSize, 0)

      val colAttrib: Int = glGetAttribLocation(shaderProgram, "color")
      glEnableVertexAttribArray(colAttrib)
      glVertexAttribPointer(colAttrib,
                            3,
                            GL_FLOAT,
                            false,
                            6 * floatSize,
                            3 * floatSize)

      val uniModel: Int = glGetUniformLocation(shaderProgram, "model")
      val model: Matrix4f = new Matrix4f()
      glUniformMatrix4fv(uniModel, false, model.get(fb))

      val uniView: Int = glGetUniformLocation(shaderProgram, "view")
      val view: Matrix4f = new Matrix4f()
      glUniformMatrix4fv(uniView, false, view.get(fb))

      val uniProjection: Int =
        glGetUniformLocation(shaderProgram, "projection")
      val projection: Matrix4f =
        new Matrix4f().setOrtho(-1f, 1f, -1f, 1f, -1f, 1f)
      glUniformMatrix4fv(uniProjection, false, projection.get(fb))

      uniTimer = glGetUniformLocation(shaderProgram, "timer")
      glUniform1f(uniTimer, glfwGetTime().asInstanceOf[Float])

      // Set the clear color
      glClearColor(0.0f, 0.0f, 0.0f, 0.0f)
    }

    def keyCallback(window: Long,
                    key: Int,
                    scancode: Int,
                    action: Int,
                    mods: Int): Unit = {
      (key, scancode, action, mods) match {
        case (GLFW_KEY_SPACE, _, GLFW_PRESS, _) => println("SPACE PRESSED")
        case (GLFW_KEY_ESCAPE, _, GLFW_PRESS, _) =>
          glfwSetWindowShouldClose(window, true)
        case _ => Unit
      }
    }

    def resizeCallback(window: Long, width: Int, height: Int): Unit = {
      this.width = width
      this.height = height

      val ratio = width.toFloat / height.toFloat

      val uniProjection: Int =
        glGetUniformLocation(shaderProgram, "projection")
      val projection: Matrix4f =
        new Matrix4f().setOrtho(-ratio, ratio, -1f, 1f, -1f, 1f)
      glUniformMatrix4fv(uniProjection, false, projection.get(fb))
    }

    def update(interval: Float): Unit = {}

    def render(): Unit = {
      // clear
      glClear(GL_COLOR_BUFFER_BIT)

      // set viewport
      glViewport(0, 0, width, height)

      // update timer
      glUniform1f(uniTimer, glfwGetTime().asInstanceOf[Float])

      // clear and draw
      glDrawArrays(GL_TRIANGLES, 0, 3)
    }

    def cleanup(): Unit = {
      MemoryUtil.memFree(fb)
      glDeleteVertexArrays(vao)
      glDeleteBuffers(vbo)
      glDeleteShader(vertexShader)
      glDeleteShader(fragmentShader)
      glDeleteProgram(shaderProgram)
    }
  }

  val window = new Window(640, 480, "Hello World!", 3, 2, true)
  val gameEngine = new GameEngine(window, gameLogic)
  gameEngine.start()

}
