package co.mefu.msl.vis

import org.lwjgl._
import org.lwjgl.glfw._
import org.lwjgl.opengl._
import org.lwjgl.system._

import java.nio._

import org.lwjgl.glfw.Callbacks._
import org.lwjgl.glfw.GLFW._
import org.lwjgl.opengl.GL11._
import org.lwjgl.system.MemoryStack._
import org.lwjgl.system.MemoryUtil._

import co.mefu.msl.slwjgl.engine.Window
import co.mefu.msl.slwjgl.engine.IGameLogic
import co.mefu.msl.slwjgl.engine.GameEngine
import co.mefu.msl.slwjgl.engine.Timer

object HelloWorld extends App {

  val gameLogic = new IGameLogic {
    def init(): Unit = {
      println("Hello LWJGL " + Version.getVersion() + "!");
      glClearColor(0.0f, 0.0f, 0.0f, 0.0f)
    }

    def keyCallback(window: Long,
                    key: Int,
                    scancode: Int,
                    action: Int,
                    mods: Int): Unit = {
      (key, scancode, action, mods) match {
        case (GLFW_KEY_SPACE, _, GLFW_PRESS, _) => println("SPACE PRESSED")
        case (GLFW_KEY_ESCAPE, _, GLFW_PRESS, _) =>
          glfwSetWindowShouldClose(window, true)
        case _ => Unit
      }
    }

    def resizeCallback(window: Long, width: Int, height: Int): Unit = {
      println(s"WINDOW RESIZED TO: $width, $height")
    }

    def update(interval: Float): Unit = Unit

    def render(): Unit = {
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    }

    def cleanup(): Unit = Unit
  }

  val window = new Window(640, 480, "Hello World!", 1, 1, true)
  val gameEngine = new GameEngine(window, gameLogic)
  gameEngine.start()

}
