package co.mefu.msl.vis

import org.lwjgl.glfw.GLFW._

import org.lwjgl.opengl.GL11._

import org.joml.Matrix4f

import co.mefu.msl.slwjgl.engine.Window
import co.mefu.msl.slwjgl.engine.IGameLogic
import co.mefu.msl.slwjgl.engine.GameEngine
import co.mefu.msl.slwjgl.engine.Timer
import co.mefu.msl.slwjgl.graph.Transformation
import co.mefu.msl.slwjgl.graph.ShaderProgram
import co.mefu.msl.slwjgl.graph.Mesh

object SquareShader extends App {

  val gameLogic = new IGameLogic {
    val vertexSource = """
    #version 330

    layout (location=0) in vec3 position;
    layout (location=1) in vec3 inColor;

    uniform mat4 projection;

    out vec3 exColor;

    void main()
    {
      gl_Position = projection * vec4(position, 1.0);
      exColor = inColor;
    }
    """

    val fragmentSource = """
    #version 330

    in  vec3 exColor;

    out vec4 fragColor;

    void main()
    {
      fragColor = vec4(exColor, 1.0);
    }
    """

    var width: Int = 0
    var height: Int = 0

    var shaderProgram: ShaderProgram = null

    var vaoId: Int = 0
    var vboId: Int = 0

    var mesh: Mesh = null
    var projection: Matrix4f = new Matrix4f()

    def init(): Unit = {
      // shaders
      shaderProgram = new ShaderProgram()
      shaderProgram.createVertexShader(vertexSource)
      shaderProgram.createFragmentShader(fragmentSource)
      shaderProgram.link()
      shaderProgram.bind()

      // create uniform
      shaderProgram.createUniform("projection")

      // init mesh
      mesh = new Mesh(
        Array[Float](
          -0.5f, 0.5f, -1.05f, -0.5f, -0.5f, -1.05f, 0.5f, -0.5f, -1.05f, 0.5f,
          0.5f, -1.05f
        ),
        Array[Float](
          0.5f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.0f, 0.5f, 0.0f, 0.5f,
          0.5f
        ),
        Array[Int](
          0, 1, 3, 3, 1, 2
        )
      )

      // Set the clear color
      glClearColor(0.0f, 0.0f, 0.0f, 0.0f)
    }

    def keyCallback(window: Long,
                    key: Int,
                    scancode: Int,
                    action: Int,
                    mods: Int): Unit = {
      (key, scancode, action, mods) match {
        case (GLFW_KEY_SPACE, _, GLFW_PRESS, _) => println("SPACE PRESSED")
        case (GLFW_KEY_ESCAPE, _, GLFW_PRESS, _) =>
          glfwSetWindowShouldClose(window, true)
        case _ => Unit
      }
    }

    def resizeCallback(window: Long, width: Int, height: Int): Unit = {
      this.width = width
      this.height = height

      projection =
        Transformation.projection(ratio = width.toFloat / height.toFloat)
    }

    def update(interval: Float): Unit = {}

    def render(): Unit = {
      // clear
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

      // set viewport
      glViewport(0, 0, width, height)

      // bind shaders
      shaderProgram.bind()

      // set projection matrix
      shaderProgram.setUniformMatrix4fv("projection", projection)

      // render the mesh
      mesh.render()

      // restore state
      shaderProgram.unbind()
    }

    def cleanup(): Unit = {
      // clean
      shaderProgram.cleanup()
      mesh.cleanup()
    }
  }

  val window = new Window(640, 480, "Hello World!", 3, 3, true)
  val gameEngine = new GameEngine(window, gameLogic)
  gameEngine.start()

}
