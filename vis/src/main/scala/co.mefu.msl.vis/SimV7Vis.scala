package co.mefu.msl.vis

import java.io._

import scala.collection.mutable.Buffer

import spire.implicits._

import co.mefu.msl.core.io.IO
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ops.SPMat3DOps._
import co.mefu.msl.cca.sim.simv7.SimV7
import co.mefu.msl.cca.sim.simv7.Data._
import co.mefu.msl.cca.data.CellCycle._

import org.lwjgl.glfw.GLFW._

import org.lwjgl.opengl.GL11._

import org.joml.Vector3f
import org.joml.Vector4f
import org.joml.Matrix4f

import co.mefu.msl.slwjgl.engine.Window
import co.mefu.msl.slwjgl.engine.IGameLogic
import co.mefu.msl.slwjgl.engine.GameEngine
import co.mefu.msl.slwjgl.engine.Timer
import co.mefu.msl.slwjgl.engine.Item
import co.mefu.msl.slwjgl.graph.Transformation
import co.mefu.msl.slwjgl.graph.ShaderProgram
import co.mefu.msl.slwjgl.graph.Mesh
import co.mefu.msl.slwjgl.graph.Camera
import co.mefu.msl.slwjgl.items.Box
import co.mefu.msl.slwjgl.items.BoxFactory
import co.mefu.msl.slwjgl.items.Grid

object SimV7Vis extends App {

  val sim = new SimV7(IO.getNullWriter)
  val n = sim.n
  // toggles:
  // 0 => Simulation Running
  // 1 => Draw blood cells
  // 2 => Draw normal cells
  // 3 => Draw glucose
  var toggles = Array[Boolean](false, true, true, true)

  val gameLogic = new IGameLogic {
    val vertexSource =
      """
    #version 330

    layout (location=0) in vec3 position;
    layout (location=1) in vec3 inColor;

    uniform mat4 projection;
    uniform mat4 view;
    uniform mat4 model;
    uniform vec4 color;

    out vec4 exColor;

    void main()
    {
      gl_Position = projection * view * model * vec4(position, 1.0);
      exColor = color;
    }
    """

    val fragmentSource = """
    #version 330

    in  vec4 exColor;

    out vec4 fragColor;

    void main()
    {
      fragColor = exColor;
    }
    """

    var width: Int = 0
    var height: Int = 0

    var shaderProgram: ShaderProgram = null

    var vaoId: Int = 0
    var vboId: Int = 0

    var items: Buffer[Item] = Buffer[Item]()
    var projection: Matrix4f = new Matrix4f()

    var camera: Camera = null
    val sensitivity: Float = 0.2f
    val rotSensitivity: Float = 2f

    var movement: Vector3f = new Vector3f(0, 0, 0)
    var rotation: Vector3f = new Vector3f(0, 0, 0)

    var gridItem: Item = null

    var bfRed: BoxFactory = null
    var bfBlue: BoxFactory = null

    def addItems(): Unit = {
      // draw blood cells
      if (toggles(1)) {
        val alpha = sim.data.glc.blood.toFloat / 130f
        sim.data.cells.list.collect { case vc: VesselCell => vc }.foreach {
          case vc =>
            items.append(
              bfRed.newBox(new Vector3f(vc.loc(0), vc.loc(1), vc.loc(2)),
                           1f,
                           new Vector4f(1f, 0f, 0f, alpha)))
        }
      }
      // draw body cells
      if (toggles(2)) {
        sim.data.cells.list.collect { case bc: BodyCell => bc }.foreach {
          case bc =>
            val color = bc.state match {
              case G1 => new Vector4f(1f, 1f, 0f, 0.3f)
              case S => new Vector4f(0f, 1f, 1f, 0.3f)
              case G2 => new Vector4f(1f, 0f, 1f, 0.3f)
              case M => new Vector4f(0f, 1f, 0f, 0.3f)
              case G0 => new Vector4f(1f, 1f, 1f, 0.3f)
              case _ => new Vector4f(0f, 0f, 0f, 0f)
            }
            items.append(
              bfRed.newBox(new Vector3f(bc.loc(0), bc.loc(1), bc.loc(2)),
                           0.2f + 0.8f * bc.nutrition.toFloat,
                           color))
        }
      }

      // draw glucose
      if (toggles(3)) {
        val max = Math.max(80, sim.data.nutritions.glucose.max).toFloat
        for (i <- 0 until sim.data.cells.locMap.x) {
          for (j <- 0 until sim.data.cells.locMap.y) {
            for (k <- 0 until sim.data.cells.locMap.z) {
              val current = sim.data.nutritions.glucose(i, j, k).toFloat
              if (current > (10 / (n * n * n))) {
                val scale = current / max
                items.append(
                  bfBlue.newBox(new Vector3f(i, j, k),
                                scale,
                                new Vector4f(0f, 1f - scale, 1f, 0.5f)))
              }
            }
          }
        }
      }
    }

    def init(): Unit = {
      // enable/disable features
      glEnable(GL_BLEND)
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

      // shaders
      shaderProgram = new ShaderProgram()
      shaderProgram.createVertexShader(vertexSource)
      shaderProgram.createFragmentShader(fragmentSource)
      shaderProgram.link()
      shaderProgram.bind()

      // create uniform
      shaderProgram.createUniform("projection")
      shaderProgram.createUniform("view")
      shaderProgram.createUniform("model")
      shaderProgram.createUniform("color")

      // init items
      bfRed = new BoxFactory(new Vector3f(1f, 0f, 0f))
      bfBlue = new BoxFactory(new Vector3f(0f, 0f, 1f))

      gridItem = Grid(new Vector3f(-0.5f, -0.5f, -0.5f), n, n / 5)

      items = Buffer[Item]()
      addItems()

      // init camera
      camera = new Camera(
        position = new Vector3f(1.2f * n, 1.6f * n, 1.4f * n),
        rotation = new Vector3f(38, -400, 0))

      // Set the clear color
      glClearColor(0.0f, 0.0f, 0.0f, 0.0f)
    }

    def keyCallback(window: Long,
                    key: Int,
                    scancode: Int,
                    action: Int,
                    mods: Int): Unit = {
      (key, scancode, action, mods) match {
        case (GLFW_KEY_SPACE, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          toggles(0) = !toggles(0)
        case (GLFW_KEY_1, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          toggles(1) = !toggles(1)
        case (GLFW_KEY_2, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          toggles(2) = !toggles(2)
        case (GLFW_KEY_3, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          toggles(3) = !toggles(3)
        case (GLFW_KEY_P, _, GLFW_PRESS, _) =>
          println(camera.position + " " + camera.rotation)
        case (GLFW_KEY_A, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.x = -sensitivity
        case (GLFW_KEY_D, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.x = +sensitivity
        case (GLFW_KEY_W, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.z = -sensitivity
        case (GLFW_KEY_S, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.z = +sensitivity
        case (GLFW_KEY_Q, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.y = -sensitivity
        case (GLFW_KEY_E, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.y = +sensitivity
        case (GLFW_KEY_LEFT, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          rotation.y = -rotSensitivity
        case (GLFW_KEY_RIGHT, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          rotation.y = +rotSensitivity
        case (GLFW_KEY_UP, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          rotation.x = +rotSensitivity
        case (GLFW_KEY_DOWN, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          rotation.x = -rotSensitivity
        case (GLFW_KEY_ESCAPE, _, GLFW_PRESS, _) =>
          glfwSetWindowShouldClose(window, true)
        case _ => Unit
      }
    }

    def resizeCallback(window: Long, width: Int, height: Int): Unit = {
      this.width = width
      this.height = height

      projection =
        Transformation.projection(ratio = width.toFloat / height.toFloat)
    }

    def update(interval: Float): Unit = {
      if (!movement.equals(new Vector3f(0, 0, 0))) {
        camera.move(movement)
        movement = new Vector3f(0, 0, 0)
      }
      if (!rotation.equals(new Vector3f(0, 0, 0))) {
        camera.rotate(rotation)
        rotation = new Vector3f(0, 0, 0)
      }

      if (toggles(0)) {
        sim(sim.data)
      }

      items.clear()
      addItems()
    }

    def render(): Unit = {

      // clear
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

      // set viewport
      glViewport(0, 0, width, height)

      // bind shaders
      shaderProgram.bind()

      // set projection matrix
      shaderProgram.setUniformMatrix4fv("projection", projection)

      // get view matrix
      val view = Transformation.view(camera.position, camera.rotation)
      shaderProgram.setUniformMatrix4fv("view", view)

      // render each item
      (items :+ gridItem).foreach(item => {
        val model =
          Transformation.model(item.position, item.rotation, item.scale)
        shaderProgram.setUniformMatrix4fv("model", model)
        shaderProgram.setUniformVector4fv("color", item.color)
        item.mesh.render()
      })

      // restore state
      shaderProgram.unbind()
    }

    def cleanup(): Unit = {
      // clean
      shaderProgram.cleanup()
      items.foreach(item => item.mesh.cleanup())
    }
  }

  val window = new Window(1280, 1024, "Hello World!", 3, 3, true)
  val gameEngine = new GameEngine(window, gameLogic)
  gameEngine.start()

}
