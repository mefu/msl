package co.mefu.msl.vis

import scala.collection.mutable.ArrayBuffer

import org.lwjgl.glfw.GLFW._

import org.lwjgl.opengl.GL11._

import org.joml.Vector3f
import org.joml.Matrix4f

import co.mefu.msl.slwjgl.engine.Window
import co.mefu.msl.slwjgl.engine.IGameLogic
import co.mefu.msl.slwjgl.engine.GameEngine
import co.mefu.msl.slwjgl.engine.Timer
import co.mefu.msl.slwjgl.engine.Item
import co.mefu.msl.slwjgl.graph.Transformation
import co.mefu.msl.slwjgl.graph.ShaderProgram
import co.mefu.msl.slwjgl.graph.Mesh
import co.mefu.msl.slwjgl.graph.Camera
import co.mefu.msl.slwjgl.items.Box

object BoxShader extends App {

  val gameLogic = new IGameLogic {
    val vertexSource =
      """
    #version 330

    layout (location=0) in vec3 position;
    layout (location=1) in vec3 inColor;

    uniform mat4 projection;
    uniform mat4 view;
    uniform mat4 model;

    out vec3 exColor;

    void main()
    {
      gl_Position = projection * view * model * vec4(position, 1.0);
      exColor = inColor;
    }
    """

    val fragmentSource = """
    #version 330

    in  vec3 exColor;

    out vec4 fragColor;

    void main()
    {
      fragColor = vec4(exColor, 1.0);
    }
    """

    var width: Int = 0
    var height: Int = 0

    var shaderProgram: ShaderProgram = null

    var vaoId: Int = 0
    var vboId: Int = 0

    var items: ArrayBuffer[Item] = ArrayBuffer[Item]()
    var projection: Matrix4f = new Matrix4f()

    var camera: Camera = null
    val sensitivity: Float = 0.05f
    val rotSensitivity: Float = 2f

    var movement: Vector3f = new Vector3f(0, 0, 0)
    var rotation: Vector3f = new Vector3f(0, 0, 0)

    def init(): Unit = {
      // shaders
      shaderProgram = new ShaderProgram()
      shaderProgram.createVertexShader(vertexSource)
      shaderProgram.createFragmentShader(fragmentSource)
      shaderProgram.link()
      shaderProgram.bind()

      // create uniform
      shaderProgram.createUniform("projection")
      shaderProgram.createUniform("view")
      shaderProgram.createUniform("model")

      // init items
      for (i <- 1 to 10) {
        for (j <- 1 to 10) {
          items.append(Box(new Vector3f(i, j, -5f)))
        }
      }

      // init camera
      camera = new Camera()

      // Set the clear color
      glClearColor(0.0f, 0.0f, 0.0f, 0.0f)
    }

    def keyCallback(window: Long,
                    key: Int,
                    scancode: Int,
                    action: Int,
                    mods: Int): Unit = {
      (key, scancode, action, mods) match {
        case (GLFW_KEY_SPACE, _, GLFW_PRESS, _) => println("SPACE PRESSED")
        case (GLFW_KEY_A, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.x = -sensitivity
        case (GLFW_KEY_D, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.x = +sensitivity
        case (GLFW_KEY_W, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.z = -sensitivity
        case (GLFW_KEY_S, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.z = +sensitivity
        case (GLFW_KEY_Q, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.y = -sensitivity
        case (GLFW_KEY_E, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          movement.y = +sensitivity
        case (GLFW_KEY_LEFT, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          rotation.y = -rotSensitivity
        case (GLFW_KEY_RIGHT, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          rotation.y = +rotSensitivity
        case (GLFW_KEY_UP, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          rotation.x = +rotSensitivity
        case (GLFW_KEY_DOWN, _, GLFW_PRESS | GLFW_REPEAT, _) =>
          rotation.x = -rotSensitivity
        case (GLFW_KEY_ESCAPE, _, GLFW_PRESS, _) =>
          glfwSetWindowShouldClose(window, true)
        case _ => Unit
      }
    }

    def resizeCallback(window: Long, width: Int, height: Int): Unit = {
      this.width = width
      this.height = height

      projection =
        Transformation.projection(ratio = width.toFloat / height.toFloat)
    }

    def update(interval: Float): Unit = {
      if (!movement.equals(new Vector3f(0, 0, 0))) {
        camera.move(movement)
        movement = new Vector3f(0, 0, 0)
      }
      if (!rotation.equals(new Vector3f(0, 0, 0))) {
        camera.rotate(rotation)
        rotation = new Vector3f(0, 0, 0)
      }
    }

    def render(): Unit = {
      // rotate
      // val time = glfwGetTime().asInstanceOf[Float] * 50
      // items(0).rotation = new Vector3f(time, 0, 0)
      // items(0).rotation = new Vector3f(0, time, 0)
      // items(0).rotation = new Vector3f(0, 0, time)

      // clear
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

      // set viewport
      glViewport(0, 0, width, height)

      // bind shaders
      shaderProgram.bind()

      // set projection matrix
      shaderProgram.setUniformMatrix4fv("projection", projection)

      // get view matrix
      val view = Transformation.view(camera.position, camera.rotation)
      shaderProgram.setUniformMatrix4fv("view", view)

      // render each item
      items.foreach(item => {
        val model =
          Transformation.model(item.position, item.rotation, item.scale)
        shaderProgram.setUniformMatrix4fv("model", model)
        item.mesh.render()
      })

      // restore state
      shaderProgram.unbind()
    }

    def cleanup(): Unit = {
      // clean
      shaderProgram.cleanup()
      items.foreach(item => item.mesh.cleanup())
    }
  }

  val window = new Window(640, 480, "Hello World!", 3, 3, true)
  val gameEngine = new GameEngine(window, gameLogic)
  gameEngine.start()

}
