package co.mefu.msl.vis

import org.lwjgl._
import org.lwjgl.glfw._
import org.lwjgl.opengl._
import org.lwjgl.system._

import java.nio._

import org.lwjgl.glfw.Callbacks._
import org.lwjgl.glfw.GLFW._
import org.lwjgl.opengl.GL11._
import org.lwjgl.system.MemoryStack._
import org.lwjgl.system.MemoryUtil._

import co.mefu.msl.slwjgl.engine.Window
import co.mefu.msl.slwjgl.engine.IGameLogic
import co.mefu.msl.slwjgl.engine.GameEngine
import co.mefu.msl.slwjgl.engine.Timer

object Triangle extends App {

  val gameLogic = new IGameLogic {
    var width: Int = 0
    var height: Int = 0
    var ratio: Float = 0f

    def init(): Unit = {
      // Set the clear color
      glClearColor(0.0f, 0.0f, 0.0f, 0.0f)
    }

    def keyCallback(window: Long,
                    key: Int,
                    scancode: Int,
                    action: Int,
                    mods: Int): Unit = {
      (key, scancode, action, mods) match {
        case (GLFW_KEY_SPACE, _, GLFW_PRESS, _) => println("SPACE PRESSED")
        case (GLFW_KEY_ESCAPE, _, GLFW_PRESS, _) =>
          glfwSetWindowShouldClose(window, true)
        case _ => Unit
      }
    }

    def resizeCallback(window: Long, width: Int, height: Int): Unit = {
      this.width = width
      this.height = height
    }

    def update(interval: Float): Unit = {
      this.ratio = width.toFloat / height.toFloat
    }

    def render(): Unit = {
      /* Set viewport and clear screen */
      glViewport(0, 0, width, height);
      glClear(GL_COLOR_BUFFER_BIT)

      /* Set ortographic projection */
      glMatrixMode(GL_PROJECTION)
      glLoadIdentity()
      glOrtho(-ratio, ratio, -1f, 1f, -1f, 1f)
      glMatrixMode(GL_MODELVIEW)

      /* Rotate matrix */
      glLoadIdentity()
      glRotatef(glfwGetTime().asInstanceOf[Float] * 50f, 0f, 0f, 1f)

      /* Render triangle */
      glBegin(GL_TRIANGLES)
      glColor3f(1f, 0f, 0f)
      glVertex3f(-0.6f, -0.4f, 0f)
      glColor3f(0f, 1f, 0f)
      glVertex3f(0.6f, -0.4f, 0f)
      glColor3f(0f, 0f, 1f)
      glVertex3f(0f, 0.6f, 0f)
      glEnd()
    }

    def cleanup(): Unit = Unit
  }

  val window = new Window(640, 480, "Hello World!", 1, 1, true)
  val gameEngine = new GameEngine(window, gameLogic)
  gameEngine.start()
}
