package co.mefu.msl.slwjgl.items

import org.joml.Vector3f
import org.joml.Vector4f

import co.mefu.msl.slwjgl.engine.Item
import co.mefu.msl.slwjgl.graph.Mesh

class BoxFactory(val color: Vector3f) {
  val vertices: Array[Float] = Array[Float](
    -0.5f, 0.5f, 0.5f, -0.5f, -0.5f, 0.5f, 0.5f, -0.5f, 0.5f, 0.5f, 0.5f, 0.5f,
    -0.5f, 0.5f, -0.5f, 0.5f, 0.5f, -0.5f, -0.5f, -0.5f, -0.5f, 0.5f, -0.5f,
    -0.5f
  )

  val borderVerts: Array[Float] =
    vertices.map(v => if (v > 0) v + 0.01f else v - 0.01f)

  val colors: Array[Float] =
    Array.fill(8)(Array[Float](color.x, color.y, color.z)).flatten

  val borderColors: Array[Float] =
    Array.fill(8)(Array[Float](1f, 1f, 1f)).flatten

  val indices: Array[Int] = Array[Int](
    0, 1, 3, 3, 1, 2, 4, 0, 3, 5, 4, 3, 3, 2, 7, 5, 3, 7, 0, 1, 6, 4, 0, 6, 6,
    1, 2, 7, 6, 2, 4, 6, 7, 5, 4, 7
  )

  val borderIndices: Array[Int] = indices.map(_ + 8)

  val mesh = new Mesh(vertices, colors, indices)

  def newBox(position: Vector3f,
             scale: Float,
             color: Vector4f = new Vector4f(0f, 0f, 0f, 0f)): Item = {
    new Item(mesh, position, scale, new Vector3f(0, 0, 0), color)
  }
}

object Box {

  val defaultBoxFactory = new BoxFactory(new Vector3f(1f, 0f, 0f))

  def apply(position: Vector3f, scale: Float = 1f): Item =
    defaultBoxFactory.newBox(position, scale)
}
