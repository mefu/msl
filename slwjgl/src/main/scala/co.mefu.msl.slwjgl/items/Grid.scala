package co.mefu.msl.slwjgl.items

import org.joml.Vector3f
import org.joml.Vector4f

import co.mefu.msl.slwjgl.engine.Item
import co.mefu.msl.slwjgl.graph.Mesh

import org.lwjgl.opengl.GL11.GL_LINES

object Grid {
  def mesh(n: Int, step: Int) = {
    val vertices: Array[Float] = {
      val zdir = for {
        i <- 0 to n by step
        j <- 0 to n by step
        if (!(i == 0 && j == 0))
      } yield (Array[Float](i, j, 0, i, j, n))

      val ydir = for {
        i <- 0 to n by step
        k <- 0 to n by step
        if (!(i == 0 && k == 0))
      } yield (Array[Float](i, 0, k, i, n, k))

      val xdir = for {
        j <- 0 to n by step
        k <- 0 to n by step
        if (!(j == 0 && k == 0))
      } yield (Array[Float](0, j, k, n, j, k))

      val axiss = Array[Float](
        0,
        0,
        0,
        0,
        0,
        n * 2,
        0,
        0,
        0,
        0,
        n * 2,
        0,
        0,
        0,
        0,
        n * 2,
        0,
        0
      )

      (xdir.flatten ++ ydir.flatten ++ zdir.flatten ++ axiss).to[Array]
    }

    val colors: Array[Float] =
      Array.fill((vertices.length / 3) - 6)(Array[Float](1f, 1f, 1f)).flatten
    val axisColors: Array[Float] = Array[Float](
      0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0
    )

    val indices: Array[Int] = (0 to vertices.length).to[Array]

    new Mesh(vertices, colors ++ axisColors, indices, GL_LINES)
  }

  def apply(position: Vector3f, n: Int, step: Int): Item = {
    new Item(mesh(n, step),
             position,
             1f,
             new Vector3f(0, 0, 0),
             new Vector4f(1f, 1f, 1f, 1f))
  }
}
