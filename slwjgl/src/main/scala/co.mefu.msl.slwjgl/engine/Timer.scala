package co.mefu.msl.slwjgl.engine

class Timer protected (
    val accumulator: Float,
    val marked: Float
) {
  import Timer._

  def mark: Timer = new Timer(accumulator + elapsed, getTime)
  def elapsed: Float = getTime - marked
  def rewind(amount: Float) = new Timer(accumulator - amount, marked)
}

object Timer {
  def apply(): Timer = new Timer(0f, getTime)
  def getTime: Float = System.nanoTime() / 1000000000f
}
