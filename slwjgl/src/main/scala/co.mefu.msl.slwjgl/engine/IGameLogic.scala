package co.mefu.msl.slwjgl.engine

trait IGameLogic {

  def init(): Unit
  def keyCallback(window: Long,
                  key: Int,
                  scancode: Int,
                  action: Int,
                  mods: Int): Unit
  def resizeCallback(window: Long, width: Int, height: Int): Unit
  def update(interval: Float): Unit
  def render(): Unit
  def cleanup(): Unit

}
