package co.mefu.msl.slwjgl.engine

import org.lwjgl._
import org.lwjgl.glfw._
import org.lwjgl.opengl._
import org.lwjgl.system._

import java.nio._

import org.lwjgl.glfw.Callbacks._
import org.lwjgl.glfw.GLFW._
import org.lwjgl.opengl.GL11._
import org.lwjgl.system.MemoryStack._
import org.lwjgl.system.MemoryUtil._

// import co.mefu.msl.slwjgl.CallbackHelpers._

class Window(
    iWidth: Int,
    iHeight: Int,
    title: String,
    vMinor: Int,
    vMajor: Int,
    val vSync: Boolean
) {

  val windowHandle = {
    glfwSetErrorCallback(GLFWErrorCallback.createPrint(System.err))

    glfwInit()

    glfwDefaultWindowHints() // optional, the current window hints are already the default
    glfwWindowHint(GLFW_VISIBLE, GL_FALSE) // the window will stay hidden after creation
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE) // the window will be resizable
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, vMinor)
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, vMajor)

    if (vMajor + vMinor * 0.1 >= 3.2) {
      glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE)
      glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE)
    }

    glfwCreateWindow(iWidth, iHeight, title, NULL, NULL)
  }

  def init(): Unit = {
    // Make the OpenGL context current
    glfwMakeContextCurrent(windowHandle)

    if (vSync) {
      // Enable v-sync
      glfwSwapInterval(1)
    }

    // Make the window visible
    glfwShowWindow(windowHandle)

    // Create capabilities for this window
    GL.createCapabilities()

    // enable depth test
    glEnable(GL_DEPTH_TEST)
  }

  def shouldClose(): Boolean =
    glfwWindowShouldClose(windowHandle)

  def setKeyCallback(keyCallback: GLFWKeyCallbackI) =
    glfwSetKeyCallback(windowHandle, keyCallback)

  def setResizeCallback(resizeCallback: GLFWWindowSizeCallbackI) =
    glfwSetWindowSizeCallback(windowHandle, resizeCallback)

  def swapBuffers() = {
    glfwSwapBuffers(windowHandle)
  }

  def pollEvents() = {
    glfwPollEvents()
  }

  def cleanup() = {
    // Free the window callbacks and destroy the window
    glfwFreeCallbacks(windowHandle)
    glfwDestroyWindow(windowHandle)

    // Terminate GLFW and free the error callback
    glfwTerminate()
    glfwSetErrorCallback(null).free()
  }
}
