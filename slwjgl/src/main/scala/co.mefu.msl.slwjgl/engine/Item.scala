package co.mefu.msl.slwjgl.engine

import org.joml.Vector3f
import org.joml.Vector4f

import co.mefu.msl.slwjgl.graph.Mesh

class Item(
    val mesh: Mesh,
    var position: Vector3f,
    var scale: Float,
    var rotation: Vector3f,
    var color: Vector4f = new Vector4f(0f, 0f, 0f, 0f)
)
