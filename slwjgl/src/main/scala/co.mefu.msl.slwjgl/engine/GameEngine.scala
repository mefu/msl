package co.mefu.msl.slwjgl.engine

class GameEngine(
    val window: Window,
    val gameLogic: IGameLogic
) extends Runnable {

  val TARGET_FPS: Int = 30
  val TARGET_UPS: Int = 30
  val LOOP_SLOT: Float = 1f / TARGET_FPS
  val TARGET_INTERVAL: Float = 1f / TARGET_UPS

  val gameLoopThread: Thread = new Thread(this, "GAME_LOOP_THREAD")

  def start() = gameLoopThread.start()

  override def run() = {
    init()
    gameLoop()
    cleanup()
  }

  def init() = {
    window.init()
    gameLogic.init()

    window.setKeyCallback(gameLogic.keyCallback)
    window.setResizeCallback(gameLogic.resizeCallback)
  }

  def gameLoop() = {
    var timer = Timer()
    while (!window.shouldClose()) {
      timer = timer.mark

      input()

      while (timer.accumulator >= TARGET_INTERVAL) {
        update(TARGET_INTERVAL)
        timer = timer.rewind(TARGET_INTERVAL)
      }

      render()

      if (!window.vSync) {
        sync(timer)
      }
    }
  }

  def input() = {
    window.pollEvents()
  }

  def update(interval: Float) = {
    gameLogic.update(interval)
  }

  def render() = {
    gameLogic.render()
    window.swapBuffers()
  }

  def sync(timer: Timer) = {
    val endTime = timer.marked + LOOP_SLOT
    while (Timer.getTime < endTime) {
      Thread.sleep(1)
    }
  }

  def cleanup() = {
    gameLogic.cleanup()
    window.cleanup()
  }
}
