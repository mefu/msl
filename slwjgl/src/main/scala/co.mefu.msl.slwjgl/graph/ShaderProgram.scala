package co.mefu.msl.slwjgl.graph

import scala.collection.mutable.{Map => MMap}

import java.nio.FloatBuffer

import org.lwjgl.system.MemoryStack

import org.lwjgl.opengl.GL20._

import org.joml.Vector3f
import org.joml.Vector4f
import org.joml.Matrix4f

class ShaderProgram {

  val programId: Int = glCreateProgram()

  var vertexShaderId: Int = 0
  var fragmentShaderId: Int = 0

  val uniforms = MMap[String, Int]()

  def createVertexShader(code: String): Unit = {
    vertexShaderId = createShader(code, GL_VERTEX_SHADER)
  }

  def createFragmentShader(code: String): Unit = {
    fragmentShaderId = createShader(code, GL_FRAGMENT_SHADER)
  }

  def createShader(code: String, shaderType: Int): Int = {
    val shaderId: Int = glCreateShader(shaderType)

    glShaderSource(shaderId, code)
    glCompileShader(shaderId)

    // println(glGetShaderi(shaderId, GL_COMPILE_STATUS))

    glAttachShader(programId, shaderId)

    shaderId
  }

  def createUniform(name: String): Unit = {
    val loc: Int = glGetUniformLocation(programId, name)
    uniforms.put(name, loc)
  }

  def setUniformMatrix4fv(name: String, value: Matrix4f) {
    val stack = MemoryStack.stackPush()
    val fb: FloatBuffer = stack.mallocFloat(16)
    value.get(fb)
    glUniformMatrix4fv(uniforms(name), false, fb)
    MemoryStack.stackPop()
  }

  def setUniformVector4fv(name: String, value: Vector4f) {
    val stack = MemoryStack.stackPush()
    val fb: FloatBuffer = stack.mallocFloat(4)
    value.get(fb)
    glUniform4fv(uniforms(name), fb)
    MemoryStack.stackPop()
  }

  def setUniformVector3fv(name: String, value: Vector3f) {
    val stack = MemoryStack.stackPush()
    val fb: FloatBuffer = stack.mallocFloat(3)
    value.get(fb)
    glUniform3fv(uniforms(name), fb)
    MemoryStack.stackPop()
  }

  def link(): Unit = {
    glLinkProgram(programId)

    // println(glGetProgrami(programId, GL_LINK_STATUS))

    glDetachShader(programId, vertexShaderId)
    glDetachShader(programId, fragmentShaderId)

    glValidateProgram(programId)

    // println(glGetProgrami(programId, GL_VALIDATE_STATUS))
  }

  def bind(): Unit = glUseProgram(programId)

  def unbind(): Unit = glUseProgram(0)

  def cleanup(): Unit = {
    unbind()
    glDeleteProgram(programId)
  }

}