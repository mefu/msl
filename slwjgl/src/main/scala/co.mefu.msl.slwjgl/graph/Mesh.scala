package co.mefu.msl.slwjgl.graph

import java.nio.IntBuffer
import java.nio.FloatBuffer

import org.lwjgl.opengl.GL11._
import org.lwjgl.opengl.GL15._
import org.lwjgl.opengl.GL20._
import org.lwjgl.opengl.GL30._
import org.lwjgl.system.MemoryUtil

class Mesh(positions: Array[Float],
           colors: Array[Float],
           indices: Array[Int],
           val glPrimitiveType: Int = GL_TRIANGLES) {

  private val floatBuffer: FloatBuffer =
    MemoryUtil.memAllocFloat(positions.length)
  private val intBuffer: IntBuffer = MemoryUtil.memAllocInt(indices.length)

  val vertexCount: Int = indices.length

  floatBuffer.put(positions).flip()

  val vaoId: Int = glGenVertexArrays()
  glBindVertexArray(vaoId)

  val vboId: Int = glGenBuffers()
  glBindBuffer(GL_ARRAY_BUFFER, vboId)
  glBufferData(GL_ARRAY_BUFFER, floatBuffer, GL_STATIC_DRAW)
  glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0)

  val colVboId = glGenBuffers()
  floatBuffer.put(colors).flip()
  glBindBuffer(GL_ARRAY_BUFFER, colVboId)
  glBufferData(GL_ARRAY_BUFFER, floatBuffer, GL_STATIC_DRAW)
  glVertexAttribPointer(1, 3, GL_FLOAT, false, 0, 0)

  val idxVboId = glGenBuffers()
  intBuffer.put(indices).flip()
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, idxVboId)
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, intBuffer, GL_STATIC_DRAW)

  // unbind
  glBindBuffer(GL_ARRAY_BUFFER, 0)
  glBindVertexArray(0)
  MemoryUtil.memFree(floatBuffer)
  MemoryUtil.memFree(intBuffer)

  def bind() {
    glBindVertexArray(vaoId)
    glEnableVertexAttribArray(0)
    glEnableVertexAttribArray(1)
  }

  def unbind() {
    glDisableVertexAttribArray(0)
    glDisableVertexAttribArray(1)
    glBindVertexArray(0)
  }

  def render() {
    // Bind to the VAO
    bind()

    // clear and draw
    glDrawElements(glPrimitiveType, vertexCount, GL_UNSIGNED_INT, 0)

    // Restore state
    unbind()
  }

  def cleanup() {
    glDisableVertexAttribArray(0)
    glDisableVertexAttribArray(1)

    // Delete the VBO
    glBindBuffer(GL_ARRAY_BUFFER, 0)
    glDeleteBuffers(vboId)
    glDeleteBuffers(idxVboId)
    glDeleteBuffers(colVboId)

    // Delete the VAO
    glBindVertexArray(0)
    glDeleteVertexArrays(vaoId)
  }

}
