package co.mefu.msl.slwjgl.graph

import org.joml.Vector3f

class Camera(
    var position: Vector3f = new Vector3f(0, 0, 0),
    var rotation: Vector3f = new Vector3f(0, 0, 0)
) {

  def move(offset: Vector3f) = {
    if (offset.z != 0) {
      position.x -= Math.sin(Math.toRadians(rotation.y)).toFloat * offset.z
      position.z += Math.cos(Math.toRadians(rotation.y)).toFloat * offset.z
    }

    if (offset.x != 0) {
      position.x -= Math
        .sin(Math.toRadians(rotation.y - 90))
        .toFloat * offset.x
      position.z += Math
        .cos(Math.toRadians(rotation.y - 90))
        .toFloat * offset.x
    }

    if (offset.y != 0) {
      position.add(0, offset.y, 0)
    }
  }

  def rotate(offset: Vector3f) = rotation.add(offset)

}
