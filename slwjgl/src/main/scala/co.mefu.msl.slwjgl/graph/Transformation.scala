package co.mefu.msl.slwjgl.graph

import org.joml.Vector3f
import org.joml.Matrix4f

object Transformation {

  def projection(FOV: Float = Math.toRadians(60f).toFloat,
                 near: Float = 0.01f,
                 far: Float = 1000f,
                 ratio: Float = 640f / 480f): Matrix4f =
    new Matrix4f().perspective(FOV, ratio, near, far)

  def model(offset: Vector3f, rotation: Vector3f, scale: Float): Matrix4f = {
    new Matrix4f()
      .translate(offset)
      .rotateX(Math.toRadians(-rotation.x).toFloat)
      .rotateY(Math.toRadians(-rotation.y).toFloat)
      .rotateZ(Math.toRadians(-rotation.z).toFloat)
      .scale(scale)
  }

  def view(position: Vector3f, rotation: Vector3f) = {
    new Matrix4f()
      .rotate(Math.toRadians(rotation.x).toFloat, new Vector3f(1, 0, 0))
      .rotate(Math.toRadians(rotation.y).toFloat, new Vector3f(0, 1, 0))
      .translate(-position.x, -position.y, -position.z)
  }

}
