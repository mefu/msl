// import Dependencies._

lazy val root = (project in file("."))
  .settings(Commons.settings: _*)
  .settings(
    name := "msl"
  )
  .aggregate(core, linalg, slwjgl, vis, modeling, cca, benchmark)

lazy val core = (project in file("core"))
  .settings(Commons.settings: _*)
  .settings(
    name := "msl-core",
    libraryDependencies ++= Dependencies.core
  )

lazy val linalg = (project in file("linalg"))
  .settings(Commons.settings: _*)
  .settings(
    name := "msl-linalg",
    libraryDependencies ++= Dependencies.linalg
  )
  .dependsOn(core % "test->test;compile->compile")

lazy val slwjgl = (project in file("slwjgl"))
  .settings(Commons.settings: _*)
  .settings(
    name := "msl-slwjgl",
    libraryDependencies ++= Dependencies.slwjgl
  )
  .dependsOn(core % "test->test;compile->compile")

lazy val vis = (project in file("vis"))
  .settings(Commons.settings: _*)
  .settings(
    name := "msl-vis",
    libraryDependencies ++= Dependencies.vis
  )
  .dependsOn(core % "test->test;compile->compile")
  .dependsOn(slwjgl % "test->test;compile->compile")
  .dependsOn(modeling % "test->test;compile->compile")
  .dependsOn(cca % "test->test;compile->compile")

lazy val modeling = (project in file("modeling"))
  .settings(Commons.settings: _*)
  .settings(
    name := "msl-modeling",
    libraryDependencies ++= Dependencies.modeling
  )
  .dependsOn(core % "test->test;compile->compile")

lazy val cca = (project in file("cca"))
  .settings(Commons.settings: _*)
  .settings(
    name := "msl-cca",
    libraryDependencies ++= Dependencies.cca
  )
  .dependsOn(core % "test->test;compile->compile")
  .dependsOn(modeling % "test->test;compile->compile")
  .dependsOn(linalg % "test->test;compile->compile")

lazy val benchmark = (project in file("benchmark"))
  .settings(Commons.settings: _*)
  .settings(
    name := "msl-benchmark",
    libraryDependencies ++= Dependencies.benchmark
  )
  .dependsOn(core % "test->test;compile->compile")
  .dependsOn(linalg % "test->test;compile->compile")
  .enablePlugins(JmhPlugin)
