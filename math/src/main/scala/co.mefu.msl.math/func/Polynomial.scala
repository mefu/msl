package co.mefu.msl.math.func

import spire.implicits._

class Polynomial[@sp(Int, Long, Float, Double) T](
  val coeffs: SPBlock[T]
) {
  def apply(x: Double): Double = {
    var res = 0d
    cfor(0)(_ < coeffs.size, _ + 1)(i => {
      res += coeffs(i) * Math.pow(x, i)
    })
    res
  }
}

object Polynomial {

  def apply(coeffs: Double...) = {
    
  }
}