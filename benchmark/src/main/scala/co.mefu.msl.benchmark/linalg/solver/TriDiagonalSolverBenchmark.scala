package co.mefu.msl.benchmark.core.math

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import spire.implicits._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.linalg.solver.TriDiagonalSolver

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class TriDiagonalSolverBenchmark {

  @Param(Array("50", "100", "200"))
  var n: Int = _

  var a: SPVec[Double] = _
  var b: SPVec[Double] = _
  var c: SPVec[Double] = _
  var d: SPVec[Double] = _
  var buf1: SPVec[Double] = _
  var buf2: SPVec[Double] = _
  var x: SPVec[Double] = _

  @Setup(Level.Trial)
  def setupTrial = {
    a = SPVec.from(Array.fill(n)(Random.next[Double](0, n)))
    b = SPVec.from(Array.fill(n)(Random.next[Double](0, n)))
    c = SPVec.from(Array.fill(n)(Random.next[Double](0, n)))
    d = SPVec.from(Array.fill(n)(Random.next[Double](0, n)))
  }

  @Setup(Level.Iteration)
  def setupIter = {
    buf1 = SPVec.from(Array.fill(n)(0d))
    buf2 = SPVec.from(Array.fill(n)(0d))
    x = SPVec.from(Array.fill(n)(0d))
  }

  @Benchmark
  def tdma = TriDiagonalSolver.tdma[Double](a, b, c, d)

  @Benchmark
  def tdmaI = {
    TriDiagonalSolver.tdma[Double](a, b, c, d, buf1, buf2, x)
    x
  }

}
