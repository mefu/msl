package co.mefu.msl.benchmark.linalg.diff

import org.openjdk.jmh.annotations._

import java.util.concurrent.TimeUnit

import co.mefu.msl.core.ds.SPMat3D

import co.mefu.msl.linalg.diff.Diffusion3DFFactor

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 2, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 5, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class Diffusion3DBenchmark {

  @Param(Array("20", "50", "100"))
  var n: Int = _

  @Param(Array("10", "50", "100"))
  var t: Int = _

  var u: SPMat3D[Double] = _
  var d3df: Diffusion3DFFactor = _

  @Setup(Level.Iteration)
  def setupIter = {
    val nhalf = n / 2
    u = SPMat3D.fill(n, n, n)(0)
    u(nhalf, nhalf, nhalf) = 10d
  }

  @Setup(Level.Trial)
  def setupTrial = {
    d3df = Diffusion3DFFactor(n, 0.0503)
  }

  @Benchmark
  def d3dFFADI = {
    d3df.diffuse(u, t)
  }
}