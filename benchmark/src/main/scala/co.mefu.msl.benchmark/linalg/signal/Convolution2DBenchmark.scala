package co.mefu.msl.benchmark.linalg.signal

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import spire.implicits._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPMat
import co.mefu.msl.linalg.signal.Convolution2D

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class Convolution2DBenchmark {

  @Param(Array("50", "100"))
  var n: Int = _

  @Param(Array("3", "5"))
  var m: Int = _

  var input: SPMat[Double] = _
  var kernel: SPMat[Double] = _
  var out: SPMat[Double] = _

  @Setup(Level.Trial)
  def setupTrial = {
    input = SPMat.from(n, n)(
      Array.fill(n)(Array.fill(n)(Random.next[Double](0, n * n))).flatten)
    kernel = SPMat.from(m, m)(
      Array.fill(m)(Array.fill(m)(Random.next[Double](0, m * m))).flatten)
  }

  @Setup(Level.Iteration)
  def setupIter = {
    out = SPMat.fill(n, n)(0d)
  }

  @Benchmark
  def conv2d = Convolution2D.conv2d(input, kernel)

  @Benchmark
  def conv2dI = {
    Convolution2D.conv2d(input, kernel, out)
    out
  }

}
