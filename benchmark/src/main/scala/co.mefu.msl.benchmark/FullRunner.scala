// package co.mefu.msl.benchmark

// import java.io.{File, FileOutputStream}

// import scala.collection.JavaConverters._
// import org.openjdk.jmh.results.RunResult
// import org.openjdk.jmh.runner.Runner
// import org.openjdk.jmh.runner.options.CommandLineOptions

// object FullRunner {

//   def main(args: Array[String]): Unit = {

//     val opts = new CommandLineOptions(args: _*) // parse command line arguments, and then bend them to your will! ;-)

//     val runner = new Runner(opts) // full access to all JMH features, you can also provide a custom output Format here

//     val results = runner.run().asScala // actually run the benchmarks

//     val f = new FileOutputStream(new File("custom.out"))
//     results.foreach { result: RunResult =>
//       // usually you'd use these results to report into some external aggregation tool for example
//       val res = result.getAggregatedResult.getPrimaryResult
//       val stats = res.getStatistics
//       f.write(
//         s"""
//         |custom reporting result:
//         |From result
//         |getLabel: ${res.getLabel()}
//         |getRole: ${res.getRole()}
//         |getSampleCount: ${res.getSampleCount}
//         |getScore: ${res.getScore}
//         |getScoreError: ${res.getScoreError}
//         |getScoreUnit: ${res.getScoreUnit}
//         |From statistics
//         |getMax: ${stats.getMax}
//         |getMean: ${stats.getMean}
//         |getMin: ${stats.getMin}
//         |getN: ${stats.getN}
//         |getStandardDeviation: ${stats.getStandardDeviation}
//         |getSum: ${stats.getSum}
//         |getVariance: ${stats.getVariance}
//         """.getBytes("UTF-8")
//       )
//     }
//     f.close()
//   }

// }
