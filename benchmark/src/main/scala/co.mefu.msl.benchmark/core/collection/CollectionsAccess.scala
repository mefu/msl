package co.mefu.msl.benchmark.core.array.collection

import java.util.concurrent.TimeUnit

import scala.collection.mutable.Buffer
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.MutableList

import org.openjdk.jmh.annotations._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPVec

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class CollectionsAccess {

  @Param(Array("1000000"))
  var n: Int = _

  // immutable
  var vec: Vector[Double] = _

  // mutable
  var arr: Array[Double] = _
  var buf: Buffer[Double] = _
  var arrbuf: ArrayBuffer[Double] = _

  // custom
  var spvec: SPVec[Double] = _

  var randXs: Array[Int] = _

  @Setup(Level.Trial)
  def setupTrial = {
    vec = Vector.fill(n)(Random.next[Int](0, n))

    arr = vec.to[Array]
    buf = vec.to[Buffer]
    arrbuf = vec.to[ArrayBuffer]

    spvec = SPVec.from(arr.clone)

    randXs = Array.fill(n)(Random.next[Int](0, n))
  }

  @Benchmark
  def accessIVector = {
    var sum = 0d
    for (i <- 0 until n)
      sum += vec(i)
    sum
  }

  @Benchmark
  def accessRandomVector = {
    var sum = 0d
    for (i <- 0 until n)
      sum += vec(randXs(i))
    sum
  }

  @Benchmark
  def accessIArray = {
    var sum = 0d
    for (i <- 0 until n)
      sum += arr(i)
    sum
  }

  @Benchmark
  def accessRandomArray = {
    var sum = 0d
    for (i <- 0 until n)
      sum += arr(randXs(i))
    sum
  }

  @Benchmark
  def accessIBuffer = {
    var sum = 0d
    for (i <- 0 until n)
      sum += buf(i)
    sum
  }

  @Benchmark
  def accessRandomBuffer = {
    var sum = 0d
    for (i <- 0 until n)
      sum += buf(randXs(i))
    sum
  }

  @Benchmark
  def accessIArrayBuffer = {
    var sum = 0d
    for (i <- 0 until n)
      sum += arrbuf(i)
    sum
  }

  @Benchmark
  def accessRandomArrayBuffer = {
    var sum = 0d
    for (i <- 0 until n)
      sum += arrbuf(randXs(i))
    sum
  }

  @Benchmark
  def accessISPVec = {
    var sum = 0d
    for (i <- 0 until n)
      sum += spvec(i)
    sum
  }

  @Benchmark
  def accessRandomSPVec = {
    var sum = 0d
    for (i <- 0 until n)
      sum += spvec(randXs(i))
    sum
  }

}
