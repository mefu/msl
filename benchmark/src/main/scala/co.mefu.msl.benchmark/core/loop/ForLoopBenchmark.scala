package co.mefu.msl.benchmark.core.array

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._
import org.openjdk.jmh.infra.Blackhole

import spire.implicits._

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class ForLoopBenchmark {

  @Param(Array("100"))
  var n: Int = _

  @Benchmark
  def loop1(bh: Blackhole) = {
    for (i <- 0 until n) {
      bh.consume(i)
    }
  }

  @Benchmark
  def loop2(bh: Blackhole) = {
    for (i <- 0 until n)
      for (j <- 0 until n) {
        bh.consume(i)
        bh.consume(j)
      }
  }

  @Benchmark
  def loop3(bh: Blackhole) = {
    for (i <- 0 until n)
      for (j <- 0 until n)
        for (k <- 0 until n) {
          bh.consume(i)
          bh.consume(j)
          bh.consume(k)
        }
  }
}
