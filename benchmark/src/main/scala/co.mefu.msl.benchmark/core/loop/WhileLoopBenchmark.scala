package co.mefu.msl.benchmark.core.array

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._
import org.openjdk.jmh.infra.Blackhole

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class WhileLoopBenchmark {

  @Param(Array("100"))
  var n: Int = _

  @Benchmark
  def loop1(bh: Blackhole) = {
    var i = 0
    while (i < n) {
      bh.consume(i)
      i += 1
    }
  }

  @Benchmark
  def loop2(bh: Blackhole) = {
    var i = 0
    var j = 0
    while (i < n) {
      j = 0
      while (j < n) {
        bh.consume(i)
        bh.consume(j)
        j += 1
      }
      i += 1
    }
  }

  @Benchmark
  def loop3(bh: Blackhole) = {
    var i = 0
    var j = 0
    var k = 0
    while (i < n) {
      j = 0
      while (j < n) {
        k = 0
        while (k < n) {
          bh.consume(i)
          bh.consume(j)
          bh.consume(k)
          k += 1
        }
        j += 1
      }
      i += 1
    }
  }

}
