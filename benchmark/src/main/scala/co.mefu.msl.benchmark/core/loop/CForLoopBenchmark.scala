package co.mefu.msl.benchmark.core.array

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._
import org.openjdk.jmh.infra.Blackhole

import spire.implicits._

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class CForLoopBenchmark {

  @Param(Array("100"))
  var n: Int = _

  @Benchmark
  def loop1(bh: Blackhole) = {
    cfor(0)(_ < n, _ + 1)(i => {
      bh.consume(i)
    })
  }

  @Benchmark
  def loop2(bh: Blackhole) = {
    cfor(0)(_ < n, _ + 1)(i => {
      cfor(0)(_ < n, _ + 1)(j => {
        bh.consume(i)
        bh.consume(j)
      })
    })
  }

  @Benchmark
  def loop3(bh: Blackhole) = {
    cfor(0)(_ < n, _ + 1)(i => {
      cfor(0)(_ < n, _ + 1)(j => {
        cfor(0)(_ < n, _ + 1)(k => {
          bh.consume(i)
          bh.consume(j)
          bh.consume(k)
        })
      })
    })
  }

}