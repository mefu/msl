package co.mefu.msl.benchmark.core.array.a1d

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPVec

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class Array1DUpdate {

  @Param(Array("1000000"))
  var n: Int = _

  var std: Array[Double] = _
  var custom: SPVec[Double] = _

  var randVals: Array[Double] = _
  var randXs: Array[Int] = _

  @Setup(Level.Trial)
  def setupTrial = {
    randVals = Array.fill(n)(Random.next[Double](0, n))
    randXs = Array.fill(n)(Random.next[Int](0, n))
  }

  @Setup(Level.Iteration)
  def setupIter = {
    std = Array.fill(n)(Random.next[Double](0, n))
    custom = SPVec.from(std.clone)
  }

  @Benchmark
  def updateIStd = {
    for (i <- 0 until n)
      std(i) = randVals(i)
    std
  }

  @Benchmark
  def updateRandomStd = {
    for (i <- 0 until n)
      std(randXs(i)) = randVals(i)
    std
  }

  @Benchmark
  def updateICustom = {
    for (i <- 0 until n)
      custom(i) = randVals(i)
    custom
  }

  @Benchmark
  def updateRandomCustom = {
    for (i <- 0 until n)
      custom(randXs(i)) = randVals(i)
    custom
  }

}
