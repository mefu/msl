package co.mefu.msl.benchmark.core.array.a1d

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import spire.implicits._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ops.SPVecOps._

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class Array1DMethods {

  @Param(Array("1000000"))
  var n: Int = _

  var std: Array[Double] = _
  var custom: SPVec[Double] = _

  @Setup(Level.Trial)
  def setupTrial = {
    std = Array.fill(n)(Random.next[Double](0, n))
    custom = SPVec.from(std.clone)
  }

  @Benchmark
  def foreachStd = {
    var sum = 0d
    std.foreach(sum += _)
    sum
  }

  @Benchmark
  def transformStd = {
    std.transform(_ * 2)
    std
  }

  @Benchmark
  def sumStd = {
    std.sum
  }

  @Benchmark
  def varianceStd = {
    val mean = std.sum / std.size
    std.map(el => Math.pow(el - mean, 2)).sum / std.size
  }

  @Benchmark
  def foreachCustom = {
    var sum = 0d
    custom.foreach(sum += _)
    sum
  }

  @Benchmark
  def transformCustom = {
    custom.transform(_ * 2)
    custom
  }

  @Benchmark
  def sumCustom = {
    custom.sum
  }

  @Benchmark
  def varianceCustom = {
    custom.variance
  }

}
