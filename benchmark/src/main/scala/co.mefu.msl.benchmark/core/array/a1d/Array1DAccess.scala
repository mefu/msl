package co.mefu.msl.benchmark.core.array.a1d

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPVec

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class Array1DAccess {

  @Param(Array("1000000"))
  var n: Int = _

  var std: Array[Double] = _
  var custom: SPVec[Double] = _

  var randXs: Array[Int] = _

  @Setup(Level.Trial)
  def setupTrial = {
    randXs = Array.fill(n)(Random.next[Int](0, n))
    std = Array.fill(n)(Random.next[Double](0, n))
    custom = SPVec.from(std.clone)
  }

  @Benchmark
  def accessIStd = {
    var sum = 0d
    for (i <- 0 until n)
      sum += std(i)
    sum
  }

  @Benchmark
  def accessRandomStd = {
    var sum = 0d
    for (i <- 0 until n)
      sum += std(randXs(i))
    sum
  }

  @Benchmark
  def accessICustom = {
    var sum = 0d
    for (i <- 0 until n)
      sum += custom(i)
    sum
  }

  @Benchmark
  def accessRandomCustom = {
    var sum = 0d
    for (i <- 0 until n)
      sum += custom(randXs(i))
    sum
  }

}
