package co.mefu.msl.benchmark.core.array.a3d

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPMat3D

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class Array3DAccess {

  @Param(Array("100"))
  var n: Int = _

  var std: Array[Array[Array[Double]]] = _
  var custom: SPMat3D[Double] = _

  var randXs: Array[Int] = _
  var randYs: Array[Int] = _
  var randZs: Array[Int] = _

  @Setup(Level.Trial)
  def setupTrial = {
    randXs = Array.fill(n * n * n)(Random.next[Int](0, n))
    randYs = Array.fill(n * n * n)(Random.next[Int](0, n))
    randZs = Array.fill(n * n * n)(Random.next[Int](0, n))
    std = Array.fill(n)(
      Array.fill(n)(Array.fill(n)(Random.next[Double](0, n * n * n))))
    custom = SPMat3D.from(n, n, n)(std.clone.flatten.flatten)
  }

  @Benchmark
  def accessIJKStd = {
    var sum = 0d
    for (i <- 0 until n)
      for (j <- 0 until n)
        for (k <- 0 until n)
          sum += std(i)(j)(k)
    sum
  }

  @Benchmark
  def accessIKJStd = {
    var sum = 0d
    for (i <- 0 until n)
      for (k <- 0 until n)
        for (j <- 0 until n)
          sum += std(i)(j)(k)
    sum
  }

  @Benchmark
  def accessJIKStd = {
    var sum = 0d
    for (j <- 0 until n)
      for (i <- 0 until n)
        for (k <- 0 until n)
          sum += std(i)(j)(k)
    sum
  }

  @Benchmark
  def accessJKIStd = {
    var sum = 0d
    for (j <- 0 until n)
      for (k <- 0 until n)
        for (i <- 0 until n)
          sum += std(i)(j)(k)
    sum
  }

  @Benchmark
  def accessKIJStd = {
    var sum = 0d
    for (k <- 0 until n)
      for (i <- 0 until n)
        for (j <- 0 until n)
          sum += std(i)(j)(k)
    sum
  }

  @Benchmark
  def accessKJIStd = {
    var sum = 0d
    for (k <- 0 until n)
      for (j <- 0 until n)
        for (i <- 0 until n)
          sum += std(i)(j)(k)
    sum
  }

  @Benchmark
  def accessRandomStd = {
    var sum = 0d
    for (i <- 0 until n * n * n)
      sum += std(randXs(i))(randYs(i))(randZs(i))
    sum
  }

  @Benchmark
  def accessIJKCustom = {
    var sum = 0d
    for (i <- 0 until n)
      for (j <- 0 until n)
        for (k <- 0 until n)
          sum += custom(i, j, k)
    sum
  }

  @Benchmark
  def accessIKJCustom = {
    var sum = 0d
    for (i <- 0 until n)
      for (k <- 0 until n)
        for (j <- 0 until n)
          sum += custom(i, j, k)
    sum
  }

  @Benchmark
  def accessJIKCustom = {
    var sum = 0d
    for (j <- 0 until n)
      for (i <- 0 until n)
        for (k <- 0 until n)
          sum += custom(i, j, k)
    sum
  }

  @Benchmark
  def accessJKICustom = {
    var sum = 0d
    for (j <- 0 until n)
      for (k <- 0 until n)
        for (i <- 0 until n)
          sum += custom(i, j, k)
    sum
  }

  @Benchmark
  def accessKIJCustom = {
    var sum = 0d
    for (k <- 0 until n)
      for (i <- 0 until n)
        for (j <- 0 until n)
          sum += custom(i, j, k)
    sum
  }

  @Benchmark
  def accessKJICustom = {
    var sum = 0d
    for (k <- 0 until n)
      for (j <- 0 until n)
        for (i <- 0 until n)
          sum += custom(i, j, k)
    sum
  }

  @Benchmark
  def accessRandomCustom = {
    var sum = 0d
    for (i <- 0 until n * n * n)
      sum += custom(randXs(i), randYs(i), randZs(i))
    sum
  }

  @Benchmark
  def accessIJKCustomSub = {
    var sum = 0d
    for (i <- 0 until n) {
      val subI = custom.subI(i)
      for (j <- 0 until n) {
        val subJ = subI.subI(j)
        for (k <- 0 until n)
          sum += subJ(k)
      }
    }
    sum
  }

  @Benchmark
  def accessIKJCustomSub = {
    var sum = 0d
    for (i <- 0 until n) {
      val subI = custom.subI(i)
      for (k <- 0 until n) {
        val subK = subI.subJ(k)
        for (j <- 0 until n)
          sum += subK(j)
      }
    }
    sum
  }

  @Benchmark
  def accessJIKCustomSub = {
    var sum = 0d
    for (j <- 0 until n) {
      val subJ = custom.subJ(j)
      for (i <- 0 until n) {
        val subI = subJ.subI(i)
        for (k <- 0 until n)
          sum += subI(k)
      }
    }
    sum
  }

  @Benchmark
  def accessJKICustomSub = {
    var sum = 0d
    for (j <- 0 until n) {
      val subJ = custom.subJ(j)
      for (k <- 0 until n) {
        val subK = subJ.subJ(k)
        for (i <- 0 until n)
          sum += subK(i)
      }
    }
    sum
  }

  @Benchmark
  def accessKIJCustomSub = {
    var sum = 0d
    for (k <- 0 until n) {
      val subK = custom.subK(k)
      for (i <- 0 until n) {
        val subI = subK.subI(i)
        for (j <- 0 until n)
          sum += subI(j)
      }
    }
    sum
  }

  @Benchmark
  def accessKJICustomSub = {
    var sum = 0d
    for (k <- 0 until n) {
      val subK = custom.subK(k)
      for (j <- 0 until n) {
        val subJ = subK.subJ(j)
        for (i <- 0 until n)
          sum += subJ(i)
      }
    }
    sum
  }

}
