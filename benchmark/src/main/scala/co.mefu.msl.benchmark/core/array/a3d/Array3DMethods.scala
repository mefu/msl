package co.mefu.msl.benchmark.core.array.a3d

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.SPFunc1
import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.ops.SPMat3DOps._

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class Array3DMethods {

  @Param(Array("100"))
  var n: Int = _

  var std: Array[Array[Array[Double]]] = _
  var custom: SPMat3D[Double] = _

  @Setup(Level.Iteration)
  def setupIter = {
    std = Array.fill(n)(
      Array.fill(n)(Array.fill(n)(Random.next[Double](0, n * n * n))))
    custom = SPMat3D.from(n, n, n)(std.clone.flatten.flatten)
  }

  @Benchmark
  def foreachStd = {
    var sum = 0d
    std.foreach(std2d => std2d.foreach(std1d => std1d.foreach(sum += _)))
    sum
  }

  @Benchmark
  def transformStd = {
    std.foreach(std2d => std2d.foreach(std1d => std1d.transform(_ * 2)))
    std
  }

  @Benchmark
  def foreachCustom = {
    var sum = 0d
    custom.foreach(sum += _)
    sum
  }

  @Benchmark
  def transformCustom = {
    custom.transform(_ * 2)
    custom
  }

}
