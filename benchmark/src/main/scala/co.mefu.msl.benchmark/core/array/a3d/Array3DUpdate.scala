package co.mefu.msl.benchmark.core.array.a3d

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPMat3D

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class Array3DUpdate {

  @Param(Array("100"))
  var n: Int = _

  var std: Array[Array[Array[Double]]] = _
  var custom: SPMat3D[Double] = _

  var randVals: Array[Array[Array[Double]]] = _
  var randXs: Array[Int] = _
  var randYs: Array[Int] = _
  var randZs: Array[Int] = _

  @Setup(Level.Trial)
  def setupTrial = {
    randVals = Array.fill(n)(
      Array.fill(n)(Array.fill(n)(Random.next[Double](0, n * n * n))))
    randXs = Array.fill(n * n)(Random.next[Int](0, n))
    randYs = Array.fill(n * n)(Random.next[Int](0, n))
    randZs = Array.fill(n * n * n)(Random.next[Int](0, n))
  }

  @Setup(Level.Iteration)
  def setupIter = {
    std = Array.fill(n)(
      Array.fill(n)(Array.fill(n)(Random.next[Double](0, n * n * n))))
    custom = SPMat3D.from(n, n, n)(std.clone.flatten.flatten)
  }

  @Benchmark
  def updateIJKStd = {
    for (i <- 0 until n)
      for (j <- 0 until n)
        for (k <- 0 until n)
          std(i)(j)(k) = randVals(i)(j)(k)
    std
  }

  @Benchmark
  def updateIKJStd = {
    for (i <- 0 until n)
      for (k <- 0 until n)
        for (j <- 0 until n)
          std(i)(j)(k) = randVals(i)(j)(k)
    std
  }

  @Benchmark
  def updateJIKStd = {
    for (j <- 0 until n)
      for (i <- 0 until n)
        for (k <- 0 until n)
          std(i)(j)(k) = randVals(i)(j)(k)
    std
  }

  @Benchmark
  def updateJKIStd = {
    for (j <- 0 until n)
      for (k <- 0 until n)
        for (i <- 0 until n)
          std(i)(j)(k) = randVals(i)(j)(k)
    std
  }

  @Benchmark
  def updateKIJStd = {
    for (k <- 0 until n)
      for (i <- 0 until n)
        for (j <- 0 until n)
          std(i)(j)(k) = randVals(i)(j)(k)
    std
  }

  @Benchmark
  def updateKJIStd = {
    for (k <- 0 until n)
      for (j <- 0 until n)
        for (i <- 0 until n)
          std(i)(j)(k) = randVals(i)(j)(k)
    std
  }

  @Benchmark
  def updateRandomStd = {
    for (i <- 0 until n * n * n)
      std(randXs(i))(randYs(i))(randZs(i)) =
        randVals(randXs(i))(randYs(i))(randZs(i))
    std
  }

  @Benchmark
  def updateIJKCustom = {
    for (i <- 0 until n)
      for (j <- 0 until n)
        for (k <- 0 until n)
          custom(i, j, k) = randVals(i)(j)(k)
    custom
  }

  @Benchmark
  def updateIKJCustom = {
    for (i <- 0 until n)
      for (k <- 0 until n)
        for (j <- 0 until n)
          custom(i, j, k) = randVals(i)(j)(k)
    custom
  }

  @Benchmark
  def updateJIKCustom = {
    for (j <- 0 until n)
      for (i <- 0 until n)
        for (k <- 0 until n)
          custom(i, j, k) = randVals(i)(j)(k)
    custom
  }

  @Benchmark
  def updateJKICustom = {
    for (j <- 0 until n)
      for (k <- 0 until n)
        for (i <- 0 until n)
          custom(i, j, k) = randVals(i)(j)(k)
    custom
  }

  @Benchmark
  def updateKIJCustom = {
    for (k <- 0 until n)
      for (i <- 0 until n)
        for (j <- 0 until n)
          custom(i, j, k) = randVals(i)(j)(k)
    custom
  }

  @Benchmark
  def updateKJICustom = {
    for (k <- 0 until n)
      for (j <- 0 until n)
        for (i <- 0 until n)
          custom(i, j, k) = randVals(i)(j)(k)
    custom
  }

  @Benchmark
  def updateRandomCustom = {
    for (i <- 0 until n * n * n)
      custom(randXs(i), randYs(i), randZs(i)) =
        randVals(randXs(i))(randYs(i))(randZs(i))
    custom
  }

}
