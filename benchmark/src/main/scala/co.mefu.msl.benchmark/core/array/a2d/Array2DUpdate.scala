package co.mefu.msl.benchmark.core.array.a2d

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPMat

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class Array2DUpdate {

  @Param(Array("1000"))
  var n: Int = _

  var std: Array[Array[Double]] = _
  var custom: SPMat[Double] = _

  var randVals: Array[Array[Double]] = _
  var randXs: Array[Int] = _
  var randYs: Array[Int] = _

  @Setup(Level.Trial)
  def setupTrial = {
    randVals = Array.fill(n)(Array.fill(n)(Random.next[Double](0, n * n)))
    randXs = Array.fill(n * n)(Random.next[Int](0, n))
    randYs = Array.fill(n * n)(Random.next[Int](0, n))
  }

  @Setup(Level.Iteration)
  def setupIter = {
    std = Array.fill(n)(Array.fill(n)(Random.next[Double](0, n * n)))
    custom = SPMat.from(n, n)(std.clone.flatten)
  }

  @Benchmark
  def updateIJStd = {
    for (i <- 0 until n)
      for (j <- 0 until n)
        std(i)(j) = randVals(i)(j)
    std
  }

  @Benchmark
  def updateJIStd = {
    for (j <- 0 until n)
      for (i <- 0 until n)
        std(i)(j) = randVals(i)(j)
    std
  }

  @Benchmark
  def updateRandomStd = {
    for (i <- 0 until n * n) {
      std(randXs(i))(randYs(i)) = randVals(randXs(i))(randYs(i))
    }
    std
  }

  @Benchmark
  def updateIJCustom = {
    for (i <- 0 until n)
      for (j <- 0 until n)
        custom(i, j) = randVals(i)(j)
    custom
  }

  @Benchmark
  def updateJICustom = {
    for (j <- 0 until n)
      for (i <- 0 until n)
        custom(i, j) = randVals(i)(j)
    custom
  }

  @Benchmark
  def updateRandomCustom = {
    for (i <- 0 until n * n) {
      custom(randXs(i), randYs(i)) = randVals(randXs(i))(randYs(i))
    }
    custom
  }

  @Benchmark
  def updateIJCustomSub = {
    for (i <- 0 until n) {
      val subI = custom.subI(i)
      for (j <- 0 until n)
        subI(j) = randVals(i)(j)
    }
    custom
  }

  @Benchmark
  def updateJICustomSub = {
    for (j <- 0 until n) {
      val subJ = custom.subJ(j)
      for (i <- 0 until n)
        subJ(i) = randVals(i)(j)
    }
    custom
  }

}
