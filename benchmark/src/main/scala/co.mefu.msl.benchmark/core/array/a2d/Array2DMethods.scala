package co.mefu.msl.benchmark.core.array.a2d

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import spire.implicits._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.SPFunc1
import co.mefu.msl.core.ds.SPMat
import co.mefu.msl.core.ops.SPMatOps._

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class Array2DMethods {

  @Param(Array("1000"))
  var n: Int = _

  var std: Array[Array[Double]] = _
  var custom: SPMat[Double] = _

  @Setup(Level.Iteration)
  def setupIter = {
    std = Array.fill(n)(Array.fill(n)(Random.next[Double](0, n * n)))
    custom = SPMat.from(n, n)(std.clone.flatten)
  }

  @Benchmark
  def foreachStd = {
    var sum = 0d
    std.foreach(std1d => std1d.foreach(sum += _))
    sum
  }

  @Benchmark
  def transformStd = {
    std.foreach(std1d => std1d.transform(_ * 2))
    std
  }

  @Benchmark
  def foreachCustom = {
    var sum = 0d
    custom.foreach(sum += _)
    sum
  }

  @Benchmark
  def transformCustom = {
    custom.transform(_ * 2)
    custom
  }

}
