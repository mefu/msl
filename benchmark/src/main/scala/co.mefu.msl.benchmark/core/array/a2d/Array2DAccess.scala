package co.mefu.msl.benchmark.core.array.a2d

import java.util.concurrent.TimeUnit

import org.openjdk.jmh.annotations._

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPMat

@BenchmarkMode(Array(Mode.AverageTime))
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@Warmup(iterations = 10, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Measurement(iterations = 20, time = 1, timeUnit = TimeUnit.MILLISECONDS)
@Fork(1)
@State(Scope.Thread)
class Array2DAccess {

  @Param(Array("1000"))
  var n: Int = _

  var std: Array[Array[Double]] = _
  var custom: SPMat[Double] = _

  var randXs: Array[Int] = _
  var randYs: Array[Int] = _

  @Setup(Level.Trial)
  def setupTrial = {
    randXs = Array.fill(n * n)(Random.next[Int](0, n))
    randYs = Array.fill(n * n)(Random.next[Int](0, n))
    std = Array.fill(n)(Array.fill(n)(Random.next[Double](0, n * n)))
    custom = SPMat.from(n, n)(std.clone.flatten)
  }

  @Benchmark
  def accessIJStd = {
    var sum = 0d
    for (i <- 0 until n)
      for (j <- 0 until n)
        sum += std(i)(j)
    sum
  }

  @Benchmark
  def accessJIStd = {
    var sum = 0d
    for (j <- 0 until n)
      for (i <- 0 until n)
        sum += std(i)(j)
    sum
  }

  @Benchmark
  def accessRandomStd = {
    var sum = 0d
    for (i <- 0 until n * n)
      sum += std(randXs(i))(randYs(i))
    sum
  }

  @Benchmark
  def accessIJCustom = {
    var sum = 0d
    for (i <- 0 until n)
      for (j <- 0 until n)
        sum += custom(i, j)
    sum
  }

  @Benchmark
  def accessJICustom = {
    var sum = 0d
    for (j <- 0 until n)
      for (i <- 0 until n)
        sum += custom(i, j)
    sum
  }

  @Benchmark
  def accessRandomCustom = {
    var sum = 0d
    for (i <- 0 until n * n)
      sum += custom(randXs(i), randYs(i))
    sum
  }

  @Benchmark
  def accessIJCustomSub = {
    var sum = 0d
    for (i <- 0 until n) {
      val subI = custom.subI(i)
      for (j <- 0 until n)
        sum += subI(j)
    }
    sum
  }

  @Benchmark
  def accessJICustomSub = {
    var sum = 0d
    for (j <- 0 until n) {
      val subJ = custom.subJ(j)
      for (i <- 0 until n)
        sum += subJ(i)
    }
    sum
  }

  @Benchmark
  def accessRandomCustomSub = {
    var sum = 0d
    for (i <- 0 until n) {
      val subI = custom.subI(randXs(i))
      for (j <- 0 until n)
        sum += subI(randYs(j))
    }
    sum
  }

}
