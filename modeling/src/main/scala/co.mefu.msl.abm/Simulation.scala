package co.mefu.msl.modeling

import scala.collection.mutable.Buffer

import com.typesafe.scalalogging.LazyLogging

trait Transformation[T] extends (T => Unit) with LazyLogging {

  // runtime and run count
  var rt: Long = 0
  var rc: Int = 0
  private var before: Long = 0l

  override def apply(t: T): Unit = {
    if (logger.underlying.isDebugEnabled) before = System.nanoTime
    transform(t)
    if (logger.underlying.isDebugEnabled) {
      rc += 1
      val took = (System.nanoTime - before)
      rt += took
    }
  }

  def report: String =
    f"""
    |Transformation: ${this}%s
    |Run count = ${rc}%d
    |Total: ${rt / 1e6d}%.3f ms = ${rt / 1e9d}%.3f s.
    |Average: ${rt / rc}%.3f ns = ${rt / rc / 1e6d}%.3f ms.
    """.stripMargin

  def transform(t: T): Unit
}

object Transformation {

  def apply[T](name: String, f: T => Unit): Transformation[T] =
    new Transformation[T] {
      def transform(t: T): Unit = f(t)
      override val toString: String = name
    }

  def condition[T](condition: T => Boolean,
                   tt: Transformation[T]): Transformation[T] =
    Transformation(
      "[ " + tt + " with condition" + " ]",
      t => if (condition(t)) tt(t)
    )

  def combine[T, U](tr1: Transformation[T],
                    tr2: Transformation[U]): Transformation[(T, U)] =
    new Transformation[(T, U)] {
      def transform(tu: (T, U)): Unit = {
        tr1(tu._1)
        tr2(tu._2)
      }
      override val toString: String = tr1 + " |+| " + tr2
      override def report: String =
        super.report +
          f"""|Overhead: ${(rt - (tr1.rt + tr2.rt)) / 1e6d}%.3f ms = ${(rt - (tr1.rt + tr2.rt)) / 1e9d}%.3f s.
        """.stripMargin + tr1.report + tr2.report
    }

  def combineTuple[T, U, V](
      tr1: Transformation[(T, U)],
      tr2: Transformation[(T, V)]): Transformation[(T, (U, V))] =
    new Transformation[(T, (U, V))] {
      def transform(tuv: (T, (U, V))): Unit = {
        tr1((tuv._1, tuv._2._1))
        tr2((tuv._1, tuv._2._2))
      }
      override val toString: String = tr1 + " |++| " + tr2
      override def report: String =
        super.report +
          f"""|Overhead: ${(rt - (tr1.rt + tr2.rt)) / 1e6d}%.3f ms = ${(rt - (tr1.rt + tr2.rt)) / 1e9d}%.3f s.
        """.stripMargin + tr1.report + tr2.report
    }

  def dummy[T]: Transformation[T] =
    new Transformation[T] {
      override def apply(t: T): Unit = Unit
      def transform(t: T): Unit = Unit
      override val toString: String = "DummyTransformation"
      override val report: String = ""
    }

  implicit def combineWithDummy1[T, U](
      tr: Transformation[T]): Transformation[(T, U)] =
    combine[T, U](tr, dummy[U])

  implicit def combineWithDummy2[T, U](
      tr: Transformation[T]): Transformation[(U, T)] =
    combine[U, T](dummy[U], tr)

  implicit def combineTupleWithDummy1[T, U, V](
      tr: Transformation[(T, U)]): Transformation[(T, (U, V))] =
    combineTuple[T, U, V](tr, dummy[(T, V)])

  implicit def combineTupleWithDummy2[T, U, V](
      tr: Transformation[(T, U)]): Transformation[(T, (V, U))] =
    combineTuple[T, V, U](dummy[(T, V)], tr)

  implicit def reverse[T, U](
      tr: Transformation[(T, U)]): Transformation[(U, T)] =
    new Transformation[(U, T)] {
      def transform(ut: (U, T)): Unit = tr((ut._2, ut._1))
      override val toString: String = "-" + tr
      override def report: String =
        super.report +
          f"""|Overhead: ${(rt - tr.rt) / 1e6d}%.3f ms = ${(rt - tr.rt) / 1e9d}%.3f s.
        """.stripMargin + tr.report
    }
}

class ChainedTransformation[T](
    val tr1: Transformation[T],
    val tr2: Transformation[T]
) extends Transformation[T] {

  def transform(t: T): Unit = { tr1(t); tr2(t) }

  override def report: String =
    super.report +
      f"""|Overhead: ${(rt - (tr1.rt + tr2.rt)) / 1e6d}%.3f ms = ${(rt - (tr1.rt + tr2.rt)) / 1e9d}%.3f s.
    """.stripMargin + tr1.report + tr2.report

  override val toString: String = "[ " + tr1 + " => " + tr2 + " ]"
}

object ChainedTransformation {
  def apply[T](tr1: Transformation[T],
               tr2: Transformation[T],
               trs: Transformation[T]*): ChainedTransformation[T] = {
    trs match {
      case Seq() => new ChainedTransformation[T](tr1, tr2)
      case Seq(tr, tail @ _ *) =>
        ChainedTransformation[T](new ChainedTransformation[T](tr1, tr2),
                                 tr,
                                 tail: _*)
    }
  }
}

// T can sub in place of U
trait Substitution[T, U] extends LazyLogging {

  def apply(u: U): Seq[T]
}

object Substitution {

  def apply[T, U](name: String, subber: U => Seq[T]) = new Substitution[T, U] {
    def apply(u: U): Seq[T] = subber(u)
    override val toString: String = name
  }

  // if T can sub V and
  // if U can sub V
  // then we can implicitly define (T, U) subs U
  implicit def combine[T, U, V](
      implicit subTV: Substitution[T, V],
      subUV: Substitution[U, V]): Substitution[(T, U), V] =
    Substitution[(T, U), V](
      "[ " + subTV + " + " + subUV + " ]",
      v => {
        val ts = subTV(v)
        val us = subUV(v)
        val buffer = Buffer.empty[(T, U)]

        var i = 0
        var j = 0
        while (i < ts.length) {
          while (j < us.length) {
            val el = (ts(i), us(j))
            buffer.append(el)
            j += 1
          }
          i += 1
          j = 0
        }

        buffer
      }
    )
}

class SubbedTransformation[T, U](val original: Transformation[T],
                                 val sub: Substitution[T, U])
    extends Transformation[U] {

  def transform(u: U): Unit = {
    val ts = sub(u)
    var i = 0
    while (i < ts.length) {
      original(ts(i))
      i += 1
    }
  }

  override val toString
    : String = "{ " + original + " subbed by ( " + sub + " ) }"
  override def report: String =
    super.report +
      f"""|Overhead: ${(rt - original.rt) / 1e6d}%.3f ms = ${(rt - original.rt) / 1e9d}%.3f s.
     """.stripMargin +
      original.report

}

object SubbedTransformation {

  implicit def sub[T, U](tt: Transformation[T])(
      implicit subst: Substitution[T, U]): Transformation[U] =
    new SubbedTransformation[T, U](tt, subst)

}

trait Simulation[T] extends Transformation[T] {

  def data: T

  def run: Unit = this(data)

  def runWhile(condition: T => Boolean): Unit = {

    while (condition(data)) this(data)

    logger.debug(this.report)
  }

  override def report =
    f"""
    |Simulation: ${this}%s
    |Run count = ${rc}%d
    |Total: ${rt / 1e6d}%.3f ms = ${rt / 1e9d}%.3f s.
    |Average: ${rt / rc}%.3f ns = ${rt / rc / 1e6d}%.3f ms.
    """.stripMargin
}

object Simulation {

  def apply[T](t: T, tr: Transformation[T]): Simulation[T] =
    new Simulation[T] {
      val data: T = t
      override def transform(t: T): Unit = tr(t)
      override val toString: String = tr.toString
    }
}
