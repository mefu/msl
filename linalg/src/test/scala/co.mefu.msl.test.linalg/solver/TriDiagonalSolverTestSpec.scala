package co.mefu.msl.core.test.math

import org.scalatest._
import Matchers._

import spire.implicits._

import co.mefu.msl.test.BaseSpec

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.linalg.solver.TriDiagonalSolver._

class TriDiagonalSolverTestSpec extends BaseSpec {

  val eps = 1e-10

  "TDMA" should "compute result correctly" in {
    val n = 100
    val a: SPVec[Double] =
      SPVec.from(Array.fill(n)(Random.next[Double](0, n / 4)))
    val b: SPVec[Double] =
      SPVec.from(Array.fill(n)(Random.next[Double](n / 2, n)))
    val c: SPVec[Double] =
      SPVec.from(Array.fill(n)(Random.next[Double](0, n / 4)))
    val d: SPVec[Double] =
      SPVec.from(Array.fill(n)(Random.next[Double](0, n)))

    for (i <- 0 until n)
      Math.abs(b(i)) should be > Math.abs(a(i)) + Math.abs(c(i))

    val x = tdma(a, b, c, d)

    d(0) should be((b(0) * x(0) + c(0) * x(1)) +- eps)
    for (i <- 1 until n - 1) {
      d(i) should be((a(i) * x(i - 1) + b(i) * x(i) + c(i) * x(i + 1)) +- eps)
    }
    d(n - 1) should be((a(n - 1) * x(n - 2) + b(n - 1) * x(n - 1)) +- eps)
  }

  "TDMA" should "compute result correctly with explicit buffers" in {
    val n = 100
    val a: SPVec[Double] =
      SPVec.from(Array.fill(n)(Random.next[Double](0, n / 4)))
    val b: SPVec[Double] =
      SPVec.from(Array.fill(n)(Random.next[Double](n / 2, n)))
    val c: SPVec[Double] =
      SPVec.from(Array.fill(n)(Random.next[Double](0, n / 4)))
    val d: SPVec[Double] =
      SPVec.from(Array.fill(n)(Random.next[Double](0, n)))

    val buf1: SPVec[Double] = SPVec.fill(n)(0d)
    val buf2: SPVec[Double] = SPVec.fill(n)(0d)
    val x: SPVec[Double] = SPVec.fill(n)(0d)

    for (i <- 0 until n)
      Math.abs(b(i)) should be > Math.abs(a(i)) + Math.abs(c(i))

    tdma(a, b, c, d, buf1, buf2, x)

    d(0) should be((b(0) * x(0) + c(0) * x(1)) +- eps)
    for (i <- 1 until n - 1) {
      d(i) should be((a(i) * x(i - 1) + b(i) * x(i) + c(i) * x(i + 1)) +- eps)
    }
    d(n - 1) should be((a(n - 1) * x(n - 2) + b(n - 1) * x(n - 1)) +- eps)
  }
}