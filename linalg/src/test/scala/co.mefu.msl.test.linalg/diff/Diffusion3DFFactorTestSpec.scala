package co.mefu.msl.test.linalg.diff

import org.scalatest._
import Matchers._

import spire.implicits._

import co.mefu.msl.test.BaseSpec

import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.ops.SPMat3DOps._

import co.mefu.msl.linalg.diff.Diffusion3DFFactor

class Diffusion3DFFactorTestSpec extends BaseSpec {

  val eps = 1e-8
  val n = 20
  val amount = 100d
  val dc = 0.05
  val t = 20000

  "Diffusion" should "disperse density correctly" in {
    val nhalf = n / 2
    val arr3d = SPMat3D.fill(n, n, n)(0d)
    arr3d(nhalf, nhalf, nhalf) = amount
    val diff = Diffusion3DFFactor(n, dc)

    diff.diffuse(arr3d, t)

    arr3d.forall(_ > 0) should be(true)
    arr3d.sum should be(amount +- eps)
    arr3d.mean should be((amount / (n * n * n)) +- eps)
    arr3d.sd should be(0d +- eps)
  }
}