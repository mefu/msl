package co.mefu.msl.linalg.diff

import cats.Show
import spire.implicits._

import co.mefu.msl.linalg.solver.TriDiagonalSolver._
import co.mefu.msl.linalg.signal.Convolution2D._

import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ds.SPMat
import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.ops.SPVecOps._
import co.mefu.msl.core.ops.SPMatOps._
import co.mefu.msl.core.ops.SPMat3DOps._

/**
  * Diffusion in 3D density map implemented from paper {reference}
  * This implementation only considers cubic space with cubic discretization.
  * l = length of cube
  * dl = discretization delta of length
  * dc = diffusion constant
  * dt = discretization delta of time
  * f = f-factor
  */
class Diffusion3DFFactor(l: Int, dc: Double, f: Double, dl: Double, dt: Double) {

  private val L: Int = Math.floor(l / dl).toInt

  private val alpha: Double = 3 * dl * dl / dt
  private val dcf: Double = dc * f
  private val beta: Double = dc * (3 - 2 * f)
  private val alpham4f: Double = alpha - 4 * dcf
  private val alpham3f: Double = alpha - 3 * dcf
  private val alpham2f: Double = alpha - 2 * dcf

  private val a: SPVec[Double] =
    SPVec.from(0d +: Array.fill(L - 1)(-1 * beta))
  private val b: SPVec[Double] = SPVec.from(
    (beta + alpha) +: Array.fill(L - 2)(2 * beta + alpha) :+ (beta + alpha))
  private val c: SPVec[Double] =
    SPVec.from(Array.fill(L - 1)(-1 * beta) :+ 0d)

  private val kernel: SPMat.S33[Double] =
    SPMat.S33(0, dcf, 0, dcf, alpham4f, dcf, 0, dcf, 0)

  private val padMat: SPMat.S22[Int] = SPMat.S22(1, 1, 1, 1)

  private val b1d1: SPVec[Double] = SPVec.fill(L)(0d)
  private val b1d2: SPVec[Double] = SPVec.fill(L)(0d)
  // private val b2d: SPMat[Double] = SPMat.fill(L, L)(0d)
  private val b2d: SPMat[Double] = SPMat.empty(L, L).emptyPadDest(padMat)

  private var m: Double = 0d

  // println(f"No negative coefficient = ${dt / (dl * dl)}%.5f < ${0.75 / f}%.5f" +
  //   f"= ${(dt / (dl * dl)) < (0.75 / f)} ")
  // println(f"Stability criterion = ${dt / (dl * dl)}%.5f <= ${1.5 / f}%.5f" +
  //   f"= ${(dt / (dl * dl)) < (1.5 / f)} ")

  def diffuse(u: SPMat3D[Double], T: Double): Unit = {
    // println(f"${T/dt}%.2f iterations.")
    cfor(0d)(_ < T, _ + dt)(t => {

      cfor(0)(_ < L, _ + 1)(i => conv2d(u.subI(i)))

      cfor(0)(_ < L, _ + 1)(j => {
        val us = u.subJ(j)
        cfor(0)(_ < L, _ + 1)(k => {
          val uss = us.subJ(k)
          tdma(a, b, c, uss, b1d1, b1d2, uss)
        })
      })

      cfor(0)(_ < L, _ + 1)(j => conv2d(u.subJ(j)))

      cfor(0)(_ < L, _ + 1)(i => {
        val us = u.subI(i)
        cfor(0)(_ < L, _ + 1)(k => {
          val uss = us.subJ(k)
          tdma(a, b, c, uss, b1d1, b1d2, uss)
        })
      })

      cfor(0)(_ < L, _ + 1)(k => conv2d(u.subK(k)))

      cfor(0)(_ < L, _ + 1)(i => {
        val us = u.subI(i)
        cfor(0)(_ < L, _ + 1)(j => {
          val uss = us.subI(j)
          tdma(a, b, c, uss, b1d1, b1d2, uss)
        })
      })
    })
  }

  def conv2d(u: SPMat[Double]): Unit = {
    u.padMirrorTo(padMat, b2d)
    conv2d_3x3_pk(b2d, kernel, u)
  }
}

object Diffusion3DFFactor {

  def apply(l: Int,
            dc: Double = 0.5,
            f: Double = 0.01,
            dl: Double = 1,
            dt: Double = 1): Diffusion3DFFactor =
    new Diffusion3DFFactor(l, dc, f, dl, dt)
}

object DiffTest extends App {
  val n = 5
  val nhalf = n / 2
  val u = SPMat3D.fill(n, n, n)(0d)

  u(nhalf, nhalf, nhalf) = 10d

  val diff = Diffusion3DFFactor(n, 0.05)

  println(u.sum)

  diff.diffuse(u, 10)

  println(u.sum)

  implicit val doubleShow: Show[Double] = d => f"${d}%15.10f"

  println(u.show)


}
