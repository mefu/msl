package co.mefu.msl.linalg.solver

import scala.{specialized => sp}
import scala.reflect.ClassTag

import spire.implicits._
import spire.algebra._

import co.mefu.msl.core.ds.SPVec

object TriDiagonalSolver {
  def tdma[@sp(Int, Long, Float, Double) T: ClassTag](
      a: SPVec[T],
      b: SPVec[T],
      c: SPVec[T],
      d: SPVec[T])(implicit ev: Field[T]): SPVec[T] = {
    val buf1 = SPVec.fill[T](b.x)(ev.zero)
    val buf2 = SPVec.fill[T](b.x)(ev.zero)
    val x = SPVec.fill[T](b.x)(ev.zero)
    tdma(a, b, c, d, buf1, buf2, x)
    x
  }

  def tdma[@sp(Int, Long, Float, Double) T](
      a: SPVec[T],
      b: SPVec[T],
      c: SPVec[T],
      d: SPVec[T],
      buf1: SPVec[T],
      buf2: SPVec[T],
      x: SPVec[T])(implicit ev: Field[T]): Unit = {

    val n = b.x
    var m = ev.zero

    buf1(0) = b(0)
    buf2(0) = d(0)
    cfor(1)(_ < n, _ + 1)(i => {
      m = a(i) / buf1(i - 1)
      buf1(i) = b(i) - m * c(i - 1)
      buf2(i) = d(i) - m * buf2(i - 1)
    })

    x(n - 1) = buf2(n - 1) / buf1(n - 1)

    cfor(n - 2)(_ >= 0, _ - 1)(i => {
      x(i) = (buf2(i) - c(i) * x(i + 1)) / buf1(i)
    })
  }
}
