package co.mefu.msl.linalg.signal

import scala.{specialized => sp}
import scala.reflect.ClassTag

import cats.Show
import spire.implicits._
import spire.algebra._

import co.mefu.msl.core.SPFunc3

import co.mefu.msl.core.ds.SPMat
import co.mefu.msl.core.ops.SPMatOps._

object Convolution2D {

  @inline final def clamp(v: Int, min: Int, max: Int): Int = {
    if (v < min) min
    else if (v > max) max
    else v
  }

  def conv2d[@sp(Int, Long, Float, Double) T: ClassTag](
      input: SPMat[T],
      kernel: SPMat[T]
  )(implicit ev: Ring[T]): SPMat[T] = {
    val out = SPMat.empty[T](input.x, input.y)
    conv2d(input, kernel, out)
    out
  }

  def conv2d[@sp(Int, Long, Float, Double) T](
      input: SPMat[T],
      kernel: SPMat[T],
      out: SPMat[T]
  )(implicit ev: Ring[T]): Unit = {
    val kCx = kernel.x / 2
    val kCy = kernel.y / 2

    cfor(0)(_ < out.x, _ + 1)(i => {
      cfor(0)(_ < out.y, _ + 1)(j => {
        out(i, j) = ev.zero
        cfor(0)(_ < kernel.x, _ + 1)(ki => {
          cfor(0)(_ < kernel.y, _ + 1)(kj => {
            val ii = Math.min(Math.max(i + ki - kCx, 0), out.x - 1)
            val jj = Math.min(Math.max(j + kj - kCy, 0), out.y - 1)
            out(i, j) += kernel(ki, kj) * input(ii, jj)
          })
        })
      })
    })
  }

  // Convolution 2D
  // 3x3 Plus Shaped Kernel as in
  // 0  x1  0
  // x2 x3  x4
  // 0  x5  0
  def conv2d_3x3_pk[@sp(Int, Long, Float, Double) T](
      input: SPMat[T],
      kernel: SPMat.S33[T],
      out: SPMat[T]
  )(implicit ev: Ring[T]): Unit = {
    // no border conditions
    val x1 = kernel(0, 1)
    val x2 = kernel(1, 0)
    val x3 = kernel(1, 1)
    val x4 = kernel(1, 2)
    val x5 = kernel(2, 1)

    cfor(0)(_ < out.x, _ + 1)(i => {
      cfor(0)(_ < out.y, _ + 1)(j => {
        out(i, j) = x1 * input(i, j - 1) +
          x3 * input(i, j) +
          x5 * input(i, j + 1) +
          x2 * input(i - 1, j) +
          x4 * input(i + 1, j)
      })
    })
  }
}
