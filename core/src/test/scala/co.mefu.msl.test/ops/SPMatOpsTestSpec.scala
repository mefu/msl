package co.mefu.msl.test.ops

import org.scalatest._
import Matchers._

import cats.Show
import spire.implicits._

import co.mefu.msl.test.BaseSpec

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._
import co.mefu.msl.core.SPFunc1
import co.mefu.msl.core.SPFunc2
import co.mefu.msl.core.SPFunc3

import co.mefu.msl.core.ds.SPMat
import co.mefu.msl.core.ops.SPMatOps._

class SPMatOpsTestSpec extends BaseSpec {

  val x = 1000
  val y = 1000
  val sizee = x * y
  val data = Array.fill(sizee)(Random.next[Double](0, sizee))

  def idx(i: Int, j: Int): Int = i * y + j

  "SPMat" should "foreach" in {
    val mat = SPMat.from(x, y)(data.clone)

    var sum = 0d

    mat.foreach(sum += _)

    sum should be(data.sum)
  }

  it should "foreach with index" in {
    val mat = SPMat.from(x, y)(data.clone)

    var sumPowI = 0d

    mat.foreach((i, j, el) => sumPowI += Math.pow(el, idx(i, j)))

    sumPowI should be(
      data.zipWithIndex.map { case (el, i) => Math.pow(el, i) }.sum)
  }

  it should "transform" in {
    val mat = SPMat.from(x, y)(data.clone)

    mat.transform(el => el * 2 + 0.1)

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      mat(i, j) should be(data(idx(i, j)) * 2 + 0.1)
    }
  }

  it should "transform with index" in {
    val mat = SPMat.from(x, y)(data.clone)

    mat.transform((i, j, el) => Math.pow(el, idx(i, j)))

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      mat(i, j) should be(Math.pow(data(idx(i, j)), idx(i, j)))
    }
  }

  it should "fold" in {
    val mat = SPMat.from(x, y)(data.clone)

    val sum = mat.fold(0d)(_ + _)

    sum should be(data.sum)
  }

  it should "reduce" in {
    val mat = SPMat.from(x, y)(data.clone)

    val sum = mat.reduce(_ + _)

    sum should be(data.sum)
  }

  it should "forall" in {
    val mat = SPMat.from(x, y)(data.clone)
    val max = data.max
    val min = data.min

    val result1 = mat.forall(_ <= max)
    val result2 = mat.forall(_ <= (max + min) / 2)

    result1 should be(true)
    result2 should be(false)
  }

  it should "forall with index" in {
    val mat = SPMat.from(x, y)(data.clone)

    val result1 = mat.forall((i, j, el) => el == data(idx(i, j)))

    result1 should be(true)
  }

  it should "find" in {
    val mat = SPMat.from(x, y)(data.clone)
    val max = data.max
    val min = data.min

    val result1 = mat.find(_ == max + 1)

    val result2 = mat.find(_ == min)

    result1 should be(None)
    result2 should be(Some(min))
  }

  it should "find with index" in {
    val mat = SPMat.from(x, y)(data.clone)

    val result1 = mat.find((i, j, el) => i == x / 2 && j == y / 3)

    result1 should be(Some(data(idx(x / 2, y / 3))))
  }

  it should "fill" in {
    val mat = SPMat.from(x, y)(data.clone)
    val newData = Array.fill(sizee)(Random.next[Double](0, sizee))
    val newMat = SPMat.from(x, y)(newData.clone)

    mat.fill(newMat)

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      mat(i, j) should be(newData(idx(i, j)))
    }
  }

  it should "copy to another matrix" in {
    val mat = SPMat.from(x, y)(data.clone)
    val dst = SPMat.empty[Double](x, y)

    mat.copyTo(dst)

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      mat(i, j) should be(dst(i, j))
    }
  }

  it should "swap matrices" in {
    val mat = SPMat.from(x, y)(data.clone)
    val newData = Array.fill(sizee)(Random.next[Double](0, sizee))
    val newMat = SPMat.from(x, y)(newData.clone)

    mat.swap(newMat)

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      mat(i, j) should be(newData(idx(i, j)))
      newMat(i, j) should be(data(idx(i, j)))
    }
  }

  it should "map" in {
    val mat = SPMat.from(x, y)(data.clone)

    val newVec = mat.map(el => el * 2 + 0.1)

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      newVec(i, j) should be(data(idx(i, j)) * 2 + 0.1)
    }
  }

  it should "copy" in {
    val mat = SPMat.from(x, y)(data.clone)
    val cl = mat.copy()

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      cl(i, j) should be(data(idx(i, j)))
    }
  }

  it should "pad with constant" in {
    val mat = SPMat.from(x, y)(data.clone)
    val padEl = 5d
    val padMat = SPMat.S22(4, 2, 3, 1)
    val padded = mat.padWith(padMat, padEl)

    for {
      i <- 0 - padMat(0, 0) until x + padMat(0, 1)
      j <- 0 - padMat(1, 0) until y + padMat(1, 1)
    } {
      if (i < 0 || j < 0 || i > x - 1 || j > x - 1)
        padded(i, j) should be(padEl)
      else
        padded(i, j) should be(data(idx(i, j)))
    }
  }

  it should "pad clamp" in {
    val mat = SPMat.from(x, y)(data.clone)
    val padMat = SPMat.S22(12, 10, 4, 9)
    val padded = mat.padClamp(padMat)

    for {
      i <- 0 - padMat(0, 0) until x + padMat(0, 1)
      j <- 0 - padMat(1, 0) until y + padMat(1, 1)
    } {
      if (i < 0 && j < 0)
        padded(i, j) should be(data(idx(0, 0)))
      else if (i < 0 && j > y - 1)
        padded(i, j) should be(data(idx(0, y - 1)))
      else if (i > x - 1 && j < 0)
        padded(i, j) should be(data(idx(x - 1, 0)))
      else if (i > x - 1 && j > y - 1)
        padded(i, j) should be(data(idx(x - 1, y - 1)))
      else if (i < 0)
        padded(i, j) should be(data(idx(0, j)))
      else if (i > x - 1)
        padded(i, j) should be(data(idx(x - 1, j)))
      else if (j < 0)
        padded(i, j) should be(data(idx(i, 0)))
      else if (j > y - 1)
        padded(i, j) should be(data(idx(i, y - 1)))
      else
        padded(i, j) should be(data(idx(i, j)))
    }
  }

  it should "show" in {
    val mat = SPMat.from(3, 3)(
      Array(0.123, 0.234, 0.456, 1.345, 1.678, 1.497, 2.612, 4.543, 3.567))

    implicit val doubleShow = new Show[Double] {
      def show(d: Double): String = f"${d}%.2f"
    }

    val expected = "[ [ 0.12 0.23 0.46 ]\n" +
      "  [ 1.35 1.68 1.50 ]\n" +
      "  [ 2.61 4.54 3.57 ] ]"

    mat.show should be(expected)
  }

  it should "do isEqual, add, sub, mult, div" in {
    val mat = SPMat.from(x, y)(data.clone)
    val newData = Array.fill(sizee)(Random.next[Double](0, sizee))
    val newMat = SPMat.from(x, y)(newData.clone)
    val value = Random.next[Double](0, sizee)

    val mat1 = mat.copy()
    val mat2 = mat.copy()
    val mat3 = mat.copy()
    val mat4 = mat.copy()
    val mat5 = mat.copy()
    val mat6 = mat.copy()
    val mat7 = mat.copy()

    mat.isEqual(mat1) should be(true)

    mat.add(newMat)
    mat1.sub(newMat)
    mat2.mult(newMat)
    mat3.div(newMat)

    mat4.add(value)
    mat5.sub(value)
    mat6.mult(value)
    mat7.div(value)

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      val idxx = idx(i, j)
      mat(i, j) should be(data(idxx) + newData(idxx))
      mat1(i, j) should be(data(idxx) - newData(idxx))
      mat2(i, j) should be(data(idxx) * newData(idxx))
      mat3(i, j) should be(data(idxx) / newData(idxx))
      mat4(i, j) should be(data(idxx) + value)
      mat5(i, j) should be(data(idxx) - value)
      mat6(i, j) should be(data(idxx) * value)
      mat7(i, j) should be(data(idxx) / value)
    }
  }

  it should "calculate sum, max, min, mean, variance, std deviance" in {
    val mat = SPMat.from(x, y)(data.clone)

    val sum = data.sum
    val max = data.max
    val min = data.min
    val mean = sum / data.size
    val variance = data.map(el => Math.pow(el - mean, 2)).sum / data.size
    val sd = Math.sqrt(variance)

    mat.sum should be(sum)
    mat.max should be(max)
    mat.min should be(min)
    mat.mean should be(mean)
    mat.variance should be(variance)
    mat.sd should be(sd)
  }

}
