package co.mefu.msl.test.ops

import org.scalatest._
import Matchers._

import cats.Show
import spire.implicits._

import co.mefu.msl.test.BaseSpec

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._
import co.mefu.msl.core.SPFunc1
import co.mefu.msl.core.SPFunc2

import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ops.SPVecOps._

class SPVecOpsTestSpec extends BaseSpec {

  val x = 1000000
  val data = Array.fill(x)(Random.next[Double](0, x))

  "SPVec" should "foreach" in {
    val vec = SPVec.from(data.clone)

    var sum = 0d

    vec.foreach(sum += _)

    sum should be(data.sum)
  }

  it should "foreach with index" in {
    val vec = SPVec.from(data.clone)

    var sumPowI = 0d

    vec.foreach((i, el) => sumPowI += Math.pow(el, i))

    sumPowI should be(
      data.zipWithIndex.map { case (el, i) => Math.pow(el, i) }.sum)
  }

  it should "transform" in {
    val vec = SPVec.from(data.clone)

    vec.transform(el => el * 2 + 0.1)

    for (i <- 0 until x)
      vec(i) should be(data(i) * 2 + 0.1)
  }

  it should "transform with index" in {
    val vec = SPVec.from(data.clone)

    vec.transform((i, el) => Math.pow(el, i))

    for (i <- 0 until x)
      vec(i) should be(Math.pow(data(i), i))
  }

  it should "fold" in {
    val vec = SPVec.from(data.clone)

    val sum = vec.fold(0d)(_ + _)

    sum should be(data.sum)
  }

  it should "reduce" in {
    val vec = SPVec.from(data.clone)

    val sum = vec.reduce(_ + _)

    sum should be(data.sum)
  }

  it should "forall" in {
    val vec = SPVec.from(data.clone)
    val max = data.max
    val min = data.min

    val result1 = vec.forall(_ <= max)

    val result2 = vec.forall(_ <= (max + min) / 2)

    result1 should be(true)
    result2 should be(false)
  }

  it should "forall with index" in {
    val vec = SPVec.from(data.clone)

    val result1 = vec.forall((i, el) => el == data(i))

    result1 should be(true)
  }

  it should "find" in {
    val vec = SPVec.from(data.clone)
    val max = data.max
    val min = data.min

    val result1 = vec.find(_ == max)

    val result2 = vec.find(_ == min - 1)

    result1 should be(Some(max))
    result2 should be(None)
  }

  it should "find with index" in {
    val vec = SPVec.from(data.clone)

    val result1 = vec.find((i, el) => i == x / 2)

    result1 should be(Some(data(x / 2)))
  }

  it should "fill" in {
    val vec = SPVec.from(data.clone)
    val newData = Array.fill(x)(Random.next[Double](0, x))
    val newVec = SPVec.from(newData.clone)

    vec.fill(newVec)

    for (i <- 0 until x)
      vec(i) should be(newData(i))
  }

  it should "copy to another vector" in {
    val vec = SPVec.from(data.clone)
    val dst = SPVec.empty[Double](x)

    vec.copyTo(dst)

    for (i <- 0 until x)
      dst(i) should be(data(i))
  }

  it should "swap vectors" in {
    val vec = SPVec.from(data.clone)
    val newData = Array.fill(x)(Random.next[Double](0, x))
    val newVec = SPVec.from(newData.clone)

    vec.swap(newVec)

    for (i <- 0 until x) {
      vec(i) should be(newData(i))
      newVec(i) should be(data(i))
    }
  }

  it should "swap elements" in {
    val vec = SPVec.from(data.clone)

    vec.swap(0, x - 1)

    vec(0) should be(data(x - 1))
    vec(x - 1) should be(data(0))
  }

  it should "reverse" in {
    val vec = SPVec.from(data.clone)

    vec.reverse()

    for (i <- 0 until x)
      vec(i) should be(data(x - i - 1))
  }

  it should "map" in {
    val vec = SPVec.from(data.clone)

    val newVec = vec.map(el => el * 2 + 0.1)

    for (i <- 0 until x)
      newVec(i) should be(data(i) * 2 + 0.1)
  }

  it should "copy" in {
    val vec = SPVec.from(data.clone)
    val cl = vec.copy()

    for (i <- 0 until x)
      cl(i) should be(data(i))
  }

  it should "pad with constant" in {
    val vec = SPVec.from(data.clone)
    val padEl = 5d
    val padVec = SPVec.S2(5, 8)
    val padded = vec.padWith(padVec, padEl)

    for (i <- (0 - padVec(0)) until (x + padVec(1))) {
      if (i < 0 || i > x - 1)
        padded(i) should be(padEl)
      else
        padded(i) should be(data(i))
    }
  }

  it should "pad clamp" in {
    val vec = SPVec.from(data.clone)
    val padVec = SPVec.S2(24, 11)
    val padded = vec.padClamp(padVec)

    for (i <- (0 - padVec(0)) until (x + padVec(1))) {
      if (i < 0)
        padded(i) should be(data(0))
      else if (i > x - 1)
        padded(i) should be(data(x - 1))
      else
        padded(i) should be(data(i))
    }
  }

  it should "show" in {
    val vec = SPVec.from(Array(0.231, 1.25345, 5.4324324, 7.4374324))

    implicit val doubleShow = new Show[Double] {
      def show(d: Double): String = f"${d}%.2f"
    }

    vec.show should be("[ 0.23 1.25 5.43 7.44 ]")
  }

  it should "do isEqual, add, sub, mult, div" in {
    val vec = SPVec.from(data.clone)
    val newData = Array.fill(x)(Random.next[Double](0, x))
    val newVec = SPVec.from(newData.clone)
    val value = Random.next[Double](0, x)

    val vec1 = vec.copy()
    val vec2 = vec.copy()
    val vec3 = vec.copy()
    val vec4 = vec.copy()
    val vec5 = vec.copy()
    val vec6 = vec.copy()
    val vec7 = vec.copy()

    vec.isEqual(vec1) should be(true)

    vec.addI(newVec)
    vec1.subI(newVec)
    vec2.multI(newVec)
    vec3.divI(newVec)

    vec4.addI(value)
    vec5.subI(value)
    vec6.multI(value)
    vec7.divI(value)

    for (i <- 0 until x) {
      vec(i) should be(data(i) + newData(i))
      vec1(i) should be(data(i) - newData(i))
      vec2(i) should be(data(i) * newData(i))
      vec3(i) should be(data(i) / newData(i))
      vec4(i) should be(data(i) + value)
      vec5(i) should be(data(i) - value)
      vec6(i) should be(data(i) * value)
      vec7(i) should be(data(i) / value)
    }
  }

  it should "calculate sum, max, min, mean, variance, std deviance" in {
    val vec = SPVec.from(data.clone)

    val sum = data.sum
    val max = data.max
    val min = data.min
    val mean = sum / data.size
    val variance = data.map(el => Math.pow(el - mean, 2)).sum / data.size
    val sd = Math.sqrt(variance)

    vec.sum should be(sum)
    vec.max should be(max)
    vec.min should be(min)
    vec.mean should be(mean)
    vec.variance should be(variance)
    vec.sd should be(sd)
  }

}
