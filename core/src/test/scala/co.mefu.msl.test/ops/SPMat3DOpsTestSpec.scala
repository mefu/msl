package co.mefu.msl.test.ops

import org.scalatest._
import Matchers._

import cats.Show
import spire.implicits._

import co.mefu.msl.test.BaseSpec

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._
import co.mefu.msl.core.SPFunc1
import co.mefu.msl.core.SPFunc2
import co.mefu.msl.core.SPFunc4

import co.mefu.msl.core.ds.SPMat3D
import co.mefu.msl.core.ops.SPMat3DOps._

class SPMat3DOpsTestSpec extends BaseSpec {

  val x = 100
  val y = 100
  val z = 100
  val sizee = x * y * z
  val data = Array.fill(sizee)(Random.next[Double](0, sizee))

  def idx(i: Int, j: Int, k: Int): Int = i * y * z + j * z + k

  "SPMat3D" should "foreach" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)

    var sum = 0d

    mat.foreach(sum += _)

    sum should be(data.sum)
  }

  it should "foreach with index" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)

    var sumPowI = 0d

    mat.foreach((i, j, k, el) => sumPowI += Math.pow(el, idx(i, j, k)))

    sumPowI should be(
      data.zipWithIndex.map { case (el, i) => Math.pow(el, i) }.sum)
  }

  it should "transform" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)

    mat.transform(el => el * 2 + 0.1)

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      mat(i, j, k) should be(data(idx(i, j, k)) * 2 + 0.1)
    }
  }

  it should "transform with index" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)

    mat.transform((i, j, k, el) => Math.pow(el, idx(i, j, k)))

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      mat(i, j, k) should be(Math.pow(data(idx(i, j, k)), idx(i, j, k)))
    }
  }

  it should "fold" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)

    val sum = mat.fold(0d)(_ + _)

    sum should be(data.sum)
  }

  it should "reduce" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)

    val sum = mat.reduce(_ + _)

    sum should be(data.sum)
  }

  it should "forall" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)
    val max = data.max
    val min = data.min

    val result1 = mat.forall(_ <= max)

    val result2 = mat.forall(_ <= (max + min) / 2)

    result1 should be(true)
    result2 should be(false)
  }

  it should "forall with index" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)

    val result1 = mat.forall((i, j, k, el) => el == data(idx(i, j, k)))

    result1 should be(true)
  }

  it should "find" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)
    val max = data.max
    val min = data.min

    val result1 = mat.find(_ == max)

    val result2 = mat.find(_ == min)

    result1 should be(Some(max))
    result2 should be(Some(min))
  }

  it should "find with index" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)

    val result1 =
      mat.find((i, j, k, el) => i == x / 2 && j == y / 3 && k == z / 4)

    result1 should be(Some(data(idx(x / 2, y / 3, z / 4))))
  }

  it should "fill" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)
    val newData = Array.fill(sizee)(Random.next[Double](0, sizee))
    val newMat = SPMat3D.from(x, y, z)(newData.clone)

    mat.fill(newMat)

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      mat(i, j, k) should be(newData(idx(i, j, k)))
    }
  }

  it should "copy to another matrix" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)
    val dst = SPMat3D.empty[Double](x, y, z)

    mat.copyTo(dst)

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      mat(i, j, k) should be(dst(i, j, k))
    }
  }

  it should "swap matrices" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)
    val newData = Array.fill(sizee)(Random.next[Double](0, sizee))
    val newMat = SPMat3D.from(x, y, z)(newData.clone)

    mat.swap(newMat)

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      mat(i, j, k) should be(newData(idx(i, j, k)))
      newMat(i, j, k) should be(data(idx(i, j, k)))
    }
  }

  it should "map" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)

    val newMat = mat.map(el => el * 2 + 0.1)

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      newMat(i, j, k) should be(data(idx(i, j, k)) * 2 + 0.1)
    }
  }

  it should "copy" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)
    val cl = mat.copy()

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      cl(i, j, k) should be(data(idx(i, j, k)))
    }
  }

  it should "show" in {
    val mat = SPMat3D.from(2, 2, 2)(
      Array(0.123, 0.234, 0.456, 1.345, 1.678, 1.497, 2.612, 4.543))

    implicit val doubleShow = new Show[Double] {
      def show(d: Double): String = f"${d}%.2f"
    }

    val expected = "[\n" +
      "[ [ 0.12 0.23 ]\n" +
      "  [ 0.46 1.35 ] ]\n\n" +
      "[ [ 1.68 1.50 ]\n" +
      "  [ 2.61 4.54 ] ]\n" +
      "]"

    mat.show should be(expected)
  }

  it should "do isEqual, add, sub, mult, div" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)
    val newData = Array.fill(sizee)(Random.next[Double](0, sizee))
    val newMat = SPMat3D.from(x, y, z)(newData.clone)
    val value = Random.next[Double](0, sizee)

    val mat1 = mat.copy()
    val mat2 = mat.copy()
    val mat3 = mat.copy()
    val mat4 = mat.copy()
    val mat5 = mat.copy()
    val mat6 = mat.copy()
    val mat7 = mat.copy()

    mat.isEqual(mat1) should be(true)

    mat.add(newMat)
    mat1.sub(newMat)
    mat2.mult(newMat)
    mat3.div(newMat)

    mat4.add(value)
    mat5.sub(value)
    mat6.mult(value)
    mat7.div(value)

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      val idxx = idx(i, j, k)
      mat(i, j, k) should be(data(idxx) + newData(idxx))
      mat1(i, j, k) should be(data(idxx) - newData(idxx))
      mat2(i, j, k) should be(data(idxx) * newData(idxx))
      mat3(i, j, k) should be(data(idxx) / newData(idxx))
      mat4(i, j, k) should be(data(idxx) + value)
      mat5(i, j, k) should be(data(idxx) - value)
      mat6(i, j, k) should be(data(idxx) * value)
      mat7(i, j, k) should be(data(idxx) / value)
    }
  }

  it should "calculate sum, max, min, mean, variance, std deviance" in {
    val mat = SPMat3D.from(x, y, z)(data.clone)

    val sum = data.sum
    val max = data.max
    val min = data.min
    val mean = sum / data.size
    val variance = data.map(el => Math.pow(el - mean, 2)).sum / data.size
    val sd = Math.sqrt(variance)

    mat.sum should be(sum)
    mat.max should be(max)
    mat.min should be(min)
    mat.mean should be(mean)
    mat.variance should be(variance)
    mat.sd should be(sd)
  }

}
