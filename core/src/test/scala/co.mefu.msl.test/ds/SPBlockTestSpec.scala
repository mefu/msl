package co.mefu.msl.test.ds

import org.scalatest._
import Matchers._

import co.mefu.msl.test.BaseSpec

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPBlock

class SPBlockTestSpec extends BaseSpec {

  val x = 1000000
  val data = Array.fill(x)(Random.next[Double](0, x))

  "SPBlock" should "access data correctly" in {
    val spb = SPBlock.from(data.clone)

    for (i <- 0 until x)
      spb(i) should be(data(i))
  }

  it should "update data correctly" in {
    val spb = SPBlock.from(data.clone)
    val newData = Array.fill(x)(Random.next[Double](0, x))

    for (i <- 0 until x)
      spb(i) = newData(i)

    for (i <- 0 until x)
      spb(i) should be(newData(i))
  }
}
