package co.mefu.msl.test.ds

import org.scalatest._
import Matchers._

import co.mefu.msl.test.BaseSpec

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPMat

class SPMatTestSpec extends BaseSpec {

  val x = 1000
  val y = 1000
  val sizee = x * y
  val data = Array.fill(sizee)(Random.next[Double](0, sizee))

  "SPMat" should "access data" in {
    val mat = SPMat.from(x, y)(data.clone)

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      mat(i, j) should be(data(i * y + j))
    }
  }

  it should "update data" in {
    val mat = SPMat.from(x, y)(data.clone)
    val newData = Array.fill(sizee)(Random.next[Double](0, sizee))

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      mat(i, j) = newData(i * y + j)
    }

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      mat(i, j) should be(newData(i * y + j))
    }
  }

  it should "access sub vectors" in {
    val mat = SPMat.from(x, y)(data.clone)

    for (i <- 0 until x) {
      val subI = mat.subI(i)
      for (j <- 0 until y) {
        subI(j) should be(data(i * y + j))
      }
    }

    for (j <- 0 until y) {
      val subJ = mat.subJ(j)
      for (i <- 0 until x) {
        subJ(i) should be(data(i * y + j))
      }
    }
  }

  it should "update sub vectors" in {
    val mat = SPMat.from(x, y)(data.clone)
    val mat2 = SPMat.from(x, y)(data.clone)
    val newData = Array.fill(sizee)(Random.next[Double](0, sizee))

    for (i <- 0 until x) {
      val subI = mat.subI(i)
      for (j <- 0 until y) {
        subI(j) = newData(i * y + j)
      }
    }

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      mat(i, j) should be(newData(i * y + j))
    }

    for (j <- 0 until y) {
      val subJ = mat2.subJ(j)
      for (i <- 0 until x) {
        subJ(i) = newData(i * y + j)
      }
    }

    for {
      i <- 0 until x
      j <- 0 until y
    } {
      mat2(i, j) should be(newData(i * y + j))
    }
  }

}
