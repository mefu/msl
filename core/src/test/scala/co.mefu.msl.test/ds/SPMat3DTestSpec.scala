package co.mefu.msl.test.ds

import org.scalatest._
import Matchers._

import co.mefu.msl.test.BaseSpec

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPMat3D

class SPMat3DTestSpec extends BaseSpec {

  val x = 100
  val y = 100
  val z = 100
  val sizee = x * y * z
  val data = Array.fill(sizee)(Random.next[Double](0, sizee))

  "SPMat3D" should "access data" in {
    val mat3d = SPMat3D.from(x, y, z)(data.clone)

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      mat3d(i, j, k) should be(data(i * y * z + j * z + k))
    }
  }

  it should "update data" in {
    val mat3d = SPMat3D.from(x, y, z)(data.clone)
    val newData = Array.fill(sizee)(Random.next[Double](0, sizee))

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      mat3d(i, j, k) = newData(i * y * z + j * z + k)
    }

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      mat3d(i, j, k) should be(newData(i * y * z + j * z + k))
    }
  }

  it should "access sub matrices" in {
    val mat3d = SPMat3D.from(x, y, z)(data.clone)

    for (i <- 0 until x) {
      val sub = mat3d.subI(i)
      for (j <- 0 until y) {
        val ssub = sub.subI(j)
        for (k <- 0 until z) {
          ssub(k) should be(data(i * y * z + j * z + k))
        }
      }
    }
  }

  it should "update sub matrices" in {
    val mat3d = SPMat3D.from(x, y, z)(data.clone)
    val newData = Array.fill(sizee)(Random.next[Double](0, sizee))

    for (i <- 0 until x) {
      val sub = mat3d.subI(i)
      for (j <- 0 until y) {
        val ssub = sub.subI(j)
        for (k <- 0 until z) {
          ssub(k) = newData(i * y * z + j * z + k)
        }
      }
    }

    for {
      i <- 0 until x
      j <- 0 until y
      k <- 0 until z
    } {
      mat3d(i, j, k) should be(newData(i * y * z + j * z + k))
    }
  }

}
