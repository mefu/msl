package co.mefu.msl.test.ds

import org.scalatest._
import Matchers._

import co.mefu.msl.test.BaseSpec

import co.mefu.msl.core.Random
import co.mefu.msl.core.Random._

import co.mefu.msl.core.ds.SPVec

class SPVecTestSpec extends BaseSpec {

  val x = 1000000
  val data = Array.fill(x)(Random.next[Double](0, x))

  "SPVec" should "access data" in {
    val vec = SPVec.from(data.clone)

    for (i <- 0 until x)
      vec(i) should be(data(i))
  }

  it should "update data" in {
    val vec = SPVec.from(data.clone)
    val newData = Array.fill(x)(Random.next[Double](0, x))

    for (i <- 0 until x)
      vec(i) = newData(i)

    for (i <- 0 until x)
      vec(i) should be(newData(i))
  }

}
