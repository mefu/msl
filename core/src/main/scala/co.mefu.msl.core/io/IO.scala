package co.mefu.msl.core.io

import java.io._

object IO {

  def getOutputFile(filePath: String): File =
    new File(filePath)

  def getOutputWriter(filePath: String): PrintWriter =
    new PrintWriter(getOutputFile(filePath))

  def createFolder(filePath: String): Unit = {
    val file = new File(filePath)
    file.mkdirs()
  }

  def getNullWriter: PrintWriter =
    new PrintWriter(new OutputStream {
      override def write(b: Int): Unit = Unit
    })
}
