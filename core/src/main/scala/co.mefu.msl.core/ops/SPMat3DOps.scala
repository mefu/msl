package co.mefu.msl.core.ops

import scala.{specialized => sp}
import scala.reflect.ClassTag

import cats.Show

import spire.algebra._
import spire.implicits._

import co.mefu.msl.core.SPFunc1
import co.mefu.msl.core.SPFunc2
import co.mefu.msl.core.SPFunc3
import co.mefu.msl.core.SPFunc4

import co.mefu.msl.core.ds.SPMat3D

import SPMatOps._

object SPMat3DOps {

  implicit class SPMat3DOps[@sp(Int, Long, Float, Double) T](
      val mat: SPMat3D[T]
  ) {

    def foreach(f: SPFunc1[T, Unit]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            f(mat(i, j, k))
          })
        })
      })
    }

    def foreach(f: SPFunc4[Int, Int, Int, T, Unit]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            f(i, j, k, mat(i, j, k))
          })
        })
      })
    }

    def transform(f: SPFunc1[T, T]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            mat(i, j, k) = f(mat(i, j, k))
          })
        })
      })
    }

    def transform(f: SPFunc4[Int, Int, Int, T, T]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            mat(i, j, k) = f(i, j, k, mat(i, j, k))
          })
        })
      })
    }

    def fold(initial: T)(f: SPFunc2[T, T, T]): T = {
      var acc = initial
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            acc = f(acc, mat(i, j, k))
          })
        })
      })
      acc
    }

    def reduce(f: SPFunc2[T, T, T]): T = {
      var acc = mat(0, 0, 0)

      cfor(1)(_ < mat.z, _ + 1)(k => {
        acc = f(acc, mat(0, 0, k))
      })

      cfor(1)(_ < mat.y, _ + 1)(j => {
        cfor(0)(_ < mat.z, _ + 1)(k => {
          acc = f(acc, mat(0, j, k))
        })
      })

      cfor(1)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            acc = f(acc, mat(i, j, k))
          })
        })
      })

      acc
    }

    def forall(f: SPFunc1[T, Boolean]): Boolean = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            if (!f(mat(i, j, k))) return false
          })
        })
      })
      true
    }

    def forall(f: SPFunc4[Int, Int, Int, T, Boolean]): Boolean = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            if (!f(i, j, k, mat(i, j, k))) return false
          })
        })
      })
      true
    }

    def find(f: SPFunc1[T, Boolean]): Option[T] = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            val el = mat(i, j, k)
            if (f(el)) return Some(el)
          })
        })
      })
      None
    }

    def find(f: SPFunc4[Int, Int, Int, T, Boolean]): Option[T] = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            val el = mat(i, j, k)
            if (f(i, j, k, el)) return Some(el)
          })
        })
      })
      None
    }

    def fill(f: SPFunc3[Int, Int, Int, T]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            mat(i, j, k) = f(i, j, k)
          })
        })
      })
    }

    def copyTo(dst: SPMat3D[T]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            dst(i, j, k) = mat(i, j, k)
          })
        })
      })
    }

    def swap(other: SPMat3D[T]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            val tmp = other(i, j, k)
            other(i, j, k) = mat(i, j, k)
            mat(i, j, k) = tmp
          })
        })
      })
    }

    def swap(i1: Int, j1: Int, k1: Int, i2: Int, j2: Int, k2: Int): Unit = {
      val tmp = mat(i1, j1, k1)
      mat(i1, j1, k1) = mat(i2, j2, k2)
      mat(i2, j2, k2) = tmp
    }
  }

  implicit class SPMat3DOpsClassTag[@sp(Int, Long, Float, Double) T: ClassTag](
      val mat: SPMat3D[T]
  ) {

    def map(f: SPFunc1[T, T]): SPMat3D[T] = {
      val dst = SPMat3D.empty(mat.x, mat.y, mat.z)
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            dst(i, j, k) = f(mat(i, j, k))
          })
        })
      })
      dst
    }

    def copy(fromX: Int = 0,
             toX: Int = mat.x,
             fromY: Int = 0,
             toY: Int = mat.y,
             fromZ: Int = 0,
             toZ: Int = mat.z): SPMat3D[T] = {
      val dst = SPMat3D.empty(toX - fromX, toY - fromY, toZ - fromZ)
      cfor(fromX)(_ < toX, _ + 1)(i => {
        cfor(fromY)(_ < toY, _ + 1)(j => {
          cfor(fromZ)(_ < toZ, _ + 1)(k => {
            dst(i - fromX, j - fromY, k - fromZ) = mat(i, j, k)
          })
        })
      })
      dst
    }
  }

  implicit class SPMat3DOpsShow[@sp(Int, Long, Float, Double) T](
      val mat: SPMat3D[T]
  )(implicit ev: Show[T]) {

    def show: String = {
      val sb = new scala.collection.mutable.StringBuilder()
      cfor(0)(_ < mat.x, _ + 1)(i => {
        sb ++= (if (i == 0) "[\n" else "")
        sb ++= mat.subI(i).show
        sb ++= (if (i == mat.x - 1) "\n]" else "\n\n")
      })
      sb.toString
    }

  }

  implicit class SPMat3DOpsEq[@sp(Int, Long, Float, Double) T](
      val mat: SPMat3D[T]
  )(implicit ev: Eq[T]) {

    def isEqual(other: SPMat3D[T]): Boolean =
      mat.forall((i, j, k, el) => el === other(i, j, k))

  }

  implicit class SPMat3DOpsOrder[@sp(Int, Long, Float, Double) T](
      val mat: SPMat3D[T]
  )(implicit ev: Order[T]) {

    def max: T = mat.reduce(ev.max(_, _))

    def min: T = mat.reduce(ev.min(_, _))

  }

  implicit class SPMat3DOpsAdditiveMonoid[@sp(Int, Long, Float, Double) T](
      val mat: SPMat3D[T]
  )(implicit ev: AdditiveMonoid[T]) {

    def sum: T = mat.reduce(_ + _)

    def add(value: T): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            mat(i, j, k) += value
          })
        })
      })

    def add(other: SPMat3D[T]): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            mat(i, j, k) += other(i, j, k)
          })
        })
      })

  }

  implicit class SPMat3DOpsAdditiveAbGroup[@sp(Int, Long, Float, Double) T](
      val mat: SPMat3D[T]
  )(implicit ev: AdditiveAbGroup[T]) {

    def sub(value: T): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            mat(i, j, k) -= value
          })
        })
      })

    def sub(other: SPMat3D[T]): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            mat(i, j, k) -= other(i, j, k)
          })
        })
      })

  }

  implicit class SPMat3DOpsMultiplicativeMonoid[
      @sp(Int, Long, Float, Double) T](
      val mat: SPMat3D[T]
  )(implicit ev: MultiplicativeMonoid[T]) {

    def product: T = mat.reduce(_ * _)

    def mult(value: T): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            mat(i, j, k) *= value
          })
        })
      })

    def mult(other: SPMat3D[T]): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            mat(i, j, k) *= other(i, j, k)
          })
        })
      })

  }

  implicit class SPMat3DOpsField[@sp(Int, Long, Float, Double) T](
      val mat: SPMat3D[T]
  )(implicit ev: Field[T]) {

    def mean: T = mat.sum / mat.size

    def variance: T = {
      val m = mean
      var res = ev.zero

      mat.foreach(el => res += (el - m) ** 2)

      res / mat.size
    }

    def div(value: T): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            mat(i, j, k) /= value
          })
        })
      })

    def div(other: SPMat3D[T]): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          cfor(0)(_ < mat.z, _ + 1)(k => {
            mat(i, j, k) /= other(i, j, k)
          })
        })
      })

  }

  implicit class SPMat3DOpsFieldNRoot[@sp(Int, Long, Float, Double) T](
      val mat: SPMat3D[T]
  )(implicit ev1: Field[T], ev2: NRoot[T]) {

    def sd: T = ev2.sqrt(mat.variance)

  }
}
