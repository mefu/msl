package co.mefu.msl.core.ops

import scala.{specialized => sp}
import scala.reflect.ClassTag

import cats.Show

import spire.algebra._
import spire.implicits._

import co.mefu.msl.core.SPFunc1
import co.mefu.msl.core.SPFunc2
import co.mefu.msl.core.SPFunc3

import co.mefu.msl.core.ds.SPBlock
import co.mefu.msl.core.ds.SPMat

import SPVecOps._

object SPMatOps {

  implicit class SPMatOps[@sp(Int, Long, Float, Double) T](
      val mat: SPMat[T]
  ) {

    def foreach(f: SPFunc1[T, Unit]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          f(mat(i, j))
        })
      })
    }

    def foreach(f: SPFunc3[Int, Int, T, Unit]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          f(i, j, mat(i, j))
        })
      })
    }

    def transform(f: SPFunc1[T, T]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          mat(i, j) = f(mat(i, j))
        })
      })
    }

    def transform(f: SPFunc3[Int, Int, T, T]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          mat(i, j) = f(i, j, mat(i, j))
        })
      })
    }

    def fold(initial: T)(f: SPFunc2[T, T, T]): T = {
      var acc = initial
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          acc = f(acc, mat(i, j))
        })
      })
      acc
    }

    def reduce(f: SPFunc2[T, T, T]): T = {
      var acc = mat(0, 0)

      cfor(1)(_ < mat.y, _ + 1)(j => {
        acc = f(acc, mat(0, j))
      })

      cfor(1)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          acc = f(acc, mat(i, j))
        })
      })

      acc
    }

    def forall(f: SPFunc1[T, Boolean]): Boolean = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          if (!f(mat(i, j))) return false
        })
      })
      true
    }

    def forall(f: SPFunc3[Int, Int, T, Boolean]): Boolean = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          if (!f(i, j, mat(i, j))) return false
        })
      })
      true
    }

    def find(f: SPFunc1[T, Boolean]): Option[T] = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          val el = mat(i, j)
          if (f(el)) return Some(el)
        })
      })
      None
    }

    def find(f: SPFunc3[Int, Int, T, Boolean]): Option[T] = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          val el = mat(i, j)
          if (f(i, j, el)) return Some(el)
        })
      })
      None
    }

    def fill(f: SPFunc2[Int, Int, T]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          mat(i, j) = f(i, j)
        })
      })
    }

    def copyTo(dst: SPMat[T]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          dst(i, j) = mat(i, j)
        })
      })
    }

    def swap(other: SPMat[T]): Unit = {
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          val tmp = other(i, j)
          other(i, j) = mat(i, j)
          mat(i, j) = tmp
        })
      })
    }

    def swap(i1: Int, j1: Int, i2: Int, j2: Int): Unit = {
      val tmp = mat(i1, j1)
      mat(i1, j1) = mat(i2, j2)
      mat(i2, j2) = tmp
    }

    lazy val clamped: SPFunc2[Int, Int, T] =
      (i, j) =>
        mat(
          if (i < 0) 0
          else if (i > mat.x - 1) mat.x - 1
          else i,
          if (j < 0) 0
          else if (j > mat.y - 1) mat.y - 1
          else j
      )

    lazy val mirrored: SPFunc2[Int, Int, T] =
      (i, j) =>
        mat(
          if (i < 0) -1 - i
          else if (i > mat.x - 1) 2 * mat.x - i - 1
          else i,
          if (j < 0) -1 - j
          else if (j > mat.y - 1) 2 * mat.y - j - 1
          else j
      )

    def padTo(mat2: SPMat.S22[Int],
              padF: SPFunc2[Int, Int, T],
              dst: SPMat[T]): Unit = {
      mat.copyTo(dst)

      cfor(0 - mat2(0, 0))(_ < 0, _ + 1)(i => {
        cfor(0 - mat2(1, 0))(_ < mat.y + mat2(1, 1), _ + 1)(j => {
          dst(i, j) = padF(i, j)
        })
      })

      cfor(mat.x)(_ < mat.x + mat2(0, 1), _ + 1)(i => {
        cfor(0 - mat2(1, 0))(_ < mat.y + mat2(1, 1), _ + 1)(j => {
          dst(i, j) = padF(i, j)
        })
      })

      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0 - mat2(1, 0))(_ < 0, _ + 1)(j => {
          dst(i, j) = padF(i, j)
        })
      })

      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(mat.y)(_ < mat.y + mat2(1, 1), _ + 1)(j => {
          dst(i, j) = padF(i, j)
        })
      })
    }

    def padWithTo(mat2: SPMat.S22[Int], elem: T, dst: SPMat[T]): Unit = {
      padTo(mat2, (_, _) => elem, dst)
    }

    def padClampTo(mat2: SPMat.S22[Int], dst: SPMat[T]): Unit = {
      padTo(mat2, clamped, dst)
    }

    def padMirrorTo(mat2: SPMat.S22[Int], dst: SPMat[T]): Unit = {
      padTo(mat2, mirrored, dst)
    }
  }

  implicit class SPMatOpsClassTag[@sp(Int, Long, Float, Double) T: ClassTag](
      val mat: SPMat[T]
  ) {

    def map(f: SPFunc1[T, T]): SPMat[T] = {
      val dst = SPMat.empty(mat.x, mat.y)
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          dst(i, j) = f(mat(i, j))
        })
      })
      dst
    }

    def copy(fromX: Int = 0,
             toX: Int = mat.x,
             fromY: Int = 0,
             toY: Int = mat.y): SPMat[T] = {
      val dst = SPMat.empty(toX - fromX, toY - fromY)
      cfor(fromX)(_ < toX, _ + 1)(i => {
        cfor(fromY)(_ < toY, _ + 1)(j => {
          dst(i - fromX, j - fromY) = mat(i, j)
        })
      })
      dst
    }

    def emptyPadDest(mat2: SPMat.S22[Int]): SPMat[T] = {
      val newBlkX = mat2(0, 0) + mat.x + mat2(0, 1) // up down
      val newBlkY = mat2(1, 0) + mat.y + mat2(1, 1) // left right
      val dstBlk = SPBlock.empty[T](newBlkX * newBlkY)
      SPMat.create[T](
        mat.x, mat.y, newBlkY, 1, 
        mat2(1, 0) + newBlkY * mat2(0, 0)
      )(dstBlk)
    }

    def pad(mat2: SPMat.S22[Int], padF: SPFunc2[Int, Int, T]): SPMat[T] = {
      val dst = emptyPadDest(mat2)
      mat.padTo(mat2, padF, dst)
      dst
    }

    def padWith(mat2: SPMat.S22[Int], elem: T): SPMat[T] = {
      val dst = emptyPadDest(mat2)
      mat.padWithTo(mat2, elem, dst)
      dst
    }

    def padClamp(mat2: SPMat.S22[Int]): SPMat[T] = {
      val dst = emptyPadDest(mat2)
      mat.padClampTo(mat2, dst)
      dst
    }

    def padMirror(mat2: SPMat.S22[Int]): SPMat[T] = {
      val dst = emptyPadDest(mat2)
      mat.padMirrorTo(mat2, dst)
      dst
    }
  }

  implicit class SPMatOpsShow[@sp(Int, Long, Float, Double) T](
      val mat: SPMat[T]
  )(implicit ev: Show[T]) {

    def show: String = {
      val sb = new scala.collection.mutable.StringBuilder()
      cfor(0)(_ < mat.x, _ + 1)(i => {
        sb ++= (if (i == 0) "[ " else "  ")
        sb ++= mat.subI(i).show
        sb ++= (if (i == mat.x - 1) " ]" else "\n")
      })
      sb.toString
    }

  }

  implicit class SPMatOpsEq[@sp(Int, Long, Float, Double) T](
      val mat: SPMat[T]
  )(implicit ev: Eq[T]) {

    def isEqual(other: SPMat[T]): Boolean =
      mat.forall((i, j, el) => el === other(i, j))

  }

  implicit class SPMatOpsOrder[@sp(Int, Long, Float, Double) T](
      val mat: SPMat[T]
  )(implicit ev: Order[T]) {

    def max: T = mat.reduce(ev.max(_, _))

    def min: T = mat.reduce(ev.min(_, _))

  }

  implicit class SPMatOpsAdditiveMonoid[@sp(Int, Long, Float, Double) T](
      val mat: SPMat[T]
  )(implicit ev: AdditiveMonoid[T]) {

    def sum: T = mat.reduce(_ + _)

    def add(value: T): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          mat(i, j) += value
        })
      })

    def add(other: SPMat[T]): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          mat(i, j) += other(i, j)
        })
      })

  }

  implicit class SPMatOpsAdditiveAbGroup[@sp(Int, Long, Float, Double) T](
      val mat: SPMat[T]
  )(implicit ev: AdditiveAbGroup[T]) {

    def sub(value: T): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          mat(i, j) -= value
        })
      })

    def sub(other: SPMat[T]): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          mat(i, j) -= other(i, j)
        })
      })

  }

  implicit class SPMatOpsMultiplicativeMonoid[@sp(Int, Long, Float, Double) T](
      val mat: SPMat[T]
  )(implicit ev: MultiplicativeMonoid[T]) {

    def product: T = mat.reduce(_ * _)

    def mult(value: T): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          mat(i, j) *= value
        })
      })

    def mult(other: SPMat[T]): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          mat(i, j) *= other(i, j)
        })
      })
  }

  implicit class SPMatOpsField[@sp(Int, Long, Float, Double) T](
      val mat: SPMat[T]
  )(implicit ev: Field[T]) {

    def mean: T = mat.sum / mat.size

    def variance: T = {
      val m = mean
      var res = ev.zero

      mat.foreach(el => res += (el - m) ** 2)

      res / mat.size
    }

    def div(value: T): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          mat(i, j) /= value
        })
      })

    def div(other: SPMat[T]): Unit =
      cfor(0)(_ < mat.x, _ + 1)(i => {
        cfor(0)(_ < mat.y, _ + 1)(j => {
          mat(i, j) /= other(i, j)
        })
      })

  }

  implicit class SPMatOpsFieldNRoot[@sp(Int, Long, Float, Double) T](
      val mat: SPMat[T]
  )(implicit ev1: Field[T], ev2: NRoot[T]) {

    def sd: T = ev2.sqrt(mat.variance)

  }
}
