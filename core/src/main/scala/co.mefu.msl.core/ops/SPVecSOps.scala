package co.mefu.msl.core.ops

import scala.{specialized => sp}
import scala.reflect.ClassTag

import cats.Show

import spire.algebra._
import spire.implicits._

import co.mefu.msl.core.SPFunc1
import co.mefu.msl.core.SPFunc2

import co.mefu.msl.core.ds.SPBlock
import co.mefu.msl.core.ds.SPVec

object SPVecSOps {

  implicit class SPVecS2OpsCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S2[T]
  ) {
    def change(x0: T = vec(0), x1: T = vec(1)): SPVec.S2[T] = SPVec.S2(x0, x1)
  }

  implicit class SPVecS2OpsNumericCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S2[T]
  )(implicit ev: Numeric[T]) {
    def toDouble: SPVec.S2[Double] = 
      SPVec.S2(ev.toDouble(vec(0)), ev.toDouble(vec(1)))
  }

  implicit class SPVecS2OpsAdditiveMonoidCT[
      @sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S2[T]
  )(implicit ev: AdditiveMonoid[T]) {

    def add(value: T): SPVec.S2[T] =
      SPVec.S2(vec(0) + value, vec(1) + value)

    def add(other: SPVec.S2[T]): SPVec.S2[T] =
      SPVec.S2(vec(0) + other(0), vec(1) + other(1))

  }

  implicit class SPVecS2OpsAdditiveAbGroupCT[
      @sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S2[T]
  )(implicit ev: AdditiveAbGroup[T]) {

    def sub(value: T): SPVec.S2[T] =
      SPVec.S2(vec(0) - value, vec(1) - value)

    def sub(other: SPVec.S2[T]): SPVec.S2[T] =
      SPVec.S2(vec(0) - other(0), vec(1) - other(1))

  }

  implicit class SPVecS2OpsMultiplicativeMonoidCT[
      @sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S2[T]
  )(implicit ev: MultiplicativeMonoid[T]) {

    def mult(value: T): SPVec.S2[T] =
      SPVec.S2(vec(0) * value, vec(1) * value)

    def mult(other: SPVec.S2[T]): SPVec.S2[T] =
      SPVec.S2(vec(0) * other(0), vec(1) * other(1))

  }

  implicit class SPVecS2OpsRingCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S2[T]
  )(implicit ev: Ring[T]) {

    def pow(value: Int): SPVec.S2[T] =
      SPVec.S2(vec(0) ** value, vec(1) ** value)

  }

  implicit class SPVecS2OpsFieldCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S2[T]
  )(implicit ev: Field[T]) {

    def div(value: T): SPVec.S2[T] =
      SPVec.S2(vec(0) / value, vec(1) / value)

    def div(other: SPVec.S2[T]): SPVec.S2[T] =
      SPVec.S2(vec(0) / other(0), vec(1) / other(1))

  }

  implicit class SPVecS3OpsCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S3[T]
  ) {
    def change(x0: T = vec(0), x1: T = vec(1), x2: T = vec(2)): SPVec.S3[T] =
      SPVec.S3(x0, x1, x2)
  }

  implicit class SPVecS3OpsNumericCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S3[T]
  )(implicit ev: Numeric[T]) {
    def toDouble: SPVec.S3[Double] = 
      SPVec.S3(ev.toDouble(vec(0)), ev.toDouble(vec(1)), ev.toDouble(vec(2)))
  }

  implicit class SPVecS3OpsAdditiveMonoidCT[
      @sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S3[T]
  )(implicit ev: AdditiveMonoid[T]) {

    def add(value: T): SPVec.S3[T] =
      SPVec.S3(vec(0) + value, vec(1) + value, vec(2) + value)

    def add(other: SPVec.S3[T]): SPVec.S3[T] =
      SPVec.S3(vec(0) + other(0), vec(1) + other(1), vec(2) + other(2))

  }

  implicit class SPVecS3OpsAdditiveAbGroupCT[
      @sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S3[T]
  )(implicit ev: AdditiveAbGroup[T]) {

    def sub(value: T): SPVec.S3[T] =
      SPVec.S3(vec(0) - value, vec(1) - value, vec(2) - value)

    def sub(other: SPVec.S3[T]): SPVec.S3[T] =
      SPVec.S3(vec(0) - other(0), vec(1) - other(1), vec(2) - other(2))

  }

  implicit class SPVecS3OpsMultiplicativeMonoidCT[
      @sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S3[T]
  )(implicit ev: MultiplicativeMonoid[T]) {

    def mult(value: T): SPVec.S3[T] =
      SPVec.S3(vec(0) * value, vec(1) * value, vec(2) * value)

    def mult(other: SPVec.S3[T]): SPVec.S3[T] =
      SPVec.S3(vec(0) * other(0), vec(1) * other(1), vec(2) * other(2))

  }

  implicit class SPVecS3OpsRingCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S3[T]
  )(implicit ev: Ring[T]) {

    def pow(value: Int): SPVec.S3[T] =
      SPVec.S3(vec(0) ** value, vec(1) ** value, vec(2) ** value)

  }

  implicit class SPVecS3OpsFieldCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec.S3[T]
  )(implicit ev: Field[T]) {

    def div(value: T): SPVec.S3[T] =
      SPVec.S3(vec(0) / value, vec(1) / value, vec(2) / value)

    def div(other: SPVec.S3[T]): SPVec.S3[T] =
      SPVec.S3(vec(0) / other(0), vec(1) / other(1), vec(2) / other(2))

  }


}