package co.mefu.msl.core.ops

import scala.{specialized => sp}
import scala.reflect.ClassTag

import cats.Show

import spire.algebra._
import spire.implicits._

import co.mefu.msl.core.SPFunc1
import co.mefu.msl.core.SPFunc2

import co.mefu.msl.core.ds.SPBlock
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ds.SPVec.DSPVec
import co.mefu.msl.core.ds.SPVec.S2
import co.mefu.msl.core.ds.SPVec.S3

object SPVecOps {

  implicit class SPVecOps[@sp(Int, Long, Float, Double) T](
      val vec: SPVec[T]
  ) {

    def foreach(f: SPFunc1[T, Unit]): Unit =
      cfor(0)(_ < vec.x, _ + 1)(i => f(vec(i)))

    def foreach(f: SPFunc2[Int, T, Unit]): Unit =
      cfor(0)(_ < vec.x, _ + 1)(i => f(i, vec(i)))

    def transform(f: SPFunc1[T, T]): Unit =
      cfor(0)(_ < vec.x, _ + 1)(i => vec(i) = f(vec(i)))

    def transform(f: SPFunc2[Int, T, T]): Unit =
      cfor(0)(_ < vec.x, _ + 1)(i => vec(i) = f(i, vec(i)))

    def fold(initial: T)(f: SPFunc2[T, T, T]): T = {
      var acc = initial
      cfor(0)(_ < vec.x, _ + 1)(i => {
        acc = f(acc, vec(i))
      })
      acc
    }

    def reduce(f: SPFunc2[T, T, T]): T = {
      var acc = vec(0)
      cfor(1)(_ < vec.x, _ + 1)(i => {
        acc = f(acc, vec(i))
      })
      acc
    }

    def forall(f: SPFunc1[T, Boolean]): Boolean = {
      cfor(0)(_ < vec.x, _ + 1)(i => {
        if (!f(vec(i))) return false
      })
      true
    }

    def forall(f: SPFunc2[Int, T, Boolean]): Boolean = {
      cfor(0)(_ < vec.x, _ + 1)(i => {
        if (!f(i, vec(i))) return false
      })
      true
    }

    def find(f: SPFunc1[T, Boolean]): Option[T] = {
      cfor(0)(_ < vec.x, _ + 1)(i => {
        val el = vec(i)
        if (f(el)) return Some(el)
      })
      None
    }

    def find(f: SPFunc2[Int, T, Boolean]): Option[T] = {
      cfor(0)(_ < vec.x, _ + 1)(i => {
        val el = vec(i)
        if (f(i, el)) return Some(el)
      })
      None
    }

    def fill(f: SPFunc1[Int, T]): Unit =
      cfor(0)(_ < vec.x, _ + 1)(i => vec(i) = f(i))

    def copyTo(dst: SPVec[T]): Unit =
      cfor(0)(_ < vec.x, _ + 1)(i => dst(i) = vec(i))

    def swap(other: SPVec[T]): Unit = {
      cfor(0)(_ < vec.x, _ + 1)(i => {
        val tmp = other(i)
        other(i) = vec(i)
        vec(i) = tmp
      })
    }

    def swap(i1: Int, i2: Int): Unit = {
      val tmp = vec(i1)
      vec(i1) = vec(i2)
      vec(i2) = tmp
    }

    def reverse(from: Int = 0, to: Int = vec.x): Unit =
      cfor(from)(_ < (to - from) / 2, _ + 1)(i => swap(i, to - i - 1))

    lazy val clamped: SPFunc1[Int, T] =
      i =>
        vec(
          if (i < 0) 0
          else if (i > vec.x - 1) vec.x - 1
          else i
      )

    lazy val mirrored: SPFunc1[Int, T] =
      i =>
        vec(
          if (i < 0) -1 - i
          else if (i > vec.x - 1) 2 * vec.x - i - 1
          else i
      )

    def padTo(vec2: SPVec.S2[Int],
              padF: SPFunc1[Int, T],
              dst: SPVec[T]): Unit = {
      vec.copyTo(dst)

      cfor(0 - vec2(0))(_ < 0, _ + 1)(i => {
        dst(i) = padF(i)
      })

      cfor(vec.x)(_ < vec.x + vec2(1), _ + 1)(i => {
        dst(i) = padF(i)
      })
    }

    def padWithTo(vec2: SPVec.S2[Int], elem: T, dst: SPVec[T]): Unit = {
      padTo(vec2, _ => elem, dst)
    }

    def padClampTo(vec2: SPVec.S2[Int], dst: SPVec[T]): Unit = {
      padTo(vec2, clamped, dst)
    }

    def padMirrorTo(vec2: SPVec.S2[Int], dst: SPVec[T]): Unit = {
      padTo(vec2, mirrored, dst)
    }

  }

  implicit class SPVecOpsCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec[T]
  ) {

    def map(f: SPFunc1[T, T]): SPVec[T] = {
      val dst = SPVec.empty(vec.x)
      cfor(0)(_ < vec.x, _ + 1)(i => dst(i) = f(vec(i)))
      dst
    }

    def map(f: SPFunc2[Int, T, T]): SPVec[T] = {
      val dst = SPVec.empty(vec.x)
      cfor(0)(_ < vec.x, _ + 1)(i => dst(i) = f(i, vec(i)))
      dst
    }

    def copy(from: Int = 0, to: Int = vec.x): SPVec[T] = {
      val dst = SPVec.empty(to - from)
      cfor(from)(_ < to, _ + 1)(i => dst(i - from) = vec(i))
      dst
    }

    def emptyPadDest(vec2: SPVec.S2[Int]): SPVec[T] = {
      val dstBlk = SPBlock.empty[T](vec2(0) + vec.x + vec2(1))
      SPVec.create[T](vec.x, 1, vec2(0))(dstBlk)
    }

    def pad(vec2: SPVec.S2[Int], padF: SPFunc1[Int, T]): SPVec[T] = {
      val dst = emptyPadDest(vec2)
      vec.padTo(vec2, padF, dst)
      dst
    }

    def padWith(vec2: SPVec.S2[Int], elem: T): SPVec[T] = {
      val dst = emptyPadDest(vec2)
      vec.padWithTo(vec2, elem, dst)
      dst
    }

    def padClamp(vec2: SPVec.S2[Int]): SPVec[T] = {
      val dst = emptyPadDest(vec2)
      vec.padClampTo(vec2, dst)
      dst
    }

    def padMirror(vec2: SPVec.S2[Int]): SPVec[T] = {
      val dst = emptyPadDest(vec2)
      vec.padMirrorTo(vec2, dst)
      dst
    }

  }

  implicit class SPVecOpsShow[@sp(Int, Long, Float, Double) T](
      val vec: SPVec[T]
  )(implicit ev: Show[T]) {

    def show: String = {
      val sb = new scala.collection.mutable.StringBuilder()
      sb ++= "[ "
      cfor(0)(_ < vec.x, _ + 1)(i => {
        sb ++= ev.show(vec(i)) + " "
      })
      sb ++= "]"
      sb.toString
    }

  }

  implicit class SPVecOpsEq[@sp(Int, Long, Float, Double) T](
      val vec: SPVec[T]
  )(implicit ev: Eq[T]) {

    def isEqual(other: SPVec[T]): Boolean =
      vec.forall((i, el) => el === other(i))

  }

  implicit class SPVecOpsOrder[@sp(Int, Long, Float, Double) T](
      val vec: SPVec[T]
  )(implicit ev: Order[T]) {

    def max: T = vec.reduce(ev.max(_, _))

    def min: T = vec.reduce(ev.min(_, _))

  }

  implicit class SPVecOpsAdditiveMonoid[@sp(Int, Long, Float, Double) T](
      val vec: SPVec[T]
  )(implicit ev: AdditiveMonoid[T]) {

    def sum: T = vec.reduce(_ + _)

    def addI(value: T): Unit = vec.transform(_ + value)

    def addI(other: SPVec[T]): Unit = vec.transform((i, el) => el + other(i))

  }

  implicit class SPVecOpsAdditiveMonoidCT[
      @sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec[T]
  )(implicit ev: AdditiveMonoid[T]) {

    def add(value: T): SPVec[T] = vec.map(_ + value)

    def add(other: SPVec[T]): SPVec[T] = vec.map((i, el) => el + other(i))

  }

  implicit class SPVecOpsAdditiveAbGroup[@sp(Int, Long, Float, Double) T](
      val vec: SPVec[T]
  )(implicit ev: AdditiveAbGroup[T]) {

    def subI(value: T): Unit = vec.transform(_ - value)

    def subI(other: SPVec[T]): Unit = vec.transform((i, el) => el - other(i))
  }

  implicit class SPVecOpsAdditiveAbGroupCT[
      @sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec[T]
  )(implicit ev: AdditiveAbGroup[T]) {

    def sub(value: T): SPVec[T] = vec.map(_ - value)

    def sub(other: SPVec[T]): SPVec[T] = vec.map((i, el) => el - other(i))
  }

  implicit class SPVecOpsMultiplicativeMonoid[@sp(Int, Long, Float, Double) T](
      val vec: SPVec[T]
  )(implicit ev: MultiplicativeMonoid[T]) {

    def product: T = vec.reduce(_ * _)

    def multI(value: T): Unit = vec.transform(_ * value)

    def multI(other: SPVec[T]): Unit = vec.transform((i, el) => el * other(i))

  }

  implicit class SPVecOpsMultiplicativeMonoidCT[
      @sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec[T]
  )(implicit ev: MultiplicativeMonoid[T]) {

    def mult(value: T): SPVec[T] = vec.map(_ * value)

    def mult(other: SPVec[T]): SPVec[T] = vec.map((i, el) => el * other(i))
  }

  implicit class SPVecOpsRing[@sp(Int, Long, Float, Double) T](
      val vec: SPVec[T]
  )(implicit ev: Ring[T]) {

    def powI(value: Int): Unit =
      cfor(0)(_ < vec.x, _ + 1)(i => vec(i) **= value)

  }

  implicit class SPVecOpsRingCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec[T]
  )(implicit ev: Ring[T]) {

    def pow(value: Int): SPVec[T] = vec.map(_ ** value)

  }

  implicit class SPVecOpsField[@sp(Int, Long, Float, Double) T](
      val vec: SPVec[T]
  )(implicit ev: Field[T]) {

    def mean: T = vec.sum / vec.size

    def variance: T = {
      val m = mean
      var res = ev.zero

      vec.foreach(el => res += (el - m) ** 2)

      res / vec.size
    }

    def divI(value: T): Unit = vec.transform(_ / value)

    def divI(other: SPVec[T]): Unit = vec.transform((i, el) => el / other(i))

  }

  implicit class SPVecOpsFieldCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec[T]
  )(implicit ev: Field[T]) {

    def div(value: T): SPVec[T] = vec.map(_ / value)

    def div(other: SPVec[T]): SPVec[T] = vec.map((i, el) => el / other(i))

  }

  implicit class SPVecOpsFieldOrder[@sp(Int, Long, Float, Double) T](
      val vec: SPVec[T]
  )(implicit ev: Field[T], ev2: Order[T]) {

    def normalizeI(min: T = ev.zero, max: T = ev.one): Unit = { 
      vec.divI(vec.max)
      vec.multI(max-min)
      vec.addI(min)
    }
  }

  implicit class SPVecOpsFieldOrderCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec[T]
  )(implicit ev: Field[T], ev2: Order[T]) {

    def normalize(min: T = ev.zero, max: T = ev.one): SPVec[T] = 
      vec.div(vec.max).mult(max-min).add(min)
  }

  implicit class SPVecOpsFieldNRoot[@sp(Int, Long, Float, Double) T](
      val vec: SPVec[T]
  )(implicit ev1: Field[T], ev2: NRoot[T]) {

    def sd: T = ev2.sqrt(vec.variance)

  }

  implicit class SPVecOpsRingNRootCT[@sp(Int, Long, Float, Double) T: ClassTag](
      val vec: SPVec[T]
  )(implicit ev1: Ring[T], ev2: NRoot[T]) {

    def distance(other: SPVec[T]): T = {
      var sqSum = ev1.zero

      cfor(0)(_ < vec.x, _ + 1)(i => {
        sqSum += (vec(i) - other(i)) ** 2
      })

      ev2.sqrt(sqSum)
    }

  }

}
