package co.mefu.msl.core

import scala.util.{Random => ScalaRandom}

trait RandomGenerator[T] {
  def next: T
  def next(min: T, range: T): T
}

object Random {
  val generator = new ScalaRandom(1L)

  implicit val doubleRandGen: RandomGenerator[Double] =
    new RandomGenerator[Double] {
      def next: Double = generator.nextDouble()
      def next(min: Double, range: Double) = min + next * range
    }

  implicit val intRandGen: RandomGenerator[Int] = new RandomGenerator[Int] {
    def next: Int = generator.nextInt(Int.MaxValue)
    def next(min: Int, range: Int): Int = min + generator.nextInt(range)
  }

  def next[T](implicit rgt: RandomGenerator[T]): T = rgt.next
  def next[T](min: T, range: T)(implicit rgt: RandomGenerator[T]): T =
    rgt.next(min, range)

  def nextList[T](size: Int)(implicit rgt: RandomGenerator[T]): List[T] =
    List.fill(size)(rgt.next)

  def nextList[T](size: Int, min: T, range: T)(
      implicit rgt: RandomGenerator[T]): List[T] =
    List.fill(size)(rgt.next(min, range))

  def roll(chance: Double): Boolean = next[Double] <= chance

  def choose[T](ts: Seq[T]): Option[T] =
    ScalaRandom.shuffle(ts).headOption

  def chooseWeighted[T](ts: Seq[(T, Double)]): Option[T] = {
    val total = ts.map(_._2).sum

    var cum = 0d
    val weighted = ts.map {
      case (el, weight) =>
        val newWeight = cum + weight / total
        cum = newWeight
        (el, cum)
    }

    val roll = next[Double]
    weighted.find(el => roll <= el._2).map(_._1)
  }
}
