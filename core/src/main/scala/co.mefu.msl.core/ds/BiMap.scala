package co.mefu.msl.core.ds

class BiMap[K, V] private (
    val kToV: Map[K, V] = Map[K, V](),
    val vToK: Map[V, K] = Map[V, K]()
) {

  object dummy1 { implicit val dummy: dummy1.type = this }
  object dummy2 { implicit val dummy: dummy2.type = this }

  def add(k: K, v: V): BiMap[K, V] =
    new BiMap(kToV + (k -> v), vToK + (v -> k))
  def addAll(kvs: List[(K, V)]) =
    new BiMap(kToV ++ kvs, vToK ++ kvs.map(e => (e._2, e._1)))

  def remove(k: K)(implicit d: dummy1.type): BiMap[K, V] =
    new BiMap(kToV - k, vToK - kToV(k))
  def remove(v: V)(implicit d: dummy2.type): BiMap[K, V] =
    new BiMap(kToV - vToK(v), vToK - v)

  def apply(k: K)(implicit d: dummy1.type): V = kToV(k)
  def apply(v: V)(implicit d: dummy2.type): K = vToK(v)

  def map[K1, V1](f: (K, V) => (K1, V1)): BiMap[K1, V1] =
    BiMap(kToV.toList.map(f.tupled))

  def filter(f: (K, V) => Boolean): BiMap[K, V] =
    BiMap(kToV.toList.filter(f.tupled))

  def toKList: List[K] = kToV.keys.toList
  def toVList: List[V] = vToK.keys.toList

}

object BiMap {
  def empty[K, V]: BiMap[K, V] = new BiMap[K, V]()
  def apply[K, V](kvs: List[(K, V)]): BiMap[K, V] = empty[K, V].addAll(kvs)
}
