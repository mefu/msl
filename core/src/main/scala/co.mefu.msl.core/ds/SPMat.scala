package co.mefu.msl.core.ds

import scala.{specialized => sp}
import scala.reflect.ClassTag

import co.mefu.msl.core.SPFunc2

trait SPMat[@sp(Int, Long, Float, Double) T] extends SPFunc2[Int, Int, T] {

  def x: Int
  def y: Int

  def apply(i: Int, j: Int): T
  def update(i: Int, j: Int, value: T): Unit

  def subI(i: Int): SPVec[T]
  def subJ(j: Int): SPVec[T]

  val w: Int = x
  val h: Int = y
  val width: Int = x
  val height: Int = y
  val size: Int = x * y

  def apply(ij: SPVec.S2[Int]): T =
    apply(ij(0), ij(1))
  def update(ij: SPVec.S2[Int], value: T): Unit =
    update(ij(0), ij(1), value)

  def contains(i: Int, j: Int): Boolean = i >= 0 && j >= 0 && i < x && j < y

  def contains(ij: SPVec.S2[Int]): Boolean =
    contains(ij(0), ij(1))
}

object SPMat {

  class DSPMat[@sp(Int, Long, Float, Double) T] private[SPMat] (
      val x: Int,
      val y: Int,
      val strideX: Int,
      val strideY: Int,
      val offset: Int,
      val block: SPBlock[T]
  ) extends SPMat[T] {

    def apply(i: Int, j: Int): T = block(offset + strideX * i + strideY * j)
    def update(i: Int, j: Int, value: T): Unit =
      block(offset + strideX * i + strideY * j) = value

    def subI(i: Int): SPVec[T] =
      SPVec.create[T](y, strideY, offset + i * strideX)(block)
    def subJ(j: Int): SPVec[T] =
      SPVec.create[T](x, strideX, offset + j * strideY)(block)
  }

  class S22[@sp(Int, Long, Float, Double) T] private[SPMat] (
      val strideX: Int,
      val strideY: Int,
      val offset: Int,
      val block: SPBlock[T]
  ) extends SPMat[T] {

    val x: Int = 2
    val y: Int = 2

    def apply(i: Int, j: Int): T = block(offset + strideX * i + strideY * j)
    def update(i: Int, j: Int, value: T): Unit =
      block(offset + strideX * i + strideY * j) = value

    def subI(i: Int): SPVec.S2[T] =
      SPVec.S2.create(strideY, offset + i * strideX)(block)
    def subJ(j: Int): SPVec.S2[T] =
      SPVec.S2.create(strideX, offset + j * strideY)(block)

  }

  class S33[@sp(Int, Long, Float, Double) T] private[SPMat] (
      val strideX: Int,
      val strideY: Int,
      val offset: Int,
      val block: SPBlock[T]
  ) extends SPMat[T] {

    val x: Int = 3
    val y: Int = 3

    def apply(i: Int, j: Int): T = block(offset + strideX * i + strideY * j)
    def update(i: Int, j: Int, value: T): Unit =
      block(offset + strideX * i + strideY * j) = value

    def subI(i: Int): SPVec.S3[T] =
      SPVec.S3.create(strideY, offset + i * strideX)(block)
    def subJ(j: Int): SPVec.S3[T] =
      SPVec.S3.create(strideX, offset + j * strideY)(block)

  }

  object DSPMat {
    def create[@sp(Int, Long, Float, Double) T](
        x: Int, 
        y: Int,
        strideX: Int,
        strideY: Int,
        offset: Int)(block: SPBlock[T]): DSPMat[T] =
      new DSPMat[T](x, y, strideX, strideY, offset, block)

    def create[@sp(Int, Long, Float, Double) T](
        x: Int, y: Int)(
        block: SPBlock[T]): DSPMat[T] =
      create[T](x, y, y, 1, 0)(block)

    def apply[@sp(Int, Long, Float, Double) T: ClassTag](
      x: Int, y: Int)(ts: T*): DSPMat[T] =
      create[T](x, y)(SPBlock(ts: _*))

    def from[@sp(Int, Long, Float, Double) T](
      x: Int, y: Int)(arr: Array[T]): DSPMat[T] =
      create[T](x, y)(SPBlock.from(arr))

    def empty[@sp(Int, Long, Float, Double) T: ClassTag](
      x: Int, y: Int): DSPMat[T] =
      create[T](x, y)(SPBlock.empty[T](x * y))

    def fill[@sp(Int, Long, Float, Double) T: ClassTag](
      x: Int, y: Int)(t: T): DSPMat[T] =
      create[T](x, y)(SPBlock.fill[T](x * y)(t))
  }

  object S22 {
    def create[@sp(Int, Long, Float, Double) T](
        strideX: Int,
        strideY: Int,
        offset: Int)(block: SPBlock[T]): S22[T] =
      new S22[T](strideX, strideY, offset, block)

    def create[@sp(Int, Long, Float, Double) T](
      block: SPBlock[T]): S22[T] =
      create[T](2, 1, 0)(block)

    def apply[@sp(Int, Long, Float, Double) T: ClassTag](
        x00: T,
        x01: T,
        x10: T,
        x11: T
    ): S22[T] =
      create[T](SPBlock(x00, x01, x10, x11))

    def empty[@sp(Int, Long, Float, Double) T: ClassTag]: S22[T] =
      create[T](SPBlock.empty[T](4))

    def fill[@sp(Int, Long, Float, Double) T: ClassTag](t: T): S22[T] =
      create[T](SPBlock.fill[T](4)(t))
  }

  object S33 {
    def create[@sp(Int, Long, Float, Double) T](
        strideX: Int,
        strideY: Int,
        offset: Int)(block: SPBlock[T]): S33[T] =
      new S33[T](strideX, strideY, offset, block)

    def create[@sp(Int, Long, Float, Double) T](
      block: SPBlock[T]): S33[T] =
      create[T](3, 1, 0)(block)

    def apply[@sp(Int, Long, Float, Double) T: ClassTag](
        x00: T,
        x01: T,
        x02: T,
        x10: T,
        x11: T,
        x12: T,
        x20: T,
        x21: T,
        x22: T
    ): S33[T] =
      create[T](SPBlock(x00, x01, x02, x10, x11, x12, x20, x21, x22))

    def empty[@sp(Int, Long, Float, Double) T: ClassTag]: S33[T] =
      create[T](SPBlock.empty[T](9))

    def fill[@sp(Int, Long, Float, Double) T: ClassTag](t: T): S33[T] =
      create[T](SPBlock.fill[T](9)(t))
  }

  def create[@sp(Int, Long, Float, Double) T](
      x: Int, 
      y: Int,
      strideX: Int,
      strideY: Int,
      offset: Int)(block: SPBlock[T]): SPMat[T] =
    DSPMat.create[T](x, y, strideX, strideY, offset)(block)

  def create[@sp(Int, Long, Float, Double) T](
      x: Int, y: Int)(
      block: SPBlock[T]): SPMat[T] =
    create[T](x, y, y, 1, 0)(block)

  def apply[@sp(Int, Long, Float, Double) T: ClassTag](
    x: Int, y: Int)(ts: T*): SPMat[T] =
    create[T](x, y)(SPBlock(ts: _*))

  def from[@sp(Int, Long, Float, Double) T](
    x: Int, y: Int)(arr: Array[T]): SPMat[T] =
    create[T](x, y)(SPBlock.from(arr))

  def empty[@sp(Int, Long, Float, Double) T: ClassTag](
    x: Int, y: Int): SPMat[T] =
    create[T](x, y)(SPBlock.empty[T](x * y))

  def fill[@sp(Int, Long, Float, Double) T: ClassTag](
    x: Int, y: Int)(t: T): SPMat[T] =
    create[T](x, y)(SPBlock.fill[T](x * y)(t))
}
