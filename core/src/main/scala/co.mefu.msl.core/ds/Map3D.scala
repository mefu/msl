package co.mefu.msl.core.ds

import scala.reflect.ClassTag
import scala.collection.mutable.Buffer

import co.mefu.msl.core.geometry.shape.PlusSign3D

import Map3D.HasLoc

class Map3D[T <: HasLoc](val empty: T, val size: SPVec.S3[Int])(
    implicit ev: ClassTag[T]) {

  val list: Buffer[T] = Buffer[T]()
  val locMap: SPMat3D[T] = SPMat3D.fill[T](size(0), size(1), size(2))(empty)

  def add(ts: T*): Unit = {
    ts.foreach(t => {
      list += t
      locMap(t.loc) = t
    })
  }

  def get(loc: SPVec.S3[Int]): T = locMap(loc)

  def remove(t: T): Unit = remove(t.loc)

  def remove(loc: SPVec.S3[Int]): Unit = {
    if (!isEmpty(loc)) {
      val t = locMap(loc)
      locMap(loc) = empty
      list -= t
    }
  }

  def move(from: SPVec.S3[Int], to: SPVec.S3[Int]): Unit = {
    if (isEmpty(to) && !isEmpty(from)) {
      locMap(to) = locMap(from)
      locMap(from) = empty
    }
  }

  def isEmpty(loc: SPVec.S3[Int]): Boolean =
    locMap.contains(loc) && locMap(loc) == empty

  def getEmptyNeighbours(loc: SPVec.S3[Int]): Seq[SPVec.S3[Int]] =
    new PlusSign3D(loc).getPoints.filter(isEmpty)

}

object Map3D {
  trait HasLoc {
    def loc: SPVec.S3[Int]
  }
}
