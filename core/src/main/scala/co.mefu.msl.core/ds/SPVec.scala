package co.mefu.msl.core.ds

import scala.{specialized => sp}
import scala.reflect.ClassTag

import co.mefu.msl.core.SPFunc1

trait SPVec[@sp(Int, Long, Float, Double) T] extends SPFunc1[Int, T] {

  def x: Int

  def apply(i: Int): T
  def update(i: Int, value: T): Unit

  val w: Int = x
  val width: Int = x
  val size: Int = x

  def contains(i: Int): Boolean = i >= 0 && i < x

}

object SPVec {

  class DSPVec[@sp(Int, Long, Float, Double) T] private[SPVec] (
      val x: Int,
      val stride: Int,
      val offset: Int,
      val block: SPBlock[T]
  ) extends SPVec[T] {

    def apply(i: Int): T = block(offset + stride * i)
    def update(i: Int, value: T): Unit = block(offset + stride * i) = value

  }

  class S2[@sp(Int, Long, Float, Double) T] private[SPVec] (
      val stride: Int,
      val offset: Int,
      val block: SPBlock[T]
  ) extends SPVec[T] {

    val x: Int = 2
    def apply(i: Int): T = block(offset + stride * i)
    def update(i: Int, value: T): Unit = block(offset + stride * i) = value

  }

  class S3[@sp(Int, Long, Float, Double) T] private[SPVec] (
      val stride: Int,
      val offset: Int,
      val block: SPBlock[T]
  ) extends SPVec[T] {

    val x: Int = 3
    def apply(i: Int): T = block(offset + stride * i)
    def update(i: Int, value: T): Unit = block(offset + stride * i) = value

  }

  object DSPVec {

    def create[@sp(Int, Long, Float, Double) T](
        x: Int, stride: Int, offset: Int)(
        block: SPBlock[T]): DSPVec[T] =
      new DSPVec[T](x, stride, offset, block)

    def create[@sp(Int, Long, Float, Double) T](
        x: Int)(block: SPBlock[T]): DSPVec[T] =
      create[T](x, 1, 0)(block)

    def create[@sp(Int, Long, Float, Double) T](
        block: SPBlock[T]): DSPVec[T] =
      create[T](block.size, 1, 0)(block)

    def from[@sp(Int, Long, Float, Double) T](arr: Array[T]) =
      create[T](SPBlock.from(arr))

    def apply[@sp(Int, Long, Float, Double) T: ClassTag](ts: T*) =
      create[T](SPBlock(ts: _*))

    def empty[@sp(Int, Long, Float, Double) T: ClassTag](x: Int): DSPVec[T] =
      create[T](SPBlock.empty[T](x))

    def fill[@sp(Int, Long, Float, Double) T: ClassTag](x: Int)(t: T): DSPVec[T] =
      create[T](SPBlock.fill[T](x)(t))

  }

  object S2 {

    def create[@sp(Int, Long, Float, Double) T](
        stride: Int, offset: Int)(
        block: SPBlock[T]): S2[T] =
      new S2[T](stride, offset, block)

    def create[@sp(Int, Long, Float, Double) T](
        block: SPBlock[T]): S2[T] =
      create[T](1, 0)(block)

    def apply[@sp(Int, Long, Float, Double) T: ClassTag](x0: T, x1: T): S2[T] =
      create[T](SPBlock(x0, x1))

    def empty[@sp(Int, Long, Float, Double) T: ClassTag]: S2[T] =
      create[T](SPBlock.empty[T](2))

    def fill[@sp(Int, Long, Float, Double) T: ClassTag](t: T): S2[T] =
      create[T](SPBlock.fill[T](2)(t))
  }

  object S3 {

    def create[@sp(Int, Long, Float, Double) T](
        stride: Int, offset: Int)(
        block: SPBlock[T]): S3[T] =
      new S3[T](stride, offset, block)

    def create[@sp(Int, Long, Float, Double) T](
        block: SPBlock[T]): S3[T] =
      new S3[T](1, 0, block)

    def apply[@sp(Int, Long, Float, Double) T: ClassTag](x0: T,
                                                         x1: T,
                                                         x2: T): S3[T] =
      create[T](SPBlock(x0, x1, x2))

    def empty[@sp(Int, Long, Float, Double) T: ClassTag]: S3[T] =
      create[T](SPBlock.empty[T](3))

    def fill[@sp(Int, Long, Float, Double) T: ClassTag](t: T): S3[T] =
      create[T](SPBlock.fill[T](3)(t))
  }

  def create[@sp(Int, Long, Float, Double) T](
      x: Int, stride: Int, offset: Int)(
      block: SPBlock[T]): SPVec[T] =
    DSPVec.create[T](x, stride, offset)(block)

  def create[@sp(Int, Long, Float, Double) T](
      x: Int)(block: SPBlock[T]): SPVec[T] =
    create[T](x, 1, 0)(block)

  def create[@sp(Int, Long, Float, Double) T](
      block: SPBlock[T]): SPVec[T] =
    create[T](block.size, 1, 0)(block)

  def from[@sp(Int, Long, Float, Double) T](arr: Array[T]): SPVec[T] =
    create[T](SPBlock.from(arr))

  def apply[@sp(Int, Long, Float, Double) T: ClassTag](ts: T*): SPVec[T] =
    create[T](SPBlock(ts: _*))

  def empty[@sp(Int, Long, Float, Double) T: ClassTag](x: Int): SPVec[T] =
    create[T](SPBlock.empty[T](x))

  def fill[@sp(Int, Long, Float, Double) T: ClassTag](x: Int)(t: T): SPVec[T] =
    create[T](SPBlock.fill[T](x)(t))
}
