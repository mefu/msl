package co.mefu.msl.core.ds

import scala.{specialized => sp}
import scala.reflect.ClassTag

import spire.implicits._

final class SPBlock[@sp(Int, Long, Float, Double) T] private[ds] (
    val size: Int,
    val mem: Array[T]
) {
  @inline def apply(idx: Int): T = mem(idx)
  @inline def update(idx: Int, value: T) = mem(idx) = value
  def contains(idx: Int): Boolean = idx >= 0 && idx < size
}

object SPBlock {

  def create[@sp(Int, Long, Float, Double) T](size: Int)(
      arr: Array[T]): SPBlock[T] =
    new SPBlock[T](size, arr)

  def apply[@sp(Int, Long, Float, Double) T: ClassTag](ts: T*): SPBlock[T] =
    create[T](ts.length)(ts.toArray)

  def from[@sp(Int, Long, Float, Double) T](arr: Array[T]): SPBlock[T] =
    create[T](arr.length)(arr)

  def fill[@sp(Int, Long, Float, Double) T: ClassTag](
      size: Int)(t: T): SPBlock[T] =
    create[T](size)(Array.fill(size)(t))

  def empty[@sp(Int, Long, Float, Double) T: ClassTag](size: Int): SPBlock[T] =
    create[T](size)(Array.ofDim[T](size))
}

