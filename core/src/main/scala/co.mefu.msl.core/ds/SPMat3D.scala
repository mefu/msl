package co.mefu.msl.core.ds

import scala.{specialized => sp}
import scala.reflect.ClassTag

import spire.implicits._

import co.mefu.msl.core.SPFunc1
import co.mefu.msl.core.SPFunc2
import co.mefu.msl.core.SPFunc3
import co.mefu.msl.core.SPFunc4

trait SPMat3D[@sp(Int, Long, Float, Double) T]
    extends SPFunc3[Int, Int, Int, T] {

  def x: Int
  def y: Int
  def z: Int

  def apply(i: Int, j: Int, k: Int): T
  def update(i: Int, j: Int, k: Int, value: T): Unit

  def subI(i: Int): SPMat[T]
  def subJ(j: Int): SPMat[T]
  def subK(k: Int): SPMat[T]

  val w: Int = x
  val h: Int = y
  val d: Int = z
  val width: Int = x
  val height: Int = y
  val depth: Int = y
  val size: Int = x * y * z

  def apply(ijk: SPVec.S3[Int]): T =
    apply(ijk(0), ijk(1), ijk(2))
  def update(ijk: SPVec.S3[Int], value: T): Unit =
    update(ijk(0), ijk(1), ijk(2), value)

  def contains(i: Int, j: Int, k: Int): Boolean =
    i >= 0 && j >= 0 && k >= 0 && i < x && j < y && k < z

  def contains(ijk: SPVec.S3[Int]): Boolean =
    contains(ijk(0), ijk(1), ijk(2))
}

object SPMat3D {

  class DSPMat3D[@sp(Int, Long, Float, Double) T] private[SPMat3D] (
      val x: Int,
      val y: Int,
      val z: Int,
      val strideX: Int,
      val strideY: Int,
      val strideZ: Int,
      val offset: Int,
      val block: SPBlock[T]
  ) extends SPMat3D[T] {

    def apply(i: Int, j: Int, k: Int): T =
      block(offset + i * strideX + j * strideY + k * strideZ)
    def update(i: Int, j: Int, k: Int, value: T): Unit =
      block(offset + i * strideX + j * strideY + k * strideZ) = value

    def subI(i: Int): SPMat[T] =
      SPMat.create[T](y, z, strideY, strideZ, offset + i * strideX)(block)
    def subJ(j: Int): SPMat[T] =
      SPMat.create[T](x, z, strideX, strideZ, offset + j * strideY)(block)
    def subK(k: Int): SPMat[T] =
      SPMat.create[T](x, y, strideX, strideY, offset + k * strideZ)(block)
  }

  object DSPMat3D {
    def create[@sp(Int, Long, Float, Double) T](
        x: Int, 
        y: Int,
        z: Int,
        strideX: Int,
        strideY: Int,
        strideZ: Int,
        offset: Int)(block: SPBlock[T]): DSPMat3D[T] =
      new DSPMat3D[T](x, y, z, strideX, strideY, strideZ, offset, block)

    def create[@sp(Int, Long, Float, Double) T](
        x: Int, y: Int, z: Int)(
        block: SPBlock[T]): DSPMat3D[T] =
      create[T](x, y, z, y * z, z, 1, 0)(block)

    def apply[@sp(Int, Long, Float, Double) T: ClassTag](
      x: Int, y: Int, z: Int)(ts: T*): DSPMat3D[T] =
      create[T](x, y, z)(SPBlock(ts: _*))

    def from[@sp(Int, Long, Float, Double) T](
      x: Int, y: Int, z: Int)(arr: Array[T]): DSPMat3D[T] =
      create[T](x, y, z)(SPBlock.from(arr))

    def empty[@sp(Int, Long, Float, Double) T: ClassTag](
      x: Int, y: Int, z: Int): DSPMat3D[T] =
      create[T](x, y, z)(SPBlock.empty[T](x * y * z))

    def fill[@sp(Int, Long, Float, Double) T: ClassTag](
      x: Int, y: Int, z: Int)(t: T): DSPMat3D[T] =
      create[T](x, y, z)(SPBlock.fill[T](x * y * z)(t))
  }

  def create[@sp(Int, Long, Float, Double) T](
      x: Int, 
      y: Int,
      z: Int,
      strideX: Int,
      strideY: Int,
      strideZ: Int,
      offset: Int)(block: SPBlock[T]): SPMat3D[T] =
    DSPMat3D.create[T](x, y, z, strideX, strideY, strideZ, offset)(block)

  def create[@sp(Int, Long, Float, Double) T](
      x: Int, y: Int, z: Int)(
      block: SPBlock[T]): SPMat3D[T] =
    create[T](x, y, z, y * z, z, 1, 0)(block)

  def apply[@sp(Int, Long, Float, Double) T: ClassTag](
    x: Int, y: Int, z: Int)(ts: T*): SPMat3D[T] =
    create[T](x, y, z)(SPBlock(ts: _*))

  def from[@sp(Int, Long, Float, Double) T](
    x: Int, y: Int, z: Int)(arr: Array[T]): SPMat3D[T] =
    create[T](x, y, z)(SPBlock.from(arr))

  def empty[@sp(Int, Long, Float, Double) T: ClassTag](
    x: Int, y: Int, z: Int): SPMat3D[T] =
    create[T](x, y, z)(SPBlock.empty[T](x * y * z))

  def fill[@sp(Int, Long, Float, Double) T: ClassTag](
    x: Int, y: Int, z: Int)(t: T): SPMat3D[T] =
    create[T](x, y, z)(SPBlock.fill[T](x * y * z)(t))
}
