package co.mefu.msl.core

import scala.{specialized => sp}

trait SPFunc0[@sp(Int, Long, Float, Double, Boolean, Unit) OUT] {
  def apply: OUT
}

trait SPFunc1[@sp(Int, Long, Float, Double) IN1,
              @sp(Int, Long, Float, Double, Boolean, Unit) OUT] {
  def apply(in1: IN1): OUT
}

trait SPFunc2[@sp(Int, Long, Float, Double) IN1,
              @sp(Int, Long, Float, Double) IN2,
              @sp(Int, Long, Float, Double, Boolean, Unit) OUT] {
  def apply(in1: IN1, in2: IN2): OUT
}

trait SPFunc3[@sp(Int, Long, Float, Double) IN1,
              @sp(Int, Long, Float, Double) IN2,
              @sp(Int, Long, Float, Double) IN3,
              @sp(Int, Long, Float, Double, Boolean, Unit) OUT] {
  def apply(in1: IN1, in2: IN2, in3: IN3): OUT
}

trait SPFunc4[@sp(Int, Long, Float, Double) IN1,
              @sp(Int, Long, Float, Double) IN2,
              @sp(Int, Long, Float, Double) IN3,
              @sp(Int, Long, Float, Double) IN4,
              @sp(Int, Long, Float, Double, Boolean, Unit) OUT] {
  def apply(in1: IN1, in2: IN2, in3: IN3, in4: IN4): OUT
}
