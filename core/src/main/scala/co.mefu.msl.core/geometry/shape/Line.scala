package co.mefu.msl.core.geometry.shape

import co.mefu.msl.core.ds.SPVec

class Line(start: SPVec.S3[Int], end: SPVec.S3[Int], direction: Int)
    extends Shape3D {
  def getPoints: List[SPVec.S3[Int]] = {
    if (direction == 0)
      (start(0) to end(0)).map(SPVec.S3(_, start(1), start(2))).toList
    else if (direction == 1)
      (start(1) to end(1)).map(SPVec.S3(start(0), _, start(2))).toList
    else if (direction == 2)
      (start(2) to end(2)).map(SPVec.S3(start(0), start(1), _)).toList
    else
      List()
  }
}
