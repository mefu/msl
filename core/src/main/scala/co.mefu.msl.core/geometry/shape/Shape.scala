package co.mefu.msl.core.geometry.shape

import co.mefu.msl.core.ds.SPVec

trait Shape2D {
  def getPoints: List[SPVec.S2[Int]]
}

trait Shape3D {
  def getPoints: List[SPVec.S3[Int]]
}