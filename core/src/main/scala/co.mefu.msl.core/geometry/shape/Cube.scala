package co.mefu.msl.core.geometry.shape

import co.mefu.msl.core.ds.SPVec

class Cube(start: SPVec.S3[Int], size: Int) extends Shape3D {
  def getPoints: List[SPVec.S3[Int]] = {
    (start(0) until (start(0) + size))
      .flatMap(
        x => {
          (start(1) until (start(1) + size)).flatMap(
            y => {
              (start(2) until (start(2) + size)).map(z => SPVec.S3(x, y, z))
            }
          )
        }
      )
      .toList
  }
}
