package co.mefu.msl.core.geometry.shape

import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ops.SPVecSOps._

class PlusSign3D(center: SPVec.S3[Int]) extends Shape3D {
  def getPoints: List[SPVec.S3[Int]] = {
    List(
      center.change(x0 = center(0) + 1),
      center.change(x0 = center(0) - 1),
      center.change(x1 = center(1) + 1),
      center.change(x1 = center(1) - 1),
      center.change(x2 = center(2) + 1),
      center.change(x2 = center(2) - 1),
      center
    )
  }
}
