package co.mefu.msl.core.geometry.shape

import scala.collection.mutable.Buffer

import spire.implicits._

import co.mefu.msl.core.Random
import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ops.SPVecOps._
import co.mefu.msl.core.ops.SPVecSOps._

class RandomLine3D(start: SPVec.S3[Int],
                   end: SPVec.S3[Int],
                   direction: Int,
                   maxSpread: Int)
    extends Shape3D {
  def getPoints: List[SPVec.S3[Int]] = {
    val straightLine = new Line(start, end, direction).getPoints

    val endd = end.toDouble
    var current = start
    val spreadLine = Buffer[SPVec.S3[Int]](start)

    while (current(direction) != end(direction)) {
      val nbs = new Cube(current.sub(1), 3).getPoints.filter(p =>
        !spreadLine.contains(p))
      val weighted = nbs.flatMap(nb => {
        val nbd = nb.toDouble
        val currentd = current.toDouble
        val distClosed = currentd.distance(endd) - nbd.distance(endd)
        if (distClosed < 0)
          None
        else {
          val spread =
            straightLine.map(slp => slp.toDouble.distance(nbd)).min
          if (spread > maxSpread)
            None
          else
            Some((nb, distClosed))
        }
      })
      val next = Random.chooseWeighted(weighted).get
      spreadLine.append(next)
      current = next
    }

    spreadLine.toList
  }
}
