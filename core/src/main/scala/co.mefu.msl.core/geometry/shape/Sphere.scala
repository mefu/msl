package co.mefu.msl.core.geometry.shape

import spire.implicits._

import co.mefu.msl.core.ds.SPVec
import co.mefu.msl.core.ops.SPVecOps._
import co.mefu.msl.core.ops.SPVecSOps._

class Sphere(center: SPVec.S3[Int], radius: Int) extends Shape3D {
  def getPoints: List[SPVec.S3[Int]] = {
    (for {
      x <- (center(0) - radius) to (center(0) + radius)
      y <- (center(1) - radius) to (center(1) + radius)
      z <- (center(2) - radius) to (center(2) + radius)
      if (SPVec.S3[Double](x, y, z).distance(center.toDouble) <= radius)
    } yield SPVec.S3[Int](x, y, z)) toList
  }
}
