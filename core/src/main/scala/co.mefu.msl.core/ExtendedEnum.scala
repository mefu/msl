package co.mefu.msl.core

import scala.reflect.ClassTag

import enumeratum._

trait ExtendedEnumEntry extends EnumEntry {
  def toInt: Int
}

trait ExtendedEnum[T <: ExtendedEnumEntry] extends Enum[T] {

  lazy val indexShift = values(0).toInt * -1

  class EArr[U: ClassTag](val arr: Array[U]) {
    def apply[V >: U](t: T): V = arr(t.toInt + indexShift)
    def update(t: T, value: U): Unit = arr(t.toInt + indexShift) = value

    def copy: EArr[U] = new EArr[U](arr.clone)
    def map(f: U => U) = new EArr[U](arr.map(f).toArray)
  }

  def arrayFill[U: ClassTag](f: T => U): EArr[U] =
    new EArr[U](values.map(f).toArray)
}