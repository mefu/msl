package co.mefu.msl

package object core {
  type EnumStorage[T <: ExtendedEnumEntry, U] = ExtendedEnum[T]#EArr[U]
}