package co.mefu.msl.core.math

import spire.implicits._

class Polynomial(val coeffs: Array[Double]) {
  def apply(x: Double): Double = {
    var res = 0d
    cfor(0)(_ < coeffs.size, _ + 1)(i => {
      res += coeffs(i) * Math.pow(x, i)
    })
    res
  }
}