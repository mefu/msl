package co.mefu.msl.core.math

import spire.implicits._

// import org.orangepalantir.leastsquares.{Fitter => FitterF}
import org.orangepalantir.leastsquares.Function
import org.orangepalantir.leastsquares.fitters.LinearFitter
import org.orangepalantir.leastsquares.fitters.NonLinearSolver
import org.orangepalantir.leastsquares.fitters.MarquardtFitter

object Fitter {

  def fitPoly(n: Int, xs: Array[Double], rhs: Array[Double]): Polynomial = {
    val func = new Function {
      override def evaluate(values: Array[Double],
                            parameters: Array[Double]): Double = {
        val x = values(0)
        var res = 0d
        cfor(0)(_ < n, _ + 1)(i => {
          res += parameters(i) * Math.pow(x, i)
        })
        res
      }

      override def getNParameters = n
      override def getNInputs = 1
    }

    // val fitter = new LinearFitter(func)
    val fitter = new NonLinearSolver(func)
    // val fitter = new MarquardtFitter(func)

    fitter.setData(xs.map(el => Array(el)), rhs)
    fitter.setParameters(Array(0d, 0d, 0d))
    fitter.fitData

    new Polynomial(fitter.getParameters.clone)
  }

}
