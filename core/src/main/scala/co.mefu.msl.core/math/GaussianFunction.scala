package co.mefu.msl.core.math

case class GaussianFunction(
    val a: Double,
    val b: Double,
    val c: Double
) {

  def apply(x: Double): Double =
    a * Math.pow(Math.E, -1 * Math.pow(x - b, 2) / (2 * Math.pow(c, 2)))
}
