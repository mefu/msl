package co.mefu.msl.core.math

case class LogisticCurve(
    val A: Double,
    val K: Double,
    val B: Double,
    val v: Double,
    val Q: Double,
    val C: Double,
    val M: Double
) {

  def apply(t: Double): Double =
    A + ((K - A) / Math.pow(C + Q * Math.pow(Math.E, -1 * B * (t - M)), 1 / v))
}
